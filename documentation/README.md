Documentation
=============

Ce répertoire contient les diverses ressources (images...) nécessaire à la documentation générale de l'application.


*file* **.png** : image utilisée directement dans la documentation

*file* **.drawio** : source d'une image permettant sa modification sur https://www.draw.io
