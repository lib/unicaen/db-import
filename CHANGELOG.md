Changelog
=========

7.2.0
-----
- Suppression de la dépendance avec unicaen/app, et avec laminas/laminas-dependency-plugin.
- Prise en compte des microsecondes dans les dates de début/fin inscrites dans la table de logs et donc dans la durée d'exécution calculée (nécessite le type TIMESTAMP sous Oracle).
- Extraction de 2 HelperTrait (Oracle et Pg) pour factoriser du code potentiellement utile à tout Helper.
- [FIX] Le générateur SQL de la destination était utilisé aussi pour interroger la source, désormais la source a le sien propre.
- [FIX] Bug dans le DatabaseService en cas de connexion source de type NoConnection.

7.1.0
-----
- Rattrapage des évolutions réalisées dans la v6.2.0.

7.0.0
-----
- Requiert la bibliotèque unicaen/privilege.

6.2.0
-----
- Nouveau type de connexion NoConnection : non-connexion pour laquelle les données sont spécifiées manuellement sous forme d'un tableau.
- Clarification et refactorisation des ColumnValueFilter.

6.1.4
-----
- [FIX] Interrogation de IMPORT_OBSERV (table des changements de valeurs à détecter) : upper() était indispensable ! 

6.1.3
-----
- Déclaration de la nouvelle plateforme Doctrine\DBAL\Platforms\PostgreSQL120Platform.

6.1.2
-----
- Possibilité de voir le SQL généré et utilisé pour réaliser une synchro.
- Ajout de la colonne IMPORT_OBSERV_RESULT.SOURCE_ID permettant de filtrer selon la source de données.
- Import : possibilité de spécifier des colonnes dont la valeur est calculée (clé 'computed_columns').
- Possibilité de spécifier la liste des attributs/colonnes de la Source dans la config (clé 'columns').
- Possibilité de spécifier un tableau de filtres dans la clé 'column_value_filter' d'une source ou destination (et plus seulement un filtre unique).
- Capacité à traiter les réponses d'API au format Solr (refactorisation suite à l'ajout de la notion de 'response data extractor').
- Source : plus de mise en majuscules systématique de la 'source code column' et des 'columns'.

6.1.1
-----
- [FIX] Correction du SQL généré pour l'update de synchro : les colonnes histo_destruct* était mises à null, ce qui est
  incompatible avec la nouvelle option 'update_if_deleted_enabled_column'.

6.1.0
-----
- Liste des synchros : affichage de l'erreur de génération de la vue diff le cas échéant.
- Possibilité d'autoriser/interdire l'opération 'undelete' (déhistorisation) sur certains enregistrements, à l'aide d'une
  colonne à ajouter dans la table destination (cf. option de config 'undelete_enabled_column').
- Possibilité d'autoriser l'opération 'update' (mise à jour) sur un enregistrement historisé, à l'aide d'une 
  colonne à ajouter dans la table destination (cf. option de config 'update_if_deleted_enabled_column').
- [FIX] Détection incomplète d'une erreur lors de la génération de la vue différentielle

6.0.3
-----
- [FIX] Génération du SQL de diff : Gros bug en cas de type booléen non supporté par le SGBD.
- [FIX] Remplacement de classes Doctrine obsolètes dans SourceListener
- [FIX] DatabaseService::populateDestinationTableFromSource() : beginTransaction() doit être dans le try-catch.
- [FIX] AbstractColumnValueFilter::setParams() : les paramètres spécifiés s'ajoutent aux existants et ne les remplacent plus.
- [FIX] Vue du différentiel d'une synchro : suppression du warning PHP 'Undefined array key'. 
- [FIX] Vue du différentiel d'une synchro : prise en compte du paramètre de config 'source_code_column'.
- [FIX] Correction du SQL générant V_DIFF_* pour le cas d'une synchro depuis une source non-importable
- [FIX] Import : correction du SQL généré en cas de valeur booléenne dans les données sources.
- [FIX] Détermination des colonnes de la table source : correction d'un bug potentiel + amélioration de la remontée d'erreur.
- Nouvelles commandes pour lister les imports ('list imports') et synchros ('list synchros'). 
- Synchro : spécification dans la table SOURCE (colonnes 'synchro_*_enabled') des opérations autorisées ou non lors de la synchro (màj bdd requise).
  Exemple : ne pas autoriser le 'delete' est utile lorsque les données sources sont obtenues de façon incrémentale
  au fil du temps, et non pas de façon exhaustive en une seule fois.
- Suppression de 2 lignes intruses dans la config
- DatabaseService::truncateDestinationTable() : on vérifie désormais que les données contiennent un source_id, sinon le vidage partiel est inopérant.
- Préparation de la suppression du paramètre de config 'intermediate_table_auto_drop'
- Refactorisation : extraction d'une classe mère abtsraite AbstractDatabaseService et de ImportLogDatabaseService.

6.0.2
-----
- Correction de la version requise de unicaen/app

6.0.1
-----
- Possibilité de passer à guzzlehttp/guzzle 7.

6.0.0
-----
- PHP 8.0 minimum.
- [FIX] CHemins liés à unicaen/console remplaçant laminas/console

5.2.3
-----
- [FIX] Version requise de unicaen/livelog

5.2.2
-----
- [FIX] Synchro : la colonne 'source_id' peut être présente dans la table/vue source.
- [FIX] Correction du type de retour trop restrictif de SourceListener::fetchDefaultSource().

5.2.1
-----
- [FIX] Dysfonctionnement total de la synchro avec table intermédiaire lorsque le 'code' de la source est spécifié dans la config
- [FIX] Correction format de résultat non prévu dans TableValidationHelper::convertTableExistenceCheckResultToBoolean()
- [FIX] Plus d'obligation d'activer le module unicaen/livelog

5.2.0
-----
- [FIX] Correction de divers bugs.

5.1.1
-----
- [FIX] Import : seules les données issues de la source concernée doivent être préalablement purgées, pas toute la table. 

5.1.0
-----
- Import : le SOURCE_ID peut être fourni dans les données à importer ou en dur dans la config via l'attribut 'code' de la 'source' ; 
  les insertions sont désormais réalisées 1 enregistrement à la fois
- Source et Destination : retour de l'attribut 'name' (petit nom). 
  Source : l'attribut facultatif 'code' permet de spécifier le CODE d'une SOURCE.
- Amélioration des ColumnNameFilter et ColumnValueFilter
- Possibilité de filtrer la liste des imports/synchros par nom

5.0.0 (Bootstrap 5)
-----
- Passage possible à unicaen/app version 5 (Bootstrap 5).

4.0.2
-----
- Possibilité d'ordonner les imports/synchros.
- Possibilité d'afficher les logs d'import/synchro en live (avec unicaen/livelog).
- Possibilité de sélectionner les imports/synchros à lancer.
- Ajout d'un témoin indiquant si un problème a été rencontré pendant l'exécution.
- [FIX] ApiService : prise en compte des paramètres de config 'timeout' et 'connect_timeout'.

4.0.1
-----
- [FIX] Remplacement d'une classe dépréciée

4.0.0
-----
- Interface graphique pour les imports, synchros, observations, logs.  
- Migration vers Laminas (back-end).
- Migration vers Bootstrap 5 (front-end).
- Possibilité de transformer des noms de colonnes source (ColumnNameFilterInterface) et des valeurs de colonnes destination (ColumnValueFilterInterface).
- Filtre de nom de colonne source se basant sur un tableau de correspondance : ArrayMapColumnNameFilter.
- [FIX] Le WHERE de la destination n'était pas pris en compte dans l'interrogation de la vue diff
- [FIX] Dysfonctionnement des appels d'API paginés à cause d'une inversion d'arguments.
