<?php

namespace UnicaenDbImport;

use Laminas\Config\Factory as ConfigFactory;
use Unicaen\Console\Adapter\AdapterInterface;
use Laminas\Mvc\Application;
use Laminas\Mvc\MvcEvent;
use Laminas\Stdlib\Glob;

class Module
{
    public function onBootstrap(MvcEvent $e)
    {
        /** @var Application $application */
//        $application = $e->getParam('application');
//        $eventManager = $application->getEventManager();
    }

    public function getConfig()
    {
        $paths = Glob::glob(__DIR__ . '/config/{,*.}{config}.php', Glob::GLOB_BRACE);

        return ConfigFactory::fromFiles($paths);
    }

    public function getAutoloaderConfig()
    {
        return [
            'Laminas\Loader\StandardAutoloader' => [
                'namespaces' => [
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ],
            ],
        ];
    }

    public function getConsoleUsage(AdapterInterface $console)
    {
        return [
            'run import [--name=<name>]' => 'Lance un import par son nom',
            ['<name>', "Nom unique de l'import (clé de config 'name')"],

            'run import' => 'Lance tous les imports',
            [],

            'run synchro [--name=<name>]' => 'Lance une synchronisation par son nom',
            ['<name>', "Nom unique de la synchronisation (clé de config 'name')"],

            'run synchro' => 'Lance toutes les synchronisations',
            [],
        ];
    }
}
