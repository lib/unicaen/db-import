<?php

namespace UnicaenDbImportUnitTest\Service;

use PHPUnit_Framework_TestCase;
use UnicaenDbImport\Config\Config;
use UnicaenDbImport\Domain\ImportInterface;
use UnicaenDbImport\Importer\AdapterInterface;
use UnicaenDbImport\Service\ImportService;

class ImportServiceTest extends PHPUnit_Framework_TestCase
{
    /**
     * @throws \UnicaenDbImport\Service\Exception\DatabaseServiceException
     */
    public function test_can_run_all_imports()
    {
        $import1 = $this->createMock(ImportInterface::class);
        $import2 = $this->createMock(ImportInterface::class);
        $import1->expects($this->once())->method('getName')->willReturn('import 1 name');
        $import2->expects($this->once())->method('getName')->willReturn('import 2 name');

        $importer1 = $this->createMock(AdapterInterface::class);
        $importer1->expects($this->once())->method('setImport')->withConsecutive($import1);
        $importer1->expects($this->once())->method('run')->willReturn(['results 1']);

        $importer2 = $this->createMock(AdapterInterface::class);
        $importer2->expects($this->once())->method('setImport')->withConsecutive($import2);
        $importer2->expects($this->once())->method('run')->willReturn(['results 2']);

        /** @var \PHPUnit_Framework_MockObject_MockObject|Config $config */
        $config = $this->createMock(Config::class);
        $config->expects($this->once())->method('getImports')->willReturn([$import1, $import2]);
        $config->expects($this->exactly(2))->method('getRunnerForImport')->withConsecutive($import1, $import2)
            ->willReturnOnConsecutiveCalls($importer1, $importer2);

        $service = new ImportService($config);
        $results = $service->runAllImports();

        $this->assertEquals([
            'import 1 name' => ['results 1'],
            'import 2 name' => ['results 2']
        ], $results);
    }

    public function test_can_getImports()
    {
        $import = $this->createMock(ImportInterface::class);

        /** @var \PHPUnit_Framework_MockObject_MockObject|Config $config */
        $config = $this->createMock(Config::class);
        $config->expects($this->once())->method('getImports')->willReturn([$import]);

        $service = new ImportService($config);
        $imports = $service->getImports();

        $this->assertCount(1, $imports);
        $this->assertSame($import, $imports[0]);
    }

    public function test_can_run_one_import_by_name()
    {
        $import = $this->createMock(ImportInterface::class);

        /** @var \PHPUnit_Framework_MockObject_MockObject|Config $config */
        $config = $this->createMock(Config::class);
        $config->expects($this->once())->method('getImport')->with('import name')->willReturn($import);

        $service = new ImportService($config);

        $this->assertSame($import, $service->getImportByName('import name'));
    }
}