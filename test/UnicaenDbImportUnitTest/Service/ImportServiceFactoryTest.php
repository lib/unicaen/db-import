<?php

namespace UnicaenDbImportUnitTest\Service;

use UnicaenDbImport\Config\Config;
use UnicaenDbImport\Service\ImportService;
use UnicaenDbImport\Service\ImportServiceFactory;
use PHPUnit_Framework_TestCase;
use Laminas\ServiceManager\ServiceLocatorInterface;

class ImportServiceFactoryTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var ImportServiceFactory
     */
    private $factory;

    protected function setUp()
    {
        parent::setUp();

        $this->factory = new ImportServiceFactory();
    }

    /**
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function test_factory_returns_service()
    {
        $config = $this->createMock(Config::class);

        /** @var \PHPUnit_Framework_MockObject_MockObject|ServiceLocatorInterface $sl */
        $sl = $this->getMockForAbstractClass(ServiceLocatorInterface::class);
        $sl->expects($this->atLeastOnce())->method('get')->with(Config::class)->willReturn($config);

        $service = $this->factory->__invoke($sl);

        $this->assertInstanceOf(ImportService::class, $service);
    }
}