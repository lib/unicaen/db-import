<?php

use Doctrine\DBAL\Platforms\PostgreSqlPlatform;
use UnicaenDbImport\Importer\PostgreSQL\PostgreSQLRunner;

return [
    'import' => [
        'importers' => [
            PostgreSqlPlatform::class => PostgreSQLRunner::class,
        ],
        'imports' => [
            [
                'source' => [
                    'name'               => 'src_ztemptable',
                    'table'              => 'src_ztemptable',
                    'connection'         => 'doctrine.connection.orm_default',
                    'source_code_column' => 'code',
                    'columns'            => ['libelle', 'debut_validite', 'fin_validite'],
                ],
                'destination' => [
                    'name'               => 'ztemptable',
                    'table'              => 'ztemptable',
                    'connection'         => 'doctrine.connection.orm_default',
                    'source_code_column' => 'code',
                    'columns'            => ['libelle', 'debut_validite', 'fin_validite'],
                ],
            ],
            [
                'source' => [
                    'name'   => 'select sifac',
                    'select' => 'SELECT COL...',
                    'connection' => 'doctrine.connection.orm_sifac',
                    'source_code_column' => 'CODE',
                    'columns' => ['LIBELLE', 'FLECHE', 'DEBUT_VALIDITE', 'FIN_VALIDITE'],
                ],
                'destination' => [
                    'name'               => 'ztemptable',
                    'table'              => 'ztemptable',
                    'connection'         => 'doctrine.connection.orm_default',
                    'source_code_column' => 'code',
                    'columns'            => ['libelle', 'debut_validite', 'fin_validite'],
                ],
            ],
        ],
    ],

];