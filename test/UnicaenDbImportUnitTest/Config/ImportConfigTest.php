<?php

namespace UnicaenDbImportUnitTest\Config;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Platforms\MySQL57Platform;
use Doctrine\DBAL\Platforms\MySqlPlatform;
use Doctrine\DBAL\Platforms\PostgreSqlPlatform;
use PHPUnit_Framework_TestCase;
use UnicaenDbImport\Config\Config;
use UnicaenDbImport\Domain\Destination;
use UnicaenDbImport\Domain\Import;
use UnicaenDbImport\Domain\ImportInterface;

class ConfigTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var Config
     */
    protected $config;

    protected function setUp()
    {
        parent::setUp();

        $this->config = new Config();
    }

    /**
     * @expectedException \UnicaenDbImport\Domain\Exception\NotFoundException
     */
    public function test_getting_import_with_unknown_name_throws_exception()
    {
        /** @var \PHPUnit_Framework_MockObject_MockObject|\UnicaenDbImport\Domain\Import $import */
        $import = $this->createMock(ImportInterface::class);
        $import->expects($this->once())->method('getName')->willReturn('import name');

        $this->config->setImports([$import]);

        $this->config->getDbImport('unexisting name');
    }

    public function test_can_retrieve_import_by_name()
    {
        /** @var \PHPUnit_Framework_MockObject_MockObject|\UnicaenDbImport\Domain\Import $import */
        $import = $this->createMock(ImportInterface::class);
        $import->expects($this->once())->method('getName')->willReturn('import name');

        $this->config->setImports([$import]);

        self::assertSame($import, $this->config->getDbImport('import name'));
    }

    public function test_importer_getter_returns_null_when_importers_list_is_empty()
    {
        /** @var MySqlPlatform $platform */
        $platform = $this->createMock(MySqlPlatform::class);

        $this->config->setRunner([]);

        $importer = $this->config->getAdapterForDatabasePlatform($platform);

        $this->assertNull($importer);
    }

    public function test_importer_getter_returns_null_when_platform_is_not_found()
    {
        /** @var MySQL57Platform $unexpectedPlatform */
        $unexpectedPlatform = $this->createMock(MySQL57Platform::class);

        $this->config->setRunner([
            PostgreSqlPlatform::class => 'Importer instance',
        ]);

        $importer = $this->config->getAdapterForDatabasePlatform($unexpectedPlatform);

        $this->assertNull($importer);
    }

    public function test_importer_getter_returns_importer_corresponding_to_requested_platform_class()
    {
        /** @var PostgreSqlPlatform $platform */
        $platform = $this->createMock(PostgreSqlPlatform::class);

        $this->config->setRunner([
            MySQL57Platform::class    => 'Importer instance 1',
            PostgreSqlPlatform::class => 'Importer instance 2',
        ]);

        $importer = $this->config->getAdapterForDatabasePlatform($platform);

        $this->assertEquals('Importer instance 2', $importer);
    }

    /**
     * @expectedException \RuntimeException
     */
    public function test_importer_getter_throws_exception_when_destination_provides_null_database_platform()
    {
        $destination = $this->createDestinationMock($platform = null);

        /** @var \PHPUnit_Framework_MockObject_MockObject|\UnicaenDbImport\Domain\Import $import */
        $import = $this->createMock(Import::class);
        $import->expects($this->once())->method('getDestination')->willReturn($destination);

        $this->config->getRunnerForImport($import);
    }

    /**
     * @expectedException \RuntimeException
     */
    public function test_importer_getter_throws_exception_when_config_returns_null_importer()
    {
        $unexpectedPlatform = $this->createMock(AbstractPlatform::class);
        $destination = $this->createDestinationMock($unexpectedPlatform);

        /** @var \PHPUnit_Framework_MockObject_MockObject|\UnicaenDbImport\Domain\Import $import */
        $import = $this->createMock(Import::class);
        $import->expects($this->once())->method('getDestination')->willReturn($destination);

        $this->config->getRunnerForImport($import);
    }

    public function test_importer_getter_returns_importer_corresponding_to_import()
    {
        $platformClass = PostgreSqlPlatform::class;

        $platform = $this->createMock($platformClass);
        $destination = $this->createDestinationMock($platform);

        /** @var \PHPUnit_Framework_MockObject_MockObject|Import $import */
        $import = $this->createMock(Import::class);
        $import->expects($this->once())->method('getDestination')->willReturn($destination);

        $this->config->setRunner([
            $platformClass => 'Importer instance',
        ]);
        $importer = $this->config->getRunnerForImport($import);

        $this->assertEquals('Importer instance', $importer);
    }

    private function createDestinationMock($platform)
    {
        $connection = $this->createMock(Connection::class);
        $connection->expects($this->once())->method('getDatabasePlatform')->willReturn($platform);

        $destination = $this->createMock(Destination::class);
        $destination->expects($this->once())->method('getConnection')->willReturn($connection);

        return $destination;
    }
}