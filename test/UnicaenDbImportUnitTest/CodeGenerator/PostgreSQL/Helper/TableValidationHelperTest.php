<?php

namespace UnicaenDbImportUnitTest\CodeGenerator\PostgreSQL\Helper;

use PHPUnit_Framework_TestCase;
use RuntimeException;
use UnicaenDbImport\CodeGenerator\PostgreSQL\Helper\TableValidationHelper;

class TableValidationHelperTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var TableValidationHelper
     */
    private $tableValidation;
    
    protected function setUp()
    {
        $this->tableValidation = new TableValidationHelper();
    }

    public function test_can_generateSQLForTableExistenceCheck()
    {
        $this->assertContains("table_name = 'ztemptable'",
            $this->tableValidation->generateSQLForTableExistenceCheck('ztemptable'));
    }

    public function test_can_convertTableExistenceCheckResultToBoolean()
    {
        $this->assertTrue($this->tableValidation->convertTableExistenceCheckResultToBoolean([
            0 => ['table_exists' => 1]
        ]));

        $this->assertFalse($this->tableValidation->convertTableExistenceCheckResultToBoolean([
            0 => ['table_exists' => 0]
        ]));

        $this->assertFalse($this->tableValidation->convertTableExistenceCheckResultToBoolean([
            0 => ['other' => 0]
        ]));

        $this->assertFalse($this->tableValidation->convertTableExistenceCheckResultToBoolean([]));
    }

    public function test_can_generateSQLForColumnsValidation()
    {
        $sql = $this->tableValidation->generateSQLForColumnsValidation('table', [
            'libelle',
            'created_on' => 'timestamp',
        ]);
        $expected = <<<EOT
SELECT 'libelle', NULL UNION
SELECT 'created_on', 'timestamp'
EOT;
        $this->assertContains($expected, $sql);
    }

    public function test_can_convertColumnsValidationResultToException()
    {
        // empty results
        $exception = $this->tableValidation->convertColumnsValidationResultToException('table', [], []);
        $this->assertNull($exception);

        // same sized results
        $exception = $this->tableValidation->convertColumnsValidationResultToException('table', ['...'], ['...']);
        $this->assertNull($exception);

        // different sized results
        $exception = $this->tableValidation->convertColumnsValidationResultToException('table', ['...'], ['...', '...']);
        $this->assertInstanceOf(RuntimeException::class, $exception);
    }

    public function test_can_generateSQLForHistoColumnsValidation()
    {
        $sql = $this->tableValidation->generateSQLForHistoColumnsValidation('table');
        $expected = <<<EOT
SELECT 'created_on', 'timestamptz' UNION
SELECT 'updated_on', 'timestamptz' UNION
SELECT 'deleted_on', 'timestamptz'
EOT;
        $this->assertContains($expected, $sql);
    }

    public function test_can_convertHistoColumnsValidationBadResultToException()
    {
        // 3 colonnes trouvées <==> succès de la validation
        $exception = $this->tableValidation->convertHistoColumnsValidationBadResultToException('table', [1, 2, 3]);
        $this->assertNull($exception);

        // sinon une exception doit être retournée
        $exception = $this->tableValidation->convertHistoColumnsValidationBadResultToException('table', [1, 2]);
        $this->assertInstanceOf(RuntimeException::class, $exception);
    }
}
