<?php

namespace UnicaenDbImportUnitTest\CodeGenerator\PostgreSQL\Helper;

use PHPUnit_Framework_TestCase;
use UnicaenDbImport\CodeGenerator\PostgreSQL\Helper\FunctionCreationHelper;

class FunctionCreationHelperTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var FunctionCreationHelper
     */
    private $helper;
    
    protected function setUp()
    {
        $this->helper = new FunctionCreationHelper();
    }

    public function test_can_generateFunctionName()
    {
        $sql = $this->helper->generateFunctionName('ztemptable');

        $expected = 'create_import_metarequest_for_ztemptable';

        self::assertEquals($expected, $sql);
    }

    public function test_can_generateSQL()
    {
        $sql = $this->helper->generateSQL('ztemptable', 'code', ['libelle', 'debut_validite', 'fin_validite']);

        $expected = file_get_contents(__DIR__ . '/expected_function_creation_sql.sql');

        self::assertEquals($expected, $sql);
    }
}
