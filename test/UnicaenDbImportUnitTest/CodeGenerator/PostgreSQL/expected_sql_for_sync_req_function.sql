CREATE OR REPLACE FUNCTION create_import_metarequest_for_ztemptable(
    src_code VARCHAR, src_libelle VARCHAR, src_debut_validite VARCHAR, src_fin_validite VARCHAR,
    dest_code VARCHAR, dest_libelle VARCHAR, dest_debut_validite VARCHAR, dest_fin_validite VARCHAR, dest_deleted_on TIMESTAMP,
    import_hash VARCHAR
) RETURNS VARCHAR AS
$Q$
DECLARE
    operation VARCHAR(64);
    sql TEXT;
BEGIN
    IF (src_code IS NOT NULL AND dest_code IS NULL) THEN
        operation = 'insert';
        sql = concat('INSERT INTO ztemptable(code, libelle, debut_validite, fin_validite) VALUES ($$', src_code, '$$, $$', src_libelle, '$$, $$', src_debut_validite, '$$, $$', src_fin_validite, '$$) ;');
        sql = concat(sql, ' UPDATE import_reg SET executed_on = now() WHERE import_hash = $$', import_hash, '$$ ;');
        INSERT INTO import_reg(operation, table_name, source_code, field_name, to_value, from_value, sql, created_on, import_hash) VALUES ('insert', 'ztemptable', src_code, null, null, null, sql, now(), import_hash);
    END IF;

    IF (src_code IS NOT NULL AND dest_code IS NOT NULL and dest_deleted_on IS NULL) THEN
        IF (src_libelle <> dest_libelle) THEN
            operation = 'update';
            sql = concat('UPDATE ztemptable SET libelle = $$', src_libelle, '$$, updated_on = now() WHERE code = $$', dest_code, '$$ ;');
            sql = concat(sql, ' UPDATE import_reg SET executed_on = now() WHERE import_hash = $$', import_hash, '$$ ;');
            INSERT INTO import_reg(operation, table_name, source_code, field_name, to_value, from_value, sql, created_on, import_hash) VALUES ('update', 'ztemptable', src_code, 'libelle', src_libelle, dest_libelle, sql, now(), import_hash);
        END IF;
        IF (src_debut_validite <> dest_debut_validite) THEN
            operation = 'update';
            sql = concat('UPDATE ztemptable SET debut_validite = $$', src_debut_validite, '$$, updated_on = now() WHERE code = $$', dest_code, '$$ ;');
            sql = concat(sql, ' UPDATE import_reg SET executed_on = now() WHERE import_hash = $$', import_hash, '$$ ;');
            INSERT INTO import_reg(operation, table_name, source_code, field_name, to_value, from_value, sql, created_on, import_hash) VALUES ('update', 'ztemptable', src_code, 'debut_validite', src_debut_validite, dest_debut_validite, sql, now(), import_hash);
        END IF;
        IF (src_fin_validite <> dest_fin_validite) THEN
            operation = 'update';
            sql = concat('UPDATE ztemptable SET fin_validite = $$', src_fin_validite, '$$, updated_on = now() WHERE code = $$', dest_code, '$$ ;');
            sql = concat(sql, ' UPDATE import_reg SET executed_on = now() WHERE import_hash = $$', import_hash, '$$ ;');
            INSERT INTO import_reg(operation, table_name, source_code, field_name, to_value, from_value, sql, created_on, import_hash) VALUES ('update', 'ztemptable', src_code, 'fin_validite', src_fin_validite, dest_fin_validite, sql, now(), import_hash);
        END IF;

    END IF;

    IF (src_code IS NOT NULL AND dest_code IS NOT NULL and dest_deleted_on IS NOT NULL) THEN
        operation = 'undelete';
        sql = concat('UPDATE ztemptable SET libelle = $$', src_libelle, '$$, debut_validite = $$', src_debut_validite, '$$, fin_validite = $$', src_fin_validite, '$$, updated_on = now(), deleted_on = null WHERE code = $$', dest_code, '$$ ;');
        sql = concat(sql, ' UPDATE import_reg SET executed_on = now() WHERE import_hash = $$', import_hash, '$$ ;');
        INSERT INTO import_reg(operation, table_name, source_code, field_name, to_value, from_value, sql, created_on, import_hash) VALUES ('undelete', 'ztemptable', src_code, null, null, null, sql, now(), import_hash);
    END IF;

    IF (src_code IS NULL AND dest_code IS NOT NULL and dest_deleted_on IS NULL) THEN
        operation = 'delete';
        sql = concat('UPDATE ztemptable SET deleted_on = now() WHERE code = $$', dest_code, '$$ ;');
        sql = concat(sql, ' UPDATE import_reg SET executed_on = now() WHERE import_hash = $$', import_hash, '$$ ;');
        INSERT INTO import_reg(operation, table_name, source_code, field_name, to_value, from_value, sql, created_on, import_hash) VALUES ('delete', 'ztemptable', src_code, null, null, null, sql, now(), import_hash);
    END IF;

    return operation;
END; $Q$
LANGUAGE plpgsql;