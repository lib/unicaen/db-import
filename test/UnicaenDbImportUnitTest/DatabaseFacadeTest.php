<?php

namespace UnicaenDbImportUnitTest;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Driver\Statement;
use Exception;
use PHPUnit_Framework_TestCase;
use RuntimeException;
use UnicaenDbImport\CodeGenerator\CodeGenerator;
use UnicaenDbImport\CodeGenerator\PostgreSQL\Helper\TableValidationHelper;
use UnicaenDbImport\Service\DatabaseFacade;
use UnicaenDbImport\Domain\DestinationInterface;
use UnicaenDbImport\Domain\SourceInterface;
use UnicaenDbImport\QueryExecutor;

class DatabaseFacadeTest extends PHPUnit_Framework_TestCase
{
    /** @var DestinationInterface|\PHPUnit_Framework_MockObject_MockObject */
    private $destination;

    /** @var CodeGenerator|\PHPUnit_Framework_MockObject_MockObject */
    private $codeGenerator;

    /** @var QueryExecutor|\PHPUnit_Framework_MockObject_MockObject */
    private $queryExecutor;

    /** @var SourceInterface|\PHPUnit_Framework_MockObject_MockObject */
    private $source;

    /** @var TableValidationHelper|\PHPUnit_Framework_MockObject_MockObject */
    private $tableValidationHelper;

    /** @var Connection|\PHPUnit_Framework_MockObject_MockObject */
    private $connection;

    /** @var DatabaseFacade */
    private $databaseFacade;

    protected function setUp()
    {
        $this->codeGenerator = $this->createMock(CodeGenerator::class);
        $this->queryExecutor = $this->createMock(QueryExecutor::class);
        $this->source = $this->createMock(SourceInterface::class);
        $this->destination = $this->createMock(DestinationInterface::class);
        $this->connection = $this->createMock(Connection::class);
        $this->tableValidationHelper = $this->createMock(TableValidationHelper::class);

        $this->databaseFacade = new DatabaseFacade($this->codeGenerator, $this->queryExecutor);
    }

    /**
     * @expectedException RuntimeException
     */
    public function test_validateSourceTable_throws_exception_if_table_not_found()
    {
        $this->source->expects($this->once())->method('getConnection')->willReturn($this->connection);
        $this->source->expects($this->once())->method('getTable')->willReturn('table');
        $this->source->expects($this->once())->method('getColumns')->willReturn(['columns']);

        $this->codeGenerator->expects($this->once())->method('generateSQLForTableExistenceCheck');
        $this->queryExecutor->expects($this->once())->method('fetchAll')->willReturn(['result']);
        $this->codeGenerator->expects($this->once())->method('convertTableExistenceCheckResultToBoolean')->willReturn(false);

        $this->databaseFacade->validateSourceTable($this->source);
    }

    /**
     * @expectedException RuntimeException
     */
    public function test_validateSourceTable_throws_exception_if_columns_are_invalid()
    {
        $this->source->expects($this->once())->method('getConnection')->willReturn($this->connection);
        $this->source->expects($this->once())->method('getTable')->willReturn('table');
        $this->source->expects($this->once())->method('getColumns')->willReturn(['columns']);

        $this->queryExecutor->expects($this->exactly(2))->method('fetchAll')->willReturn(['result']);

        // table exists
        $this->codeGenerator->expects($this->once())->method('generateSQLForTableExistenceCheck');
        $this->codeGenerator->expects($this->once())->method('convertTableExistenceCheckResultToBoolean')->willReturn(true);

        $this->codeGenerator->expects($this->once())->method('generateSQLForDataColumnsValidation');
        $this->codeGenerator->expects($this->once())->method('convertDataColumnsValidationBadResultToException')->willReturn(new Exception());

        $this->databaseFacade->validateSourceTable($this->source);
    }

    /**
     * @expectedException RuntimeException
     */
    public function test_validateDestinationTable_throws_exception_if_table_not_found()
    {
        $this->destination->expects($this->once())->method('getConnection')->willReturn($this->connection);
        $this->destination->expects($this->once())->method('getTable')->willReturn('table');
        $this->destination->expects($this->once())->method('getColumns')->willReturn(['columns']);

        $this->codeGenerator->expects($this->once())->method('generateSQLForTableExistenceCheck');
        $this->queryExecutor->expects($this->once())->method('fetchAll')->willReturn(['result']);
        $this->codeGenerator->expects($this->once())->method('convertTableExistenceCheckResultToBoolean')->willReturn(false);

        $this->databaseFacade->validateDestinationTable($this->destination);
    }

    /**
     * @expectedException RuntimeException
     */
    public function test_validateSourceTable_throws_exception_if_config_columns_not_found()
    {
        $this->destination->expects($this->once())->method('getConnection')->willReturn($this->connection);
        $this->destination->expects($this->once())->method('getTable')->willReturn('table');
        $this->destination->expects($this->once())->method('getColumns')->willReturn(['columns']);

        $this->queryExecutor->expects($this->exactly(2))->method('fetchAll')->willReturn(['result']);

        // table exists
        $this->codeGenerator->expects($this->once())->method('generateSQLForTableExistenceCheck');
        $this->codeGenerator->expects($this->once())->method('convertTableExistenceCheckResultToBoolean')->willReturn(true);

        $this->codeGenerator->expects($this->once())->method('generateSQLForDataColumnsValidation');
        $this->codeGenerator->expects($this->once())->method('convertDataColumnsValidationBadResultToException')->willReturn(new Exception());

        $this->databaseFacade->validateDestinationTable($this->destination);
    }

    /**
     * @expectedException RuntimeException
     */
    public function test_validateSourceTable_throws_exception_if_histo_columns_not_valid()
    {
        $this->destination->expects($this->once())->method('getConnection')->willReturn($this->connection);
        $this->destination->expects($this->once())->method('getTable')->willReturn('table');
        $this->destination->expects($this->once())->method('getColumns')->willReturn(['columns']);

        $this->queryExecutor->expects($this->exactly(3))->method('fetchAll')->willReturn(['result']);

        // table exists
        $this->codeGenerator->expects($this->once())->method('generateSQLForTableExistenceCheck');
        $this->codeGenerator->expects($this->once())->method('convertTableExistenceCheckResultToBoolean')->willReturn(true);

        // data columns valid
        $this->codeGenerator->expects($this->once())->method('generateSQLForDataColumnsValidation');
        $this->codeGenerator->expects($this->once())->method('convertDataColumnsValidationBadResultToException')->willReturn(null);

        $this->codeGenerator->expects($this->once())->method('generateSQLForHistoColumnsValidation');
        $this->codeGenerator->expects($this->once())->method('convertHistoColumnsValidationBadResultToException')->willReturn(new Exception());

        $this->databaseFacade->validateDestinationTable($this->destination);
    }

    public function test_createImportRegTableIfNotExists_does_not_create_table_if_it_exists()
    {
        $this->destination->expects($this->once())->method('getConnection')->willReturn($this->connection);

        $this->codeGenerator->expects($this->once())->method('generateSQLForTableExistenceCheck');
        $this->queryExecutor->expects($this->once())->method('fetchAll')->willReturn(['result']);
        $this->codeGenerator->expects($this->once())->method('convertTableExistenceCheckResultToBoolean')->willReturn(true);

        $this->codeGenerator->expects($this->never())->method('generateSQLForLogTableCreation');

        $this->databaseFacade->createLogTableIfNotExists($this->destination);
    }

    public function test_can_createImportRegTableIfNotExists()
    {
        $this->destination->expects($this->once())->method('getConnection')->willReturn($this->connection);

        // table not found
        $this->codeGenerator->expects($this->once())->method('generateSQLForTableExistenceCheck');
        $this->queryExecutor->expects($this->once())->method('fetchAll')->willReturn(['result']);
        $this->codeGenerator->expects($this->once())->method('convertTableExistenceCheckResultToBoolean')->willReturn(false);

        $this->codeGenerator->expects($this->once())->method('generateSQLForLogTableCreation')->willReturn('SQL');
        $this->queryExecutor->expects($this->once())->method('exec')->with('SQL');

        $this->databaseFacade->createLogTableIfNotExists($this->destination);
    }

    public function test_can_create_meta_request_creation_function()
    {
        $this->codeGenerator->expects($this->once())->method('generateImportMetaRequestFunctionCreationSQL')->willReturn('CREATE');
        $this->destination->expects($this->once())->method('getConnection')->willReturn($this->connection);
        $this->queryExecutor->expects($this->once())->method('exec')->with('CREATE');

        $this->databaseFacade->createMetaRequestCreationFunction($this->source, $this->destination);
    }

    /**
     * @expectedException RuntimeException
     */
    public function test_can_checkIntermediateTableNotExists_throws_exception_if_table_exists()
    {
        $this->destination->expects($this->once())->method('getConnection')->willReturn($this->connection);

        $this->codeGenerator->expects($this->once())->method('generateSQLForTableExistenceCheck')->with('interm_table');
        $this->queryExecutor->expects($this->once())->method('fetchAll')->willReturn(['result']);
        $this->codeGenerator->expects($this->once())->method('convertTableExistenceCheckResultToBoolean')->willReturn(true);

        $this->databaseFacade->checkIntermediateTableNotExists($this->destination, 'interm_table');
    }
    
    public function test_can_create_intermediate_table()
    {
        $this->codeGenerator->expects($this->once())->method('generateSQLForIntermediateTableCreation')->willReturn('CREATE');
        $this->destination->expects($this->once())->method('getConnection')->willReturn($this->connection);
        $this->queryExecutor->expects($this->once())->method('exec')->with('CREATE', $this->connection);

        $this->databaseFacade->createIntermediateTable('interm_ztemptable', $this->source, $this->destination);
    }

    public function test_can_populate_intermediate_table()
    {
        $this->codeGenerator->expects($this->once())->method('generateSQLForSelectFromSource')->willReturn('SQL 1');

        $this->queryExecutor->expects($this->once())->method('fetchAll')->with('SQL 1')->willReturn([
            ['CODE' => '...'], // NB: l'importer accepte aussi 'code' (en minuscule)
            ['CODE' => '...'],
        ]);

        $this->codeGenerator->expects($this->exactly(2))->method('generateSQLForInsertIntoIntermmediateTable')
            ->willReturnOnConsecutiveCalls('INSERT 1', 'INSERT 2');

        $this->source->expects($this->once())->method('getSourceCodeColumn')->willReturn('code');
        $this->source->expects($this->once())->method('getColumns')->willReturn(['code']);
        $this->source->expects($this->once())->method('getConnection')->willReturn($this->connection);

        $this->destination->expects($this->once())->method('getConnection')->willReturn($this->connection);

        $expectedSql = <<<EOT
INSERT 1
INSERT 2

EOT;
        $this->queryExecutor->expects($this->once())->method('exec')->with($expectedSql);

        $this->databaseFacade->populateTableFromSource('interm_ztemptable', $this->source, $this->destination);
    }

    public function test_can_dropIntermediateTable()
    {
        $this->codeGenerator->expects($this->once())->method('generateSQLForIntermmediateTableDrop')->willReturn('DELETE');
        $this->destination->expects($this->once())->method('getConnection')->willReturn($this->connection);
        $this->queryExecutor->expects($this->once())->method('exec')->with('DELETE');

        $this->databaseFacade->dropIntermediateTable('interm_ztemptable', $this->destination);
    }

    public function test_can_execute_diff_request()
    {
        $this->codeGenerator->expects($this->once())->method('generateSourceAndDestinationDiffSelectSQL')->willReturn('SELECT');

        $this->queryExecutor->expects($this->once())->method('fetchAll')->with('SELECT')->willReturn(['result']);

        $this->destination->expects($this->once())->method('getConnection')->willReturn($this->connection);

        $result = $this->databaseFacade->executeDiffRequest($this->source, $this->destination);

        $this->assertEquals(['result'], $result);
    }

    public function test_can_execute_import_requests()
    {
        $this->destination->expects($this->once())->method('getConnection')->willReturn($this->connection);
        $this->queryExecutor->expects($this->once())->method('exec')->with('INSERTS');

        $this->databaseFacade->executeImportRequests('INSERTS', $this->destination);
    }

    public function test_can_fetchImportRegister()
    {
        $this->destination->expects($this->once())->method('getConnection')->willReturn($this->connection);
        $this->codeGenerator->expects($this->once())->method('generateSQLForFetchingLogTable')->willReturn('SELECT');
        $this->queryExecutor->expects($this->once())->method('fetchAll')->with('SELECT');

        $this->databaseFacade->fetchLogTable($this->destination);
    }

    private function createStatementMockForTableSynchronisationSelectRequestResult()
    {
        /** @var Statement|\PHPUnit_Framework_MockObject_MockObject $statement */
        $statement = $this->createMock(Statement::class);
        $statement->expects($this->once())->method('fetchAll')->willReturn([
            ['operation' => 'update', 'req' => 'SQL UPDATE 1'],
            ['operation' => 'update', 'req' => 'SQL UPDATE 2'],
            ['operation' => 'insert', 'req' => 'SQL INSERT 1'],
        ]);

        return $statement;
    }

    private function createStatementMockForPopulateIntermediateTableResult()
    {
        /** @var Statement|\PHPUnit_Framework_MockObject_MockObject $statement */
        $statement = $this->createMock(Statement::class);
        $statement->expects($this->atLeastOnce())->method('fetch')->willReturnOnConsecutiveCalls(
            ['CODE' => '...'], // NB: l'importer accepte aussi 'code' (en minuscule)
            FALSE
        );

        return $statement;
    }
}