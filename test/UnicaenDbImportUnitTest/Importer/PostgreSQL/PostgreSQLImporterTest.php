<?php

namespace UnicaenDbImportUnitTest\Importer\PostgreSQL;

use UnicaenDbImport\Importer\PostgreSQL\PostgreSQLRunner;
use UnicaenDbImportUnitTest\Importer\ImporterAbstractTest;

class PostgreSQLImporterTest extends ImporterAbstractTest
{
    protected function setUp()
    {
        parent::setUp();

        $this->importer = new PostgreSQLRunner($this->databaseFacade);
    }
}