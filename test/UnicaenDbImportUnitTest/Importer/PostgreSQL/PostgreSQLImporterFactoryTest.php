<?php

namespace UnicaenDbImportUnitTest\Importer\PostgreSQL;

use PHPUnit_Framework_TestCase;
use UnicaenDbImport\Importer\PostgreSQL\PostgreSQLRunner;
use UnicaenDbImport\Importer\PostgreSQL\PostgreSQLRunnerFactory;
use Laminas\ServiceManager\ServiceLocatorInterface;

class PostgreSQLImporterFactoryTest extends PHPUnit_Framework_TestCase
{
    public function test_can_create_service()
    {
        /** @var ServiceLocatorInterface|\PHPUnit_Framework_MockObject_MockObject $sl */
        $sl = $this->createMock(ServiceLocatorInterface::class);
        $sl->expects($this->never())->method('get');

        $factory = new PostgreSQLRunnerFactory();
        $service = $factory->__invoke($sl);

        $this->assertInstanceOf(PostgreSQLRunner::class, $service);
    }
}