<?php

namespace UnicaenDbImport;

use stdClass;

class Utils
{
    /**
     * Représentation littérale d'un tableau associatif.
     *
     * @param array $array
     * @param string $separator
     * @return string
     */
    static public function associativeArrayToString(array $array, string $separator = ', '): string
    {
        return implode($separator, array_map(function (string $key) use ($array) {
            $value = $array[$key];
            if (is_object($value)) {
                $value = $value instanceof stdClass ?
                    '[' . static::associativeArrayToString(get_object_vars($value)) . ']' :
                    get_class($value);
            }
            return $key . ' => ' . $value;
        }, array_keys($array)));
    }
}