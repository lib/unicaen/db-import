<?php

namespace UnicaenDbImport\Domain;

class SynchroResult extends Result
{
    /**
     * @var SynchroInterface
     */
    private $synchro;

    /**
     * @param SynchroInterface $synchro
     * @return static
     */
    public function setSynchro(SynchroInterface $synchro): self
    {
        $this->synchro = $synchro;

        return $this;
    }

    /**
     * @return SynchroInterface
     */
    public function getSynchro(): SynchroInterface
    {
        return $this->synchro;
    }
}