<?php

namespace UnicaenDbImport\Domain;

use Assert\AssertionChain;
use Assert\InvalidArgumentException;
use Doctrine\DBAL\Connection as DbConnection;
use Laminas\Stdlib\Parameters;
use RuntimeException;
use stdClass;
use UnicaenDbImport\Config\ConfigException;
use UnicaenDbImport\Connection\ApiConnection;
use UnicaenDbImport\Connection\NoConnection;
use UnicaenDbImport\Filter\ColumnName\ColumnNameFilterInterface;
use UnicaenDbImport\Filter\ColumnValue\ColumnValueFilterInterface;

class Source implements SourceInterface
{
    protected $config;

    /**
     * @var array
     */
    protected $columns = [];

    /**
     * @var array Le résultat de la requête associée à la source ('select' ou 'table')
     */
    protected $data = [];

    /**
     * @param array $config
     * @return static
     * @throws \UnicaenDbImport\Config\ConfigException
     */
    static public function fromConfig(array $config): self
    {
        $inst = new static($config);

        try {
            $inst->validateConfig();
        } catch (InvalidArgumentException $e) {
            throw new ConfigException($e->getMessage());
        }

        return $inst;
    }

    /**
     * Source constructor.
     *
     * @param array $config
     */
    protected function __construct(array $config)
    {
        $this->config = new Parameters($config);

        if ($columns = $this->config->get('columns')) {
            $this->setColumns($columns);
        }
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return implode(' -- ', array_filter([
            $this->getSelect() ?: $this->getTable(),
            $this->getWhere() ? ' WHERE ' . $this->getWhere() : null,
        ]));
    }

    /**
     * {@inheritDoc}
     */
    public function getName(): string
    {
        return $this->config->get('name');
    }

    /**
     * {@inheritDoc}
     */
    public function getCode(): ?string
    {
        return $this->config->get('code');
    }

    /**
     * {@inheritDoc}
     */
    public function getTable(): ?string
    {
        return $this->config->get('table');
    }

    /**
     * {@inheritDoc}
     */
    public function getSelect(): ?string
    {
        return $this->config->get('select');
    }

    /**
     * {@inheritDoc}
     */
    public function getWhere(): ?string
    {
        return $this->config->get('where');
    }

    /**
     * {@inheritDoc}
     */
    public function getConnection()
    {
        return $this->config->get('connection');
    }

    /**
     * {@inheritDoc}
     */
    public function getSourceCodeColumn(): string
    {
        return $this->config->get('source_code_column');
    }

    /**
     * {@inheritDoc}
     */
    public function getColumns(): array
    {
        return $this->columns;
    }

    /**
     * {@inheritDoc}
     */
    public function setColumns(array $columns)
    {
        $this->columns = $this->normalizeColumns($columns);
    }

    /**
     * {@inheritDoc}
     */
    public function getComputedColumns(): array
    {
        return $this->config->get('computed_columns', []);
    }

    /**
     * {@inheritDoc}
     */
    public function getColumnNameFilter() : ?ColumnNameFilterInterface
    {
        return $this->config->get('column_name_filter');
    }

    /**
     * {@inheritDoc}
     */
    public function getColumnValueFilter() : array
    {
        return $this->config->get('column_value_filter', []);
    }

    /**
     * @inheritDoc
     */
    public function getExtra(): array
    {
        return $this->config->get('extra', []);
    }

    /**
     * {@inheritDoc}
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * {@inheritDoc}
     */
    public function setData(array $data)
    {
        $this->data = $this->normalizeData($data);
    }

    /**
     * {@inheritDoc}
     */
    public function getPageSize(): int
    {
        return (int) $this->config->get('page_size');
    }

    /**
     * @throws \Assert\InvalidArgumentException
     * @throws \UnicaenDbImport\Config\ConfigException
     */
    protected function validateConfig(): void
    {
        (new AssertionChain($this->config->get($key = 'name'), "Une string non vide est requise pour la clé suivante: $key"))
            ->notEmpty()
            ->string();

        if ($this->config->offsetExists($key = 'code')) {
            (new AssertionChain($this->config->get($key), "Une string non vide est requise pour la clé suivante: $key"))
                ->notEmpty()
                ->string();
        }

        if ($table = $this->config->get($key = 'table'))
            (new AssertionChain($table, "Une string non vide est requise pour la clé suivante: $key"))
                ->notEmpty()
                ->string();

        if ($select = $this->config->get($key = 'select'))
            (new AssertionChain($select, "Une string non vide est requise pour la clé suivante: $key"))
                ->notEmpty()
                ->string();

        if ($this->config->offsetExists($key = 'columns')) {
            (new AssertionChain($this->config->get($key), "Un tableau non vide contenant uniquement des chaînes de caractères est requis pour la clé suivante: $key"))
                ->isArray()
                ->notEmpty()
                ->all()
                ->string();
        }

        if ($computedColumns = $this->config->get($key = 'computed_columns')) {
            (new AssertionChain($computedColumns, "Un tableau contenant uniquement des " . ColumnValueFilterInterface::class . " est requis pour la clé facultative suivante: $key"))
                ->isArray()
                ->all()
                ->isInstanceOf(ColumnValueFilterInterface::class);
        }

        if ($table && $select) {
            throw ConfigException::exclusiveKeys(['table', 'select']);
        }
        if (!$table && !$select) {
            throw ConfigException::atLeastOneKey(['table', 'select']);
        }

        if ($where = $this->config->get($key = 'where'))
            (new AssertionChain($where, "Une string non vide est requise pour la clé suivante: $key"))
                ->notEmpty()
                ->string();

        $conn = $this->config->get($key = 'connection');
        if (! $conn instanceof DbConnection && ! $conn instanceof ApiConnection && ! $conn instanceof NoConnection) {
            throw new InvalidArgumentException(
                "Une instance de l'une des classes suivantes est requise pour la clé '$key' : " .
                implode(', ', [DbConnection::class, ApiConnection::class, NoConnection::class]),
                0, null, null
            );
        }

        (new AssertionChain($this->config->get($key = 'source_code_column'), "Une string non vide est requise pour la clé suivante: $key"))
            ->notEmpty()
            ->string();

        if ($column_value_filter = $this->config->get($key = 'column_value_filter')) {
            (new AssertionChain($column_value_filter, "Un tableau contenant uniquement des " . ColumnValueFilterInterface::class . " est requis pour la clé facultative suivante: $key"))
                ->isArray()
                ->all()
                ->isInstanceOf(ColumnValueFilterInterface::class);
        }

        if ($this->config->offsetExists($key = 'extra')) {
            (new AssertionChain($this->config->get($key), "Un tableau est requis pour la clé suivante: $key"))
                ->isArray();
        }
    }

    /**
     * Normalise et valide les colonnes des données sources.
     *
     * @param array $columns
     * @return array
     */
    private function normalizeColumns(array $columns): array
    {
        $sourceCodeColumn = $this->getSourceCodeColumn();

        // colonnes calculées
        $columns = array_merge($columns, array_keys($this->getComputedColumns()));

        // application du filtre de transformation éventuel
        $columnNameFilter = $this->getColumnNameFilter();
        if ($columnNameFilter !== null) {
            $columns = array_map([$columnNameFilter, 'filter'], $columns);
        }

        // recherche puis retrait de la "source code column" (colonne discriminante)
        if (is_int($index = array_search(strtolower($sourceCodeColumn), $columns)) ||
            is_int($index = array_search(strtoupper($sourceCodeColumn), $columns))) {
            unset($columns[$index]);
        } else {
            throw new RuntimeException(
                "La colonne ou l'attribut de référence '$sourceCodeColumn' est introuvable dans les colonnes/attributs de la source");
        }

        // la source ne peut pas comporter de colonne nommée comme l'une des colonnes d'historique
        foreach (DestinationInterface::HISTO_COLUMNS as $col) {
            if (in_array($col, $columns)) {
                throw new RuntimeException(sprintf(
                    "La source '%s' ne peut pas comporter de colonne nommée '%s'",
                    $this->getCode(),
                    $col
                ));
            }
        }

        if (empty($columns)) {
            throw new RuntimeException(sprintf(
                "La source '%s' doit comporter au moins une colonne *en plus* de la colonne de référence '%s'",
                $this->getCode(),
                $sourceCodeColumn
            ));
        }

        return $columns;
    }

    /**
     * Normalise les données sources : transforme les éventuels `stdClass` en `array`.
     *
     * @param array $data
     * @return array
     */
    private function normalizeData(array $data): array
    {
        if (empty($data)) {
            return $data;
        }

        $first = reset($data);
        if (is_array($first)) {
            return $data;
        }
        if (! $first instanceof stdClass) {
            throw new RuntimeException(
                "Impossible de normaliser les données sources car leur type est inconnu (ni array ni stdClass)");
        }

        array_walk($data, function(&$value) {
            $value = get_object_vars($value);
        });

        return $data;
    }
}