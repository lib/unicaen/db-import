<?php

namespace UnicaenDbImport\Domain;

use DateTime;
use Exception;

interface ResultInterface
{
    /**
     * Retourne l'exception ayant fait échouer totalement l'opération.
     *
     * @return Exception|null
     */
    public function getFailureException(): ?Exception;

    /**
     * Spécifie l'exception ayant fait échouer totalement l'opération.
     *
     * @param \Exception|null $failureException
     */
    public function setFailureException(Exception $failureException = null);

    /**
     * Spécifie l'instant de démarrage de l'opération.
     *
     * @param DateTime $startDate
     */
    public function setStartDate(DateTime $startDate);

    /**
     * Spécifie l'instant de fin de l'opération.
     *
     * @param DateTime $endDate
     */
    public function setEndDate(DateTime $endDate);

    /**
     * Retourne l'instant de démarrage de l'opération.
     *
     * @return DateTime
     */
    public function getStartDate(): DateTime;

    /**
     * Retourne l'instant de fin de l'opération.
     *
     * @return DateTime
     */
    public function getEndDate(): DateTime;

    /**
     * Retourne une représentation littérale de ce résultat.
     *
     * @param bool $includeDetails
     * @return string
     */
    public function toString(bool $includeDetails = false): string;

    /**
     * Retourne le hash attribué à ce résultat.
     *
     * @return string
     */
    public function getHash(): string;
}