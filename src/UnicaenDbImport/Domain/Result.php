<?php

namespace UnicaenDbImport\Domain;

use DateTime;
use Exception;
use Generator;
use Ramsey\Uuid\Uuid;
use RuntimeException;

abstract class Result implements ResultInterface
{
    /**
     * Exception ayant fait échouer totalement l'opération.
     *
     * @var Exception
     */
    private $failureException;

    /**
     * @var array [operation => int|Exception]
     */
    private $resultsByOperation = [];

    /**
     * @var bool
     */
    private $hasExceptionInResults = false;

    /**
     * @var DateTime
     */
    private $startDate;

    /**
     * @var DateTime
     */
    private $endDate;

    /**
     * @var string
     */
    private $hash;

    /**
     * @param \Exception $failureException
     * @return \Generator
     */
    public static function getFailureExceptionGenerator(Exception $failureException): Generator
    {
        $e = $failureException;
        while ($e !== null) {
            yield $e;
            $e = $e->getPrevious();
        }
    }

    /**
     * @param array $results [string => int|Exception]
     */
    public function setResultsByOperation(array $results)
    {
        $this->hasExceptionInResults = false;

        foreach ($results as $operation => $result) {
            /** @var int|Exception $result */
            $this->setResultForOperation($result, $operation);
        }
    }

    /**
     * @param int|Exception $result
     * @param string $operation Ex: {@see Operation::OPERATION_INSERT}
     */
    public function setResultForOperation($result, string $operation)
    {
        $this->resultsByOperation[$operation] = $result;

        if ($result instanceof Exception) {
            $this->hasExceptionInResults = true;
        }
    }

    /**
     * @return Exception|null
     */
    public function getFailureException(): ?Exception
    {
        return $this->failureException;
    }

    /**
     * @param \Exception|null $failureException
     */
    public function setFailureException(Exception $failureException = null)
    {
        $this->failureException = $failureException;
    }

    /**
     * @return bool
     */
    public function hasExceptionInResults(): bool
    {
        return $this->hasExceptionInResults;
    }

    /**
     * @return DateTime
     */
    public function getStartDate(): DateTime
    {
        return $this->startDate;
    }

    /**
     * @param \DateTime|null $startDate
     */
    public function setStartDate(DateTime $startDate = null)
    {
        $this->startDate = $startDate ?: date_create();
    }

    /**
     * @return DateTime
     */
    public function getEndDate(): DateTime
    {
        return $this->endDate;
    }

    /**
     * @param \DateTime|null $endDate
     */
    public function setEndDate(DateTime $endDate = null)
    {
        $this->endDate = $endDate ?: date_create();
    }

    /**
     * @return string
     */
    public function getDurationToString(): string
    {
        $diffDate = date_diff($this->startDate, $this->endDate);
        $microseconds = (float) $diffDate->format('%f');

        return sprintf('%0.2f s', $microseconds / 1000000.0);
    }

    /**
     * @param bool $includeDetails
     * @return string
     */
    public function toString(bool $includeDetails = false): string
    {
        if (count($this->resultsByOperation) === 0) {
            return "Aucune instruction exécutée.";
        }

        $str = '';
        foreach ($this->resultsByOperation as $operation => $result) {
            $str .= "# " . sprintf('%-12s', $operation . " : ");
            if (is_array($result)) {
                $str .= $result['count'] . " instructions exécutées.";
            } elseif ($result instanceof Exception) {
                $str .= "Opération impossible !" . PHP_EOL;
                foreach (Result::getFailureExceptionGenerator($result) as $e) {
                    $str .= "Raison : " . $e->getMessage() . PHP_EOL;
                    if ($includeDetails) {
                        $str .= $result->getTraceAsString() . PHP_EOL . PHP_EOL;
                    }
                }
            } elseif (is_int($result)) {
                $str .= $result . " enregistrement(s).";
            } else {
                throw new RuntimeException("Type de résultat spécifié inattendu : " . gettype($result));
            }
            $str .= PHP_EOL;
        }

        return $str;
    }

    /**
     * @param bool $includeDetails
     * @return string
     */
    public function toHtmlString(bool $includeDetails = false): string
    {
        return str_replace(PHP_EOL, '<br>', $this->toString($includeDetails));
    }

    /**
     * @return string
     * @throws \Exception
     */
    public function getHash(): string
    {
        if ($this->hash === null) {
            $this->hash = Uuid::uuid1();
        }

        return $this->hash;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->toString();
    }
}