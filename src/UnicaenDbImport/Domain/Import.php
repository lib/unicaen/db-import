<?php

namespace UnicaenDbImport\Domain;

use Assert\AssertionChain;
use Assert\InvalidArgumentException;
use Laminas\Stdlib\Parameters;
use UnicaenDbImport\Config\ConfigException;

class Import implements ImportInterface
{
    /**
     * @var Parameters
     */
    protected $config;

    /**
     * @param array $config
     * @return static
     * @throws \UnicaenDbImport\Config\ConfigException
     */
    static public function fromConfig(array $config): self
    {
        $inst = new static($config);

        try {
            $inst->validateConfig();
        } catch (InvalidArgumentException $e) {
            throw new ConfigException($e->getMessage());
        }

        return $inst;
    }

    protected function __construct(array $config)
    {
        $this->config = new Parameters($config);
    }

    /**
     * @throws InvalidArgumentException
     */
    protected function validateConfig()
    {
        (new AssertionChain($this->config->get($key = 'source'), "Une instance de " . SourceInterface::class . " est requise pour la clé suivante: $key (import $this)"))
            ->notEmpty()
            ->isInstanceOf(SourceInterface::class);

        (new AssertionChain($this->config->get($key = 'destination'), "Une instance de " . DestinationInterface::class . " est requise pour la clé suivante: $key (import $this)"))
            ->notEmpty()
            ->isInstanceOf(DestinationInterface::class);

        if ($this->config->offsetExists($key = 'order')) {
            (new AssertionChain($this->config->get('order'), "Un entier est requis pour la clé suivante: $key (import $this)"))
                ->integer();
        }
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getName();
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->config->get('name');
    }

    /**
     * @return \UnicaenDbImport\Domain\SourceInterface
     */
    public function getSource(): SourceInterface
    {
        return $this->config->get('source');
    }

    /**
     * @return \UnicaenDbImport\Domain\DestinationInterface
     */
    public function getDestination(): DestinationInterface
    {
        return $this->config->get('destination');
    }

    /**
     * @return null|int
     */
    public function getOrder(): ?int
    {
        return $this->config->get('order');
    }
}