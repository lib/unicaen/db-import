<?php

namespace UnicaenDbImport\Domain;

interface ImportInterface extends NameAwareInterface
{
    /**
     * Retourne l'instance de la source de cet import.
     *
     * @return SourceInterface
     */
    public function getSource(): SourceInterface;

    /**
     * Retourne l'instance de la destination de cet import.
     *
     * @return DestinationInterface
     */
    public function getDestination(): DestinationInterface;

    /**
     * Retourne le numéro d'ordre éventuel de cet import.
     *
     * @return null|int
     */
    public function getOrder(): ?int;
}