<?php

namespace UnicaenDbImport\Domain;

use Doctrine\DBAL\Connection as ConnectionAlias;
use UnicaenDbImport\Connection\ApiConnection;
use UnicaenDbImport\Connection\NoConnection;
use UnicaenDbImport\Filter\ColumnName\ColumnNameFilterInterface;
use UnicaenDbImport\Filter\ColumnValue\ColumnValueFilterInterface;

interface SourceInterface
{
    /**
     * Version textuelle de cette source.
     *
     * @return string
     */
    public function __toString(): string;

    /**
     * Retourne le petit nom de cette source.
     *
     * @return null|string
     */
    public function getName(): string;

    /**
     * Retourne le 'code' unique permettant d'identifier cette source.
     *
     * @return null|string
     */
    public function getCode(): ?string;

    /**
     * Retourne le nom de la table de source.
     *
     * @return string|null
     */
    public function getTable(): ?string;

    /**
     * Retourne l'instruction SQL permettant de requêter la source.
     *
     * @return string|null
     */
    public function getSelect(): ?string;

    /**
     * Retourne le filtre SQL éventuel à appliquer aux données sources.
     *
     * @return string|null
     */
    public function getWhere(): ?string;

    /**
     * Retourne l'instance de la connexion vers cette source.
     *
     * @return NoConnection|ConnectionAlias|ApiConnection
     */
    public function getConnection();

    /**
     * Retourne le nom de la colonne commune existant obligatoirement dans la table/vue source
     * ET la table destination.
     *
     * Dans les tables source et destination, cette colonne est celle d'un identifiant unique permettant
     * de rapprocher les enregistrements source et destination.
     *
     * @return string Exemple: 'code'
     */
    public function getSourceCodeColumn(): string;

    /**
     * Retourne les noms des colonnes de la table/vue source concernées par l'import.
     *
     * Ne doit pas inclure la colonne commune retournée par {@link getSourceCodeColumn}.
     *
     * @return array Exemple: ['libelle', 'debut_validite', 'fin_validite']
     */
    public function getColumns(): array;

    /**
     * Fixe les noms des colonnes de la table/vue source concernées par l'import.
     *
     * Ne doit pas inclure la colonne commune retournée par {@link getSourceCodeColumn}.
     *
     * @param array $columns Exemple: ['libelle', 'debut_validite', 'fin_validite']
     */
    public function setColumns(array $columns);

    /**
     * Retourne les éventuels colonnes/attributs dont la valeur est caclulée, au format suivant :
     * 'nom de la colonne/attribut calculée' => {@see \UnicaenDbImport\Filter\ColumnValue\ColumnValueFilterInterface}.
     *
     * @return \UnicaenDbImport\Filter\ColumnValue\ColumnValueFilterInterface[]
     */
    public function getComputedColumns(): array;

    /**
     * Retourne l'instance de filtre permettant de transformer un nom de colonne/attribut source
     * en un nom de colonne destination.
     *
     * @return ColumnNameFilterInterface|null
     */
    public function getColumnNameFilter() : ?ColumnNameFilterInterface;

    /**
     * Retourne les instances de filtres éventuels permettant de transformer une valeur de colonne/attribut source
     * en une valeur de colonne destination.
     *
     * @return ColumnValueFilterInterface[]
     */
    public function getColumnValueFilter(): array;

    /**
     * Retourne les éventuelles attributs libres complémentaires.
     *
     * @return array
     */
    public function getExtra(): array;

    /**
     * Retourne les données obtenues par l'application de la requête associée à la source.
     *
     * @return array
     */
    public function getData(): array;

    /**
     * Fixe les données obtenues par l'application de la requête associée à la source.
     *
     * @param array $data Exemple : [['libelle'=>'A', 'note'=>2], ['libelle'=>'B', 'note'=>5]]
     */
    public function setData(array $data);

    /**
     * Retourne la taille des pages à respecter dans l'obtention des données.
     *
     * @return int
     */
    public function getPageSize(): int;
}