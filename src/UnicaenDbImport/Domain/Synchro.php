<?php

namespace UnicaenDbImport\Domain;

use Assert\AssertionChain;
use Assert\InvalidArgumentException;
use UnicaenDbImport\Config\ConfigException;
use Laminas\Stdlib\Parameters;

class Synchro implements SynchroInterface
{
    const V_DIFF_UPDATED_COLUMN_PREFIX = 'U_';

    const V_DIFF_SRC_COLUMN_PREFIX = 'S_';
    const V_DIFF_DEST_COLUMN_PREFIX = 'D_';

    /**
     * @var Parameters
     */
    protected $config;

    /**
     * @param array $config
     * @return static
     * @throws \UnicaenDbImport\Config\ConfigException
     */
    static public function fromConfig(array $config): self
    {
        $inst = new static($config);

        try {
            $inst->validateConfig();
        } catch (InvalidArgumentException $e) {
            throw new ConfigException($e->getMessage());
        }

        $inst->getDestination()->setUsesIntermediateTable($inst->requiresIntermediateTable());

        return $inst;
    }

    protected function __construct(array $config)
    {
        $this->config = new Parameters($config);
    }

    /**
     * @throws InvalidArgumentException
     */
    protected function validateConfig()
    {
        (new AssertionChain($this->config->get($key = 'source'), "Une instance de " . SourceInterface::class . " est requise pour la clé suivante: $key (synchro $this)"))
            ->notEmpty()
            ->isInstanceOf(SourceInterface::class);

        (new AssertionChain($this->config->get($key = 'destination'), "Une instance de " . DestinationInterface::class . " est requise pour la clé suivante: $key (synchro $this)"))
            ->notEmpty()
            ->isInstanceOf(DestinationInterface::class);

        if ($this->config->offsetExists($key = 'intermediate_table')) {
            $intermediateTable = $this->config->get($key);
            (new AssertionChain($intermediateTable, "Une string non vide est requise pour la clé facultative suivante: $key (synchro $this)"))
                ->notEmpty()
                ->string();
        }

        if ($this->config->offsetExists($key = 'order')) {
            (new AssertionChain($this->config->get('order'), "Un entier est requis pour la clé suivante: $key (synchro $this)"))
                ->integer();
        }
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getName();
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->config->get('name');
    }

    /**
     * @return \UnicaenDbImport\Domain\SourceInterface
     */
    public function getSource(): SourceInterface
    {
        return $this->config->get('source');
    }

    /**
     * @return \UnicaenDbImport\Domain\DestinationInterface
     */
    public function getDestination(): DestinationInterface
    {
        return $this->config->get('destination');
    }

    /**
     * @return null|int
     */
    public function getOrder(): ?int
    {
        return $this->config->get('order');
    }

    /**
     * @inheritDoc
     */
    public function requiresIntermediateTable(): bool
    {
        return $this->getSource()->getConnection() !== $this->getDestination()->getConnection();
    }
}