<?php

namespace UnicaenDbImport\Domain;

use Doctrine\DBAL\Connection;
use UnicaenDbImport\Filter\ColumnValue\ColumnValueFilterInterface;

interface DestinationInterface
{
    const LOG_TABLE_DEFAULT_NAME = 'import_log';

    const HISTO_COLUMNS = ['CREATED_ON', 'UPDATED_ON', 'DELETED_ON'];

    const ID_STRATEGY_SEQUENCE = 'SEQUENCE';
    const ID_STRATEGY_IDENTITY = 'IDENTITY';
    const ID_STRATEGIES = [self::ID_STRATEGY_SEQUENCE, self::ID_STRATEGY_IDENTITY];

    /**
     * Spécifie si cette destination fait appel à une table intermédiaire.
     *
     * @param bool $usesIntermediateTable
     */
    public function setUsesIntermediateTable(bool $usesIntermediateTable) ;

    /**
     * Version textuelle de cette destination.
     *
     * @return string
     */
    public function __toString(): string;

    /**
     * Retourne le petit nom de cette destination.
     *
     * @return null|string
     */
    public function getName(): string;

    /**
     * Retourne le nom de la table de destination.
     *
     * @return string
     */
    public function getTable(): string;

    /**
     * Retourne le filtre SQL éventuel permettant de restreindre les données destinations concernées.
     *
     * @return string|null
     */
    public function getWhere(): ?string;

    /**
     * Retourne l'instance de la connexion Doctrine vers cette destination.
     *
     * @return Connection
     */
    public function getConnection(): Connection;

    /**
     * Retourne le nom de la colonne commune existant obligatoirement dans la table/vue source
     * ET la table destination.
     *
     * Dans les tables source et destination, cette colonne est celle d'un identifiant unique permettant
     * de rapprocher les enregistrements source et destination.
     *
     * @return string Exemple : 'code'
     */
    public function getSourceCodeColumn(): string;

    /**
     * Retourne le nom de la colonne éventuelle dans la table destination permettant d'autoriser ou on l'opération 'undelete'.
     *
     * @return string|null Exemple : 'synchro_undelete_enabled'
     */
    public function getUndeleteEnabledColumn(): ?string;

    /**
     * Retourne le nom de la colonne éventuelle dans la table destination permettant d'autoriser ou non l'opération 'update'
     * sur les enregistrements historisés.
     *
     * @return string|null Exemple : 'synchro_update_on_deleted_enabled'
     */
    public function getUpdateOnDeletedEnabledColumn(): ?string;

    /**
     * Retourne les noms des colonnes de la table destination concernées par l'import.
     *
     * Ne doit pas inclure la colonne commune retournée par {@link getSourceCodeColumn}.
     *
     * @return string[] Exemple : ['libelle', 'debut_validite', 'fin_validite']
     */
    public function getColumns(): array;

    /**
     * Retourne les instances de filtres éventuels permettant de transformer une veleur de colonne/attribut source
     * en une valeur de colonne destination.
     *
     * @return \UnicaenDbImport\Filter\ColumnValue\ColumnValueFilterInterface[]
     */
    public function getColumnValueFilter(): array;

    /**
     * Retourne les éventuelles attributs libres complémentaires.
     *
     * @return array
     */
    public function getExtra(): array;

    /**
     * Retourne le nom de la strategy à utiliser pour générer les valeurs de la colonne ID (clé primaire).
     * Si la stratégie est `'SEQUENCE'`, la séquence spécifiée par {@see getIdColumnSequence()} sera utilisée.
     * Si la stratégie est `'IDENTITY'`, la valeur de l'ID des données sources sera utilisée.
     * Si la stratégie est `null`, la colonne de la table destination devra avoir un type auto-incrémenté
     * pour que l'insertion fonctionne.
     *
     * @see \UnicaenDbImport\Domain\DestinationInterface::ID_STRATEGIES
     */
    public function getIdColumnStrategy(): ?string;

    /**
     * Retourne le nom éventuel de la séquence permettant de générer les valeurs dans la colonne ID (clé primaire).
     * Si la valeur retournée est `null`, le nom de séquence par défaut '{table}_ID_SEQ' sera utilisé.
     */
    public function getIdColumnSequence(): ?string;

    /**
     * Retourne le nom de la table intermédiaire à créer dans la base de données de destination
     * lorsque l'utilisation d'une table intermédiaire est nécessaire (cf {@link requiresIntermediateTable}).
     *
     * @return string
     */
    public function getIntermediateTable(): string;

    /**
     * Indique si la suppression automatique de la table intermédiaire est activée.
     *
     * @return bool
     */
    public function getIntermediateTableAutoDrop(): bool;

    /**
     * Retourne le nom de la table des logs.
     *
     * @return string
     */
    public function getLogTable(): string;

}