<?php

namespace UnicaenDbImport\Domain;

use Assert\AssertionChain;
use Assert\InvalidArgumentException;
use Doctrine\DBAL\Connection;
use UnicaenDbImport\Config\ConfigException;
use Laminas\Stdlib\Parameters;
use UnicaenDbImport\Filter\ColumnValue\ColumnValueFilterInterface;
use Webmozart\Assert\Assert;

class Destination implements DestinationInterface
{
    const INTERMEDIATE_TABLE_NAME_PREFIX = 'TMP_';

    /**
     * @var Parameters
     */
    protected $config;

    /**
     * @var bool
     */
    protected $usesIntermediateTable = false;

    /**
     * @inheritDoc
     */
    public function setUsesIntermediateTable(bool $usesIntermediateTable = true)
    {
        $this->usesIntermediateTable = $usesIntermediateTable;
    }

    /**
     * @return bool
     */
    public function usesIntermediateTable(): bool
    {
        return $this->usesIntermediateTable;
    }

    /**
     * @param array $config
     * @return static
     * @throws \UnicaenDbImport\Config\ConfigException
     */
    static public function fromConfig(array $config): self
    {
        $inst = new static($config);

        try {
            $inst->validateConfig();
        } catch (\InvalidArgumentException $e) {
            throw new ConfigException(
                sprintf("Config Destination '%s' invalide : %s", $config['name'] ?? null, $e->getMessage()),
                null,
                $e
            );
        }

        return $inst;
    }

    protected function __construct(array $config)
    {
        $this->config = new Parameters($config);
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return implode(' -- ', array_filter([
            $this->getTable(),
            $this->getWhere() ? ' WHERE ' . $this->getWhere() : null,
            $this->usesIntermediateTable ?
                'Via ' . $this->getIntermediateTable() . ' (auto_drop = ' . (int)$this->getIntermediateTableAutoDrop() . ')' :
                null,
        ]));
    }

    /**
     * @throws \Assert\InvalidArgumentException
     * @throws \UnicaenDbImport\Config\ConfigException
     */
    protected function validateConfig(): void
    {
        (new AssertionChain($this->config->get($key = 'name'), "Une string non vide est requise pour la clé suivante: $key"))
            ->notEmpty()
            ->string();

        if ($table = $this->config->get($key = 'table')) {
            (new AssertionChain($table, "Une string non vide est requise pour la clé suivante: $key"))
                ->notEmpty()
                ->string();
        }

        if ($this->config->get('select')) {
            throw ConfigException::illegalKey('select');
        }

        if ($where = $this->config->get($key = 'where'))
            (new AssertionChain($where, "Une string non vide est requise pour la clé suivante: $key"))
                ->notEmpty()
                ->string();

        (new AssertionChain($this->config->get($key = 'connection'), "Une instance de " . Connection::class . " est requise pour la clé suivante: $key"))
            ->notEmpty()
            ->isInstanceOf(Connection::class);

        (new AssertionChain($this->config->get($key = 'source_code_column'), "Une string non vide est requise pour la clé suivante: $key"))
            ->notEmpty()
            ->string();

        if ($this->config->offsetExists($key = 'undelete_enabled_column'))
            (new AssertionChain($this->config->get($key), "Une string ou null est requis pour la clé suivante: $key"))
                ->nullOr()
                ->string();

        if ($this->config->offsetExists($key = 'update_on_deleted_enabled_column'))
            (new AssertionChain($this->config->get($key), "Une string ou null est requis pour la clé suivante: $key"))
                ->nullOr()
                ->string();

        Assert::true($this->config->offsetExists($key = 'id_strategy'), "La nouvelle clé de config $key est obligatoire");
        (new AssertionChain($this->config->get($key), "Les valeurs autorisées pour la clé $key sont les suivantes : " .
            implode(' ; ', $a = DestinationInterface::ID_STRATEGIES) . " ; null"))
            ->nullOr()
            ->inArray($a);

        Assert::true($this->config->offsetExists($key = 'id_sequence'), "La clé de config $key est désormais obligatoire");
        (new AssertionChain($this->config->get($key), "Les valeurs autorisées pour la clé $key sont les suivantes : string non vide ; null"))
            ->nullOr()
            ->notEmpty()
            ->string();

        if ($this->config->offsetExists($key = 'extra')) {
            (new AssertionChain($this->config->get($key), "Un tableau est requis pour la clé suivante: $key"))
                ->isArray();
        }
    }

    public function getName(): string
    {
        return $this->config->get('name');
    }

    public function getTable(): string
    {
        return $this->config->get('table');
    }

    public function getWhere(): ?string
    {
        return $this->config->get('where');
    }

    public function getConnection(): Connection
    {
        return $this->config->get('connection');
    }

    public function getSourceCodeColumn(): string
    {
        return $this->config->get('source_code_column');
    }

    public function getUndeleteEnabledColumn(): ?string
    {
        return $this->config->get('undelete_enabled_column');
    }

    public function getUpdateOnDeletedEnabledColumn(): ?string
    {
        return $this->config->get('update_on_deleted_enabled_column');
    }

    public function getColumns(): array
    {
        return $this->config->get('columns');
    }

    public function getExtra(): array
    {
        return $this->config->get('extra', []);
    }

    public function getColumnValueFilter(): array
    {
        return $this->config->get('column_value_filter', []);
    }

    public function getIdColumnStrategy(): ?string
    {
        return $this->config->get('id_strategy');
    }

    public function getIdColumnSequence(): ?string
    {
        return $this->config->get('id_sequence');
    }

    public function getIntermediateTable(): string
    {
        if ($intermediateTable = $this->config->get('intermediate_table')) {
            return $intermediateTable;
        }

        return static::INTERMEDIATE_TABLE_NAME_PREFIX . $this->getTable();
    }

    /**
     * @deprecated Suppression de intermediate_table_auto_drop à venir
     * TODO : à supprimer
     */
    public function getIntermediateTableAutoDrop(): bool
    {
        return (bool) $this->config->get('intermediate_table_auto_drop', false);
    }

    public function getLogTable(): string
    {
        return $this->config->get('log_table', DestinationInterface::LOG_TABLE_DEFAULT_NAME);
    }
}