<?php

namespace UnicaenDbImport\Domain;

interface SynchroInterface extends NameAwareInterface
{
    /**
     * Retourne l'instance de la source de cette synchro.
     */
    public function getSource(): SourceInterface;

    /**
     * Retourne l'instance de la destination de cette synchro.
     */
    public function getDestination(): DestinationInterface;

    /**
     * Retourne le numéro d'ordre éventuel de cette synchro.
     */
    public function getOrder(): ?int;

    /**
     * Indique si cette synchro nécessite l'utilisation d'une table intermédiaire.
     *
     * C'est typiquement le cas lorsque la source de la synchro est "externe", autrement dit lorsque la connexion
     * de la source diffère de celle de la destination.
     */
    public function requiresIntermediateTable(): bool ;
}