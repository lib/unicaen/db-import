<?php

namespace UnicaenDbImport\Domain;

class ImportResult extends Result
{
    /**
     * @var ImportInterface
     */
    private $import;

    /**
     * @param int $count
     */
    public function setDestinationTablePopulateResult(int $count)
    {
        $this->setResultsByOperation([
            Operation::OPERATION_INSERT => $count,
        ]);
    }

    /**
     * @param ImportInterface $import
     */
    public function setImport(ImportInterface $import)
    {
        $this->import = $import;
    }

    /**
     * @return ImportInterface
     */
    public function getImport(): ImportInterface
    {
        return $this->import;
    }
}