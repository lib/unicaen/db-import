<?php

namespace UnicaenDbImport\Domain\Exception;

use RuntimeException;

class NotFoundException extends RuntimeException
{
    public static function importByName($name): NotFoundException
    {
        return new static("Import introuvable avec ce nom : " . $name);
    }

    public static function synchroByName($name): NotFoundException
    {
        return new static("Synchro introuvable avec ce nom : " . $name);
    }
}