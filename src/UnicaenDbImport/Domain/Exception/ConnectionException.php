<?php

namespace UnicaenDbImport\Domain\Exception;

use RuntimeException;

class ConnectionException extends RuntimeException
{
    public static function unexpected($connection): ConnectionException
    {
        return new static(
            "Type de connexion rencontrée inattendu : " .
            (is_object($connection) ? get_class($connection) : gettype($connection))
        );
    }
}