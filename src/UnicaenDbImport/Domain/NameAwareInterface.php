<?php

namespace UnicaenDbImport\Domain;

interface NameAwareInterface
{
    /**
     * Retourne le petit nom de cette instance.
     *
     * @return string
     */
    public function getName(): string;
}