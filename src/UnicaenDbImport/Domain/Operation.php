<?php

namespace UnicaenDbImport\Domain;

class Operation
{
    const OPERATION_INSERT = 'insert';
    const OPERATION_UPDATE = 'update';
    const OPERATION_UNDELETE = 'undelete';
    const OPERATION_DELETE = 'delete';

    const OPERATIONS = [
        self::OPERATION_INSERT,
        self::OPERATION_UPDATE,
        self::OPERATION_UNDELETE,
        self::OPERATION_DELETE,
    ];

    const OPERATIONS_DESC = [
        self::OPERATION_INSERT => "Création nécessaire dans la destination en raison d'un nouvel enregistrement étant apparu dans la source.",
        self::OPERATION_UPDATE => "Mise à jour nécessaire dans la destination en raison d'un enregistrement ayant été modifié dans la source.",
        self::OPERATION_UNDELETE => "Restauration nécessaire dans la destination en raison d'un enregistrement étant réapparu dans la source.",
        self::OPERATION_DELETE => "Historisation nécessaire dans la destination en raison d'un enregistrement ayant disparu de la source.",
    ];
}