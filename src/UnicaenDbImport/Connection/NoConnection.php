<?php

namespace UnicaenDbImport\Connection;

/**
 * Non-connexion pour laquelle les données sont spécifiées manuellement sous forme d'un tableau.
 */
class NoConnection
{
    private array $data = [];

    public function __construct(array $data)
    {
        $this->setData($data);
    }

    public function setData(array $data): self
    {
        $this->data = $data;

        return $this;
    }

    public function getData(): array
    {
        return $this->data;
    }
}