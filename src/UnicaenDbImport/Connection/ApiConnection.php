<?php

namespace UnicaenDbImport\Connection;

use Laminas\Hydrator\ClassMethodsHydrator;

class ApiConnection
{
    /**
     * @var string
     */
    private $url;
    /**
     * @var boolean|array
     */
    private $proxy = false;
    /**
     * @var boolean
     */
    private $verify = false;
    /**
     * @var string
     */
    private $user;
    /**
     * @var string
     */
    private $password;

    /**
     * @var string
     */
    private $pageParam = 'page';

    /**
     * @var string
     */
    private $pageSizeParam = 'page_size';

    /**
     * @var int
     */
    private $timeout = 0;

    /**
     * @var int
     */
    private $connectTimeout = 0;

    /**
     * @param array $config
     * @return ApiConnection
     */
    public static function fromArrayConfig(array $config): self
    {
        $inst = new static();

        $h = new ClassMethodsHydrator();
        $h->hydrate($config, $inst);

        return $inst;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        $h = new ClassMethodsHydrator();

        return $h->extract($this);
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @param string $url
     * @return ApiConnection
     */
    public function setUrl(string $url): ApiConnection
    {
        $this->url = $url;
        return $this;
    }

    /**
     * @return bool|array
     */
    public function getProxy()
    {
        return $this->proxy;
    }

    /**
     * Met l'option de config sous la forme littérale.
     *
     * Si l'option est un tableau du genre :
     *    ```
     *    ['http' => 'proxy.unicaen.fr:3128', 'https' => 'proxy.unicaen.fr:3128', 'no' => ['localhost', '.unicaen.fr']],
     *    ```
     * le résultat est :
     *    ```
     *    HTTP: proxy.unicaen.fr:3128
     *    HTTPS: proxy.unicaen.fr:3128
     *    NO: localhost, .unicaen.fr
     *    ```
     *
     * @return string
     */
    public function getProxyToString(): string
    {
        if (is_array($this->proxy)) {
            $proxy = $this->proxy;

            return array_reduce(array_keys($proxy), function($previous, $key) use ($proxy) {
                $value = is_array($proxy[$key]) ? implode(', ', $proxy[$key]) : $proxy[$key];
                return $previous . strtoupper($key) . ': ' . $value . PHP_EOL;
            }, '');
        }

        return $this->proxy;
    }

    /**
     * @param bool|array $proxy
     * @return ApiConnection
     */
    public function setProxy($proxy): ApiConnection
    {
        $this->proxy = $proxy;
        return $this;
    }

    /**
     * @return bool
     */
    public function getVerify(): bool
    {
        return $this->verify;
    }

    /**
     * @param bool $verify
     * @return ApiConnection
     */
    public function setVerify(bool $verify = true): ApiConnection
    {
        $this->verify = $verify;
        return $this;
    }

    /**
     * @return string
     */
    public function getUser(): ?string
    {
        return $this->user;
    }

    /**
     * @param string|null $user
     * @return ApiConnection
     */
    public function setUser(string $user = null): ApiConnection
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return string
     */
    public function getPassword(): ?string
    {
        return $this->password;
    }

    /**
     * @param string|null $password
     * @return ApiConnection
     */
    public function setPassword(string $password = null): ApiConnection
    {
        $this->password = $password;
        return $this;
    }

    /**
     * @return string
     */
    public function getPageParam(): string
    {
        return $this->pageParam;
    }

    /**
     * @param string $pageParam
     * @return ApiConnection
     */
    public function setPageParam(string $pageParam): ApiConnection
    {
        $this->pageParam = $pageParam;

        return $this;
    }

    /**
     * @return string
     */
    public function getPageSizeParam(): string
    {
        return $this->pageSizeParam;
    }

    /**
     * @param string $pageSizeParam
     * @return ApiConnection
     */
    public function setPageSizeParam(string $pageSizeParam): ApiConnection
    {
        $this->pageSizeParam = $pageSizeParam;

        return $this;
    }

    /**
     * @return int
     */
    public function getTimeout(): int
    {
        return $this->timeout;
    }

    /**
     * @param int $timeout
     * @return self
     */
    public function setTimeout(int $timeout): self
    {
        $this->timeout = $timeout;
        return $this;
    }

    /**
     * @return int
     */
    public function getConnectTimeout(): int
    {
        return $this->connectTimeout;
    }

    /**
     * @param int $connectTimeout
     * @return self
     */
    public function setConnectTimeout(int $connectTimeout): self
    {
        $this->connectTimeout = $connectTimeout;
        return $this;
    }

}