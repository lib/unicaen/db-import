<?php

namespace UnicaenDbImport\Entity\Db;

use UnicaenDbImport\Entity\Db\Traits\SourceAwareTrait;

class ImportObservResult extends AbstractImportObservResult
{
    use SourceAwareTrait;

    /**
     * @var ImportObserv
     */
    protected $importObserv;

    /**
     * @param ImportObserv $importObserv
     */
    public function setImportObserv(AbstractImportObserv $importObserv)
    {
        $this->importObserv = $importObserv;
    }

    /**
     * @return ImportObserv
     */
    public function getImportObserv(): AbstractImportObserv
    {
        return $this->importObserv;
    }
}
