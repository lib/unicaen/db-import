<?php

namespace UnicaenDbImport\Entity\Db\Traits;

use UnicaenDbImport\Entity\Db\Interfaces\SourceInterface;

trait SourceAwareTrait
{
    /**
     * @var SourceInterface
     */
    protected $source;

    /**
     * @param SourceInterface|null $source
     */
    public function setSource(SourceInterface $source = null)
    {
        $this->source = $source;
    }

    /**
     * @return SourceInterface|null
     */
    public function getSource(): ?SourceInterface
    {
        return $this->source;
    }
}