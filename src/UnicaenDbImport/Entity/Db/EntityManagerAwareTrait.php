<?php

namespace UnicaenDbImport\Entity\Db;

use Doctrine\ORM\EntityManager;

trait EntityManagerAwareTrait
{
    protected EntityManager $entityManager;

    public function setEntityManager(EntityManager $entityManager): void
    {
        $this->entityManager = $entityManager;
    }

    public function getEntityManager(): EntityManager
    {
        return $this->entityManager;
    }
}