<?php

namespace UnicaenDbImport\Entity\Db\Service\ImportObservResult;

/**
 * @author Unicaen
 */
trait ImportObservResultServiceAwareTrait
{
    /**
     * @var ImportObservResultService
     */
    protected $importObservResultService;

    /**
     * @param ImportObservResultService $importObservResultService
     * @return void
     */
    public function setImportObservResultService(ImportObservResultService $importObservResultService)
    {
        $this->importObservResultService = $importObservResultService;
    }
}