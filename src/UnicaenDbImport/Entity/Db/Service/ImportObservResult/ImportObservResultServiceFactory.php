<?php

namespace UnicaenDbImport\Entity\Db\Service\ImportObservResult;

use Doctrine\ORM\EntityManager;
use Psr\Container\ContainerInterface;
use UnicaenDbImport\Config\Config;

class ImportObservResultServiceFactory
{
    /**
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container): ImportObservResultService
    {
        /** @var EntityManager $em */
        $em = $container->get('doctrine.entitymanager.orm_default');

        /** @var Config $config */
        $config = $container->get(Config::class);

        $service = new ImportObservResultService();
        $service->setEntityManager($em);
        $service->setEntityClass($config->getImportObservResultEntityClass());

        return $service;
    }
}