<?php

namespace UnicaenDbImport\Entity\Db\Service\ImportObservResult;

use Doctrine\ORM\EntityRepository;
use UnicaenDbImport\Entity\Db\Service\AbstractService;

/**
 * @author Unicaen
 */
class ImportObservResultService extends AbstractService
{
    /**
     * @var string
     */
    protected $entityClass;

    /**
     * @param string $entityClass
     * @return ImportObservResultService
     */
    public function setEntityClass(string $entityClass): ImportObservResultService
    {
        $this->entityClass = $entityClass;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getRepository(): EntityRepository
    {
        return $this->entityManager->getRepository($this->entityClass);
    }
}
