<?php

namespace UnicaenDbImport\Entity\Db\Service\ImportObserv;

use Doctrine\ORM\EntityManager;
use Psr\Container\ContainerInterface;
use UnicaenDbImport\Config\Config;

class ImportObservServiceFactory
{
    /**
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container): ImportObservService
    {
        /** @var EntityManager $em */
        $em = $container->get('doctrine.entitymanager.orm_default');

        /** @var Config $config */
        $config = $container->get(Config::class);

        $service = new ImportObservService();
        $service->setEntityManager($em);
        $service->setEntityClass($config->getImportObservEntityClass());

        return $service;
    }
}