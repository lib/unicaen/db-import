<?php

namespace UnicaenDbImport\Entity\Db\Service\ImportObserv;

use Doctrine\ORM\EntityRepository;
use UnicaenDbImport\Entity\Db\Service\AbstractService;

/**
 * @author Unicaen
 */
class ImportObservService extends AbstractService
{
    /**
     * @var string
     */
    protected $entityClass;

    /**
     * @param string $entityClass
     * @return ImportObservService
     */
    public function setEntityClass(string $entityClass): ImportObservService
    {
        $this->entityClass = $entityClass;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getRepository(): EntityRepository
    {
        return $this->getEntityManager()->getRepository($this->entityClass);
    }
}