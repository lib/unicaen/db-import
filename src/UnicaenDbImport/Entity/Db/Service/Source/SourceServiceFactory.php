<?php

namespace UnicaenDbImport\Entity\Db\Service\Source;

use Psr\Container\ContainerInterface;
use UnicaenDbImport\Config\Config;

class SourceServiceFactory
{
    /**
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container): SourceService
    {
        /** @var Config $config */
        $config = $container->get(Config::class);

        $service = new SourceService();
        $service->setDefaultSourceCode($config->getDefaultSourceCode());

        return $service;
    }
}