<?php

namespace UnicaenDbImport\Entity\Db\Service\Source;

use Doctrine\ORM\EntityRepository;
use UnicaenDbImport\Entity\Db\Service\ServiceException;
use UnicaenDbImport\Entity\Db\Source;
use UnicaenDbImport\Entity\Db\Service\AbstractService;

class SourceService extends AbstractService
{
    /**
     * @var string
     */
    protected $defaultSourceCode;

    /**
     * @param string $defaultSourceCode
     * @return SourceService
     */
    public function setDefaultSourceCode(string $defaultSourceCode): SourceService
    {
        $this->defaultSourceCode = $defaultSourceCode;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getRepository(): EntityRepository
    {
        return $this->getEntityManager()->getRepository(Source::class);
    }

    /**
     * Retourne la Source par défaut (celle dont le code est spécifié dans la config du module).
     *
     * @return Source
     * @throws ServiceException Source introuvable
     */
    public function fetchDefaultSource(): Source
    {
        /** @var Source|null $source */
        $source = $this->getRepository()->findOneBy(['code' => $this->defaultSourceCode]);

        if ($source === null) {
            throw new ServiceException("Source introuvable avec ce code : " . $this->defaultSourceCode);
        }

        return $source;
    }
}