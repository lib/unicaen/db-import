<?php

namespace UnicaenDbImport\Entity\Db\Service\Source;

trait SourceServiceAwareTrait
{
    /**
     * @var SourceService
     */
    protected $sourceService;

    /**
     * @param SourceService $serviceSource
     */
    public function setSourceService(SourceService $serviceSource)
    {
        $this->sourceService = $serviceSource;
    }
}