<?php

namespace UnicaenDbImport\Entity\Db\Service;

use Doctrine\ORM\EntityRepository;
use UnicaenDbImport\Entity\Db\EntityManagerAwareTrait;

abstract class AbstractService
{
    use EntityManagerAwareTrait;

    /**
     * @return EntityRepository
     */
    abstract protected function getRepository(): EntityRepository;
}
