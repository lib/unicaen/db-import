<?php

namespace UnicaenDbImport\Entity\Db\Service\ImportLog;

trait ImportLogServiceAwareTrait
{
    /**
     * @var ImportLogService
     */
    protected $importLogService;

    /**
     * @param ImportLogService $serviceImportLog
     */
    public function setImportLogService(ImportLogService $serviceImportLog)
    {
        $this->importLogService = $serviceImportLog;
    }
}