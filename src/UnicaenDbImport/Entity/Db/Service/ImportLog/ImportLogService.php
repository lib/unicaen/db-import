<?php

namespace UnicaenDbImport\Entity\Db\Service\ImportLog;

use Doctrine\ORM\EntityRepository;
use UnicaenDbImport\Domain\ImportInterface;
use UnicaenDbImport\Domain\SynchroInterface;
use UnicaenDbImport\Entity\Db\AbstractImportLog;
use UnicaenDbImport\Entity\Db\ImportLog;
use UnicaenDbImport\Entity\Db\Service\AbstractService;

class ImportLogService extends AbstractService
{
    /**
     * @inheritDoc
     */
    public function getRepository(): EntityRepository
    {
        return $this->getEntityManager()->getRepository(ImportLog::class);
    }

    /**
     * @param \UnicaenDbImport\Domain\Import $import
     * @return ImportLog|null
     */
    public function findLastLogForImport(ImportInterface $import): ?ImportLog
    {
        $qb = $this->getRepository()->createQueryBuilder('l')
            ->andWhere('l.type = :type')->setParameter('type', AbstractImportLog::TYPE_IMPORT)
            ->andWhere('l.name = :name')->setParameter('name', $import->getName())
            ->addOrderBy('l.startedOn', 'desc');

        $importLogs = $qb->getQuery()->setMaxResults(1)->getResult();

        return $importLogs[0] ?? null;
    }

    public function findLastLogForSynchro(SynchroInterface $import): ?ImportLog
    {
        $qb = $this->getRepository()->createQueryBuilder('l')
            ->andWhere('l.type = :type')->setParameter('type', AbstractImportLog::TYPE_SYNCHRO)
            ->andWhere('l.name = :name')->setParameter('name', $import->getName())
            ->addOrderBy('l.startedOn', 'desc');

        $importLogs = $qb->getQuery()->setMaxResults(1)->getResult();

        return $importLogs[0] ?? null;
    }
}