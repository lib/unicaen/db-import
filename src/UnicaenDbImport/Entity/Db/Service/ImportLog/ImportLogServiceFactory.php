<?php

namespace UnicaenDbImport\Entity\Db\Service\ImportLog;

use Doctrine\ORM\EntityManager;
use Psr\Container\ContainerInterface;

class ImportLogServiceFactory
{
    /**
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container): ImportLogService
    {
        /** @var EntityManager $em */
        $em = $container->get('doctrine.entitymanager.orm_default');

        $service = new ImportLogService();
        $service->setEntityManager($em);

        return $service;
    }
}