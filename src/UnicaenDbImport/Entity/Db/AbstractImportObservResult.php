<?php

namespace UnicaenDbImport\Entity\Db;

use DateTime;

abstract class AbstractImportObservResult
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var DateTime
     */
    protected $dateCreation;

    /**
     * @var string
     */
    protected $sourceCode;

    /**
     * @var string
     */
    protected $resultat;

    /**
     * @var DateTime
     */
    protected $dateNotif;

    /**
     * @return string
     */
    public function __toString()
    {
        return sprintf("[%s] %s / %s : %s",
            $this->getId(),
            $this->dateCreation->format("d/m/Y H:i:s"),
            $this->sourceCode,
            $this->resultat
        );
    }

    /**
     * @param DateTime $dateCreation
     * @return self
     */
    public function setDateCreation(DateTime $dateCreation): self
    {
        $this->dateCreation = $dateCreation;

        return $this;
    }

    /**
     * @param string $sourceCode
     * @return self
     */
    public function setSourceCode(string $sourceCode): self
    {
        $this->sourceCode = $sourceCode;

        return $this;
    }

    /**
     * @param string $resultat
     * @return self
     */
    public function setResultat(string $resultat): self
    {
        $this->resultat = $resultat;

        return $this;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return DateTime
     */
    public function getDateCreation(): DateTime
    {
        return $this->dateCreation;
    }

    /**
     * @return string
     */
    public function getSourceCode(): string
    {
        return $this->sourceCode;
    }

    /**
     * @return string
     */
    public function getResultat(): string
    {
        return $this->resultat;
    }

    /**
     * @return DateTime|null
     */
    public function getDateNotif(): ?DateTime
    {
        return $this->dateNotif;
    }

    /**
     * @param DateTime|null $dateNotif
     * @return self
     */
    public function setDateNotif(?DateTime $dateNotif = null): self
    {
        $this->dateNotif = $dateNotif;
        return $this;
    }
}
