<?php

namespace UnicaenDbImport\Entity\Db;

class ImportLog extends AbstractImportLog
{
    /**
     * @return bool
     */
    public function isTypeImport(): bool
    {
        return $this->getType() === static::TYPE_IMPORT;
    }

    /**
     * @return bool
     */
    public function isTypeSynchro(): bool
    {
        return $this->getType() === static::TYPE_SYNCHRO;
    }
}
