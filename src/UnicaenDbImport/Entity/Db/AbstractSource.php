<?php

namespace UnicaenDbImport\Entity\Db;

use UnicaenDbImport\Entity\Db\Interfaces\SourceInterface;

/**
 * AbstractSource
 */
abstract class AbstractSource implements SourceInterface
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $code;

    /**
     * @var boolean
     */
    protected $importable;

    protected bool $synchroInsertEnabled = true;
    protected bool $synchroUpdateEnabled = true;
    protected bool $synchroUndeleteEnabled = true;
    protected bool $synchroDeleteEnabled = true;

    /**
     * @var string
     */
    protected $libelle;

    /**
     * Get id
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * Set code
     *
     * @param string $code
     */
    public function setCode(string $code)
    {
        $this->code = $code;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }

    /**
     * Set importable
     *
     * @param boolean $importable
     */
    public function setImportable(bool $importable)
    {
        $this->importable = $importable;
    }

    /**
     * Get importable
     *
     * @return boolean
     */
    public function getImportable(): bool
    {
        return $this->importable;
    }

    public function isSynchroInsertEnabled(): bool
    {
        return $this->synchroInsertEnabled;
    }

    public function setSynchroInsertEnabled(bool $synchroInsertEnabled): void
    {
        $this->synchroInsertEnabled = $synchroInsertEnabled;
    }

    public function isSynchroUpdateEnabled(): bool
    {
        return $this->synchroUpdateEnabled;
    }

    public function setSynchroUpdateEnabled(bool $synchroUpdateEnabled): void
    {
        $this->synchroUpdateEnabled = $synchroUpdateEnabled;
    }

    public function isSynchroUndeleteEnabled(): bool
    {
        return $this->synchroUndeleteEnabled;
    }

    public function setSynchroUndeleteEnabled(bool $synchroUndeleteEnabled): void
    {
        $this->synchroUndeleteEnabled = $synchroUndeleteEnabled;
    }

    public function isSynchroDeleteEnabled(): bool
    {
        return $this->synchroDeleteEnabled;
    }

    public function setSynchroDeleteEnabled(bool $synchroDeleteEnabled): void
    {
        $this->synchroDeleteEnabled = $synchroDeleteEnabled;
    }

    /**
     * Set libelle
     *
     * @param string $libelle
     */
    public function setLibelle(string $libelle)
    {
        $this->libelle = $libelle;
    }

    /**
     * Get libelle
     *
     * @return string
     */
    public function getLibelle(): string
    {
        return $this->libelle;
    }

    /**
     * Retourne la représentation littérale de cet objet.
     *
     * @return string
     */
    public function __toString(): string
    {
        return $this->getLibelle();
    }
}