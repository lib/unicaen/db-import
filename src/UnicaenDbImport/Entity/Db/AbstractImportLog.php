<?php

namespace UnicaenDbImport\Entity\Db;

use DateTime;

abstract class AbstractImportLog
{
    const TYPE_IMPORT = 'import';
    const TYPE_SYNCHRO = 'synchro';

    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $type;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var boolean
     */
    protected $success;

    /**
     * @var boolean
     */
    protected $hasProblems = false;

    /**
     * @var string
     */
    protected $log;

    /**
     * @var DateTime
     */
    protected $startedOn;

    /**
     * @var DateTime
     */
    protected $endedOn;

    /**
     * @var string
     */
    protected $importHash;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return self
     */
    public function setType(string $type): self
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return self
     */
    public function setName(string $name): self
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return bool
     */
    public function isSuccess(): bool
    {
        return $this->success;
    }

    /**
     * @param bool $success
     * @return self
     */
    public function setSuccess(bool $success): self
    {
        $this->success = $success;
        return $this;
    }

    /**
     * @return bool
     */
    public function hasProblems(): bool
    {
        return $this->hasProblems;
    }

    /**
     * @param bool $hasProblems
     * @return self
     */
    public function setHasProblems(bool $hasProblems = true): self
    {
        $this->hasProblems = $hasProblems;
        return $this;
    }

    /**
     * @return string
     */
    public function getLog(): string
    {
        return $this->log;
    }

    /**
     * @return string
     */
    public function getTruncatedLog(): string
    {
        return mb_substr($this->log, 0, 150);
    }

    /**
     * @return string
     */
    public function getLogToHtml(): string
    {
        return str_replace(PHP_EOL, '<br>', htmlspecialchars($this->getLog()));
    }

    /**
     * @return string
     */
    public function getTruncatedLogToHtml(): string
    {
        return mb_substr($this->getTruncatedLog(), 0, 150);
    }

    /**
     * @param string $log
     * @return self
     */
    public function setLog(string $log): self
    {
        $this->log = $log;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getStartedOn(): DateTime
    {
        return $this->startedOn;
    }

    /**
     * @return string
     */
    public function getStartedOnToString(): string
    {
        return $this->startedOn->format('d/m/Y H:i:s.u');
    }

    /**
     * @param DateTime $startedOn
     * @return self
     */
    public function setStartedOn(DateTime $startedOn): self
    {
        $this->startedOn = $startedOn;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getEndedOn(): DateTime
    {
        return $this->endedOn;
    }

    /**
     * @return string
     */
    public function getEndedOnToString(): string
    {
        return $this->endedOn->format('d/m/Y H:i:s.u');
    }

    /**
     * @param DateTime $endedOn
     * @return self
     */
    public function setEndedOn(DateTime $endedOn): self
    {
        $this->endedOn = $endedOn;
        return $this;
    }

    /**
     * Retourne la durée d'exécution au format "H min s", d'après les dates de début et de fin.
     */
    public function getDurationToString(): string
    {
        $diff = $this->endedOn->diff($this->startedOn);

        if ($diff->days === 0) {
            if ($diff->h === 0) {
                if ($diff->i === 0) {
                    return sprintf("%0.3f s", $diff->s + $diff->f);
                } else {
                    return sprintf("%s min %0.3f s", $diff->i, $diff->s + $diff->f);
                }
            } else {
                return sprintf("%s min %0.3f s", $diff->h*60 + $diff->i, $diff->s + $diff->f);
            }
        } else {
            return sprintf("%s h %s min %0.3f s", $diff->days*24 + $diff->h, $diff->i, $diff->s + $diff->f);
        }
    }

    /**
     * @return string|null
     */
    public function getImportHash(): ?string
    {
        return $this->importHash;
    }

    /**
     * @param string $importHash
     * @return self
     */
    public function setImportHash(string $importHash): self
    {
        $this->importHash = $importHash;
        return $this;
    }
}
