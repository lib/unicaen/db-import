<?php

namespace UnicaenDbImport\Entity\Db\Interfaces;

use UnicaenDbImport\Entity\Db\Source;

/**
 * Interface des entités possédant une "source".
 *
 * @see Source
 */
interface SourceAwareInterface
{
    /**
     * @param SourceInterface|null $source
     */
    public function setSource(SourceInterface $source = null);

    /**
     * @return SourceInterface|null
     */
    public function getSource(): ?SourceInterface;
}