<?php

namespace UnicaenDbImport\Entity\Db\Interfaces;

use UnicaenDbImport\Entity\Db\Source;

/**
 * Interface décrivant une "source".
 *
 * @see Source
 */
interface SourceInterface
{
    /**
     * Get id
     *
     * @return int
     */
    public function getId(): int;

    /**
     * Get code
     *
     * @return string
     */
    public function getCode(): string;

    /**
     * Get importable
     *
     * @return boolean
     */
    public function getImportable(): bool;

    /**
     * Get libelle
     *
     * @return string
     */
    public function getLibelle(): string;

    /**
     * Retourne la représentation littérale de cette source.
     *
     * @return string
     */
    public function __toString(): string;
}