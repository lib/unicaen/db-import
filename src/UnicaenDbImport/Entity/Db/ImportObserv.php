<?php

namespace UnicaenDbImport\Entity\Db;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

class ImportObserv extends AbstractImportObserv
{
    /**
     * @var Collection
     */
    protected $importObservResults;

    /**
     * ImportObserv constructor.
     */
    public function __construct()
    {
        $this->importObservResults = new ArrayCollection();
    }

    /**
     * @return Collection
     */
    public function getImportObservResults(): ?Collection
    {
        return $this->importObservResults;
    }

    /**
     * @param ImportObservResult $importObservesult
     * @return self
     */
    public function addImportObservResult(ImportObservResult $importObservesult): self
    {
        $this->importObservResults->add($importObservesult);

        return $this;
    }

    /**
     * @param ImportObservResult $importObservesult
     * @return self
     */
    public function removeImportObservResult(ImportObservResult $importObservesult): self
    {
        $this->importObservResults->removeElement($importObservesult);

        return $this;
    }
}
