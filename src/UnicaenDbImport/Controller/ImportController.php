<?php

namespace UnicaenDbImport\Controller;

use Laminas\Http\PhpEnvironment\Response;
use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\Mvc\Plugin\FlashMessenger\FlashMessenger;
use Laminas\Mvc\Plugin\Prg\PostRedirectGet;
use Laminas\View\Model\JsonModel;
use UnicaenDbImport\Domain\ImportInterface;
use UnicaenDbImport\Domain\ImportResult;
use UnicaenDbImport\Domain\Result;
use UnicaenDbImport\Entity\Db\Service\ImportLog\ImportLogServiceAwareTrait;
use UnicaenDbImport\Service\Traits\ImportServiceAwareTrait;

/**
 * @method FlashMessenger flashMessenger()
 * @method PostRedirectGet prg($redirect = null, $redirectToUrl = false)
 */
class ImportController extends AbstractActionController
{
    use ImportServiceAwareTrait;
    use ImportLogServiceAwareTrait;

    /**
     * Listage des imports.
     *
     * @return array|\Laminas\Http\PhpEnvironment\Response
     */
    public function indexAction()
    {
        $prg = $this->prg(null, true);
        if ($prg instanceof Response) {
            return $prg;
        }

        if ($text = $this->params()->fromQuery('text')) {
            $imports = $this->importService->getImportsFilteredBy(['text' => $text]);
        } else {
            $imports = $this->importService->getImports();
        }

        $importLogs = [];
        foreach ($imports as $import) {
            $importLog = $this->importLogService->findLastLogForImport($import);
            $importLogs[$import->getName()] = $importLog;
        }

        return [
            'imports' => $imports,
            'importLogs' => $importLogs,
            'text' => $text,
        ];
    }

    /**
     * Détails d'une synchro.
     *
     * @return array
     */
    public function voirAction(): array
    {
        $name = $this->params('name');
        $import = $this->importService->getImportByName($name);
        $importLog = $this->importLogService->findLastLogForImport($import);

        return [
            'import' => $import,
            'importLog' => $importLog,
        ];
    }

    /**
     * Lancement d'un import.
     *
     * @return \Laminas\Http\Response|bool
     * @throws \Exception
     */
    public function lancerAction()
    {
        $name = $this->params('name');
        $import = $this->importService->getImportByName($name);

        $result = $this->importService->runImport($import);

        if ($this->getRequest()->isXmlHttpRequest()) {
            return false;
        } else {
            $this->flashMessages($import, $result);

            return $this->redirect()->toRoute('unicaen-db-import/import/voir', ['name' => $import->getName()]);
        }
    }

    /**
     * @return \Laminas\Http\Response|bool
     * @throws \Exception
     */
    public function lancerMultipleAction()
    {
        $names = explode(',', $this->params('names', ''));

        foreach ($names as $name) {
            $import = $this->importService->getImportByName($name);
            $result = $this->importService->runImport($import);

            if (! $this->getRequest()->isXmlHttpRequest()) {
                $this->flashMessages($import, $result);
            }
        }

        if ($this->getRequest()->isXmlHttpRequest()) {
            return false;
        } else {
            $redirect = $this->params()->fromQuery('redirect') ?: $this->url()->fromRoute('unicaen-db-import/import');

            return $this->redirect()->toUrl($redirect);
        }
    }

    protected function flashMessages(ImportInterface $import, ImportResult $result)
    {
        if ($exception = $result->getFailureException()) {
            $message = sprintf("L'import '%s' a échoué." . PHP_EOL, $import);
            foreach (Result::getFailureExceptionGenerator($exception) as $e) {
                $message .= "Raison : " . $e->getMessage() . PHP_EOL;
            }
            $this->flashMessenger()->addErrorMessage(str_replace(PHP_EOL, '<br>', $message));
        } else {
            $message = sprintf("Import '%s' exécuté avec succès en %s." . PHP_EOL, $import, $result->getDurationToString());
            if ($result->hasExceptionInResults()) {
                $message .= "Néanmoins, un problème a été rencontré." . PHP_EOL;
            }
            $message .= $result->toHtmlString(true) . PHP_EOL;
            $this->flashMessenger()->addSuccessMessage(str_replace(PHP_EOL, '<br>', $message));
        }
    }

    /**
     * @return \Laminas\View\Model\JsonModel
     */
    public function lastLogAction(): JsonModel
    {
        $name = $this->params('name');
        $import = $this->importService->getImportByName($name);
        $importLog = $this->importLogService->findLastLogForImport($import);

        return new JsonModel([
            'importLog' => $importLog,
        ]);
    }
}