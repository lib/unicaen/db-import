<?php

namespace UnicaenDbImport\Controller;

use Psr\Container\ContainerInterface;
use UnicaenDbImport\Entity\Db\Service\ImportObserv\ImportObservService;
use UnicaenDbImport\Entity\Db\Service\ImportObservResult\ImportObservResultService;

class ObservControllerFactory
{
    /**
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    function __invoke(ContainerInterface $sl): ObservController
    {
        /** @var ImportObservService $importObservService */
        /** @var ImportObservResultService $importObservResultService */
        $importObservService = $sl->get(ImportObservService::class);
        $importObservResultService = $sl->get(ImportObservResultService::class);

        $controller = new ObservController();
        $controller->setImportObservService($importObservService);
        $controller->setImportObservResultService($importObservResultService);

        return $controller;
    }
}