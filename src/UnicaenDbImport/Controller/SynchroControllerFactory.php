<?php

namespace UnicaenDbImport\Controller;

use Psr\Container\ContainerInterface;
use UnicaenDbImport\Entity\Db\Service\ImportLog\ImportLogService;
use UnicaenDbImport\Log\LivelogLogger;
use UnicaenDbImport\Service\SynchroService;

class SynchroControllerFactory
{
    /**
     * @param \Laminas\ServiceManager\ServiceManager $container
     * @return \UnicaenDbImport\Controller\SynchroController
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    function __invoke(ContainerInterface $container): SynchroController
    {
        /** @var LivelogLogger $logger */
        $logger = $container->get(LivelogLogger::class);

        /** @var SynchroService $synchroService */
        $synchroService = $container->build(SynchroService::class, ['logger' => $logger]);

        /** @var ImportLogService $importLogService */
        $importLogService = $container->get(ImportLogService::class);

        $controller = new SynchroController();
        $controller->setSynchroService($synchroService);
        $controller->setImportLogService($importLogService);

        return $controller;
    }
}