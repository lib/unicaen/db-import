<?php

namespace UnicaenDbImport\Controller;

use Doctrine\ORM\Tools\Pagination\Paginator;
use DoctrineORMModule\Paginator\Adapter\DoctrinePaginator;
use Laminas\Mvc\Plugin\FlashMessenger\FlashMessenger;
use UnicaenDbImport\Entity\Db\Service\ImportLog\ImportLogServiceAwareTrait;
use Laminas\Mvc\Controller\AbstractActionController;

/**
 * @method FlashMessenger flashMessenger()
 */
class LogController extends AbstractActionController
{
    use ImportLogServiceAwareTrait;

    /**
     * Listage des logs.
     *
     * @return array
     */
    public function indexAction(): array
    {
        $qb = $this->importLogService->getRepository()->createQueryBuilder('l');
        $qb->addOrderBy('l.startedOn', 'desc');
        $paginator = new \Laminas\Paginator\Paginator(new DoctrinePaginator(new Paginator($qb)));

        $maxi = $this->params()->fromQuery('maxi', 30);
        $page = $this->params()->fromQuery('page', 1);
        $paginator
            ->setPageRange(30)
            ->setItemCountPerPage((int)$maxi)
            ->setCurrentPageNumber((int)$page);

        return [
            'importLogs' => $paginator,
        ];
    }

    /**
     * @return array
     */
    public function voirAction(): array
    {
        $id = $this->params('id');
        $importLog = $this->importLogService->getRepository()->find($id);

        return [
            'importLog' => $importLog,
        ];
    }
}