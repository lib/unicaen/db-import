<?php

namespace UnicaenDbImport\Controller;

use Psr\Container\ContainerInterface;
use UnicaenDbImport\Entity\Db\Service\ImportLog\ImportLogService;
use UnicaenDbImport\Log\LivelogLogger;
use UnicaenDbImport\Service\ImportService;

class ImportControllerFactory
{
    /**
     * @param \Laminas\ServiceManager\ServiceManager $container
     * @return \UnicaenDbImport\Controller\ImportController
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    function __invoke(ContainerInterface $container): ImportController
    {
        /** @var LivelogLogger $logger */
        $logger = $container->get(LivelogLogger::class);

        /** @var ImportService $importService */
        $importService = $container->build(ImportService::class, ['logger' => $logger]);

        /** @var ImportLogService $importLogService */
        $importLogService = $container->get(ImportLogService::class);

        $controller = new ImportController();
        $controller->setImportService($importService);
        $controller->setImportLogService($importLogService);

        return $controller;
    }
}