<?php

namespace UnicaenDbImport\Controller;

use Psr\Container\ContainerInterface;
use UnicaenDbImport\Entity\Db\Service\ImportLog\ImportLogService;

class LogControllerFactory
{
    /**
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    function __invoke(ContainerInterface $sl): LogController
    {
        /** @var ImportLogService $importLogService */
        $importLogService = $sl->get(ImportLogService::class);

        $controller = new LogController();
        $controller->setImportLogService($importLogService);

        return $controller;
    }
}