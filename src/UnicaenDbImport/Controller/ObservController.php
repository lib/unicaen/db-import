<?php

namespace UnicaenDbImport\Controller;

use Doctrine\ORM\Query\Expr;
use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\Mvc\Plugin\FlashMessenger\FlashMessenger;
use UnicaenDbImport\Entity\Db\Service\ImportObserv\ImportObservServiceAwareTrait;
use UnicaenDbImport\Entity\Db\Service\ImportObservResult\ImportObservResultServiceAwareTrait;

/**
 * @method FlashMessenger flashMessenger()
 */
class ObservController extends AbstractActionController
{
    use ImportObservServiceAwareTrait;
    use ImportObservResultServiceAwareTrait;

    /**
     * Listage des observations.
     *
     * @return array
     */
    public function indexAction(): array
    {
        $importObservs = $this->importObservService->getRepository()->findAll();

        return [
            'importObservs' => $importObservs,
        ];
    }

    /**
     * Résultats d'une observation.
     *
     * @return array
     */
    public function resultAction(): array
    {
        $code = $this->params('code');

        /** @var \UnicaenDbImport\Entity\Db\ImportObserv $importObserv */
        $importObserv = $this->importObservService->getRepository()->findOneBy(['code' => $code]);

        /** @var \UnicaenDbImport\Entity\Db\ImportObservResult[] $importObservResults */
        $qb = $this->importObservResultService->getRepository()->createQueryBuilder('ior')
            ->join('ior.importObserv', 'io', Expr\Join::WITH, 'io.code = :code')
            ->setParameter('code', $code)
            ->addSelect('io')
            ->addOrderBy('ior.dateCreation', 'desc');
        $importObservResults = $qb->getQuery()->getResult();

        return [
            'importObserv' => $importObserv,
            'importObservResults' => $importObservResults,
        ];
    }

}