<?php

namespace UnicaenDbImport\Controller;

use Unicaen\Console\Adapter\Posix;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Psr\Container\ContainerInterface;
use Psr\Log\LoggerInterface;
use UnicaenDbImport\Service\ImportService;
use UnicaenDbImport\Service\SynchroService;

class ConsoleControllerFactory
{
    /**
     * @param \Laminas\ServiceManager\ServiceManager $container
     * @return \UnicaenDbImport\Controller\ConsoleController
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    function __invoke(ContainerInterface $container): ConsoleController
    {
        $logger = $this->createLogger();

        /** @var ImportService $importService */
        $importService = $container->build(ImportService::class, ['logger' => $logger]);
        /** @var SynchroService $synchroService */
        $synchroService = $container->build(SynchroService::class, ['logger' => $logger]);

        $controller = new ConsoleController();
        $controller->setImportService($importService);
        $controller->setSynchroService($synchroService);
        $controller->setConsole(new Posix());

        return $controller;
    }

    /**
     * @return \Psr\Log\LoggerInterface
     */
    private function createLogger(): LoggerInterface
    {
        return new Logger('console', [new StreamHandler('php://output')]);
    }
}