<?php

namespace UnicaenDbImport\Controller;

use Exception;
use Laminas\Http\PhpEnvironment\Response;
use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\Mvc\Plugin\FlashMessenger\FlashMessenger;
use Laminas\Mvc\Plugin\Prg\PostRedirectGet;
use Laminas\View\Model\JsonModel;
use UnicaenDbImport\Domain\Result;
use UnicaenDbImport\Domain\SynchroInterface;
use UnicaenDbImport\Domain\SynchroResult;
use UnicaenDbImport\Entity\Db\Service\ImportLog\ImportLogServiceAwareTrait;
use UnicaenDbImport\Service\Traits\SynchroServiceAwareTrait;

/**
 * @method FlashMessenger flashMessenger()
 * @method PostRedirectGet prg($redirect = null, $redirectToUrl = false)
 */
class SynchroController extends AbstractActionController
{
    use SynchroServiceAwareTrait;
    use ImportLogServiceAwareTrait;

    /**
     * Listage des synchros.
     *
     * @return array|\Laminas\Http\PhpEnvironment\Response
     */
    public function indexAction()
    {
        $prg = $this->prg(null, true);
        if ($prg instanceof Response) {
            return $prg;
        }

        if ($text = $this->params()->fromQuery('text')) {
            $synchros = $this->synchroService->getSynchrosFilteredBy(['text' => $text]);
        } else {
            $synchros = $this->synchroService->getSynchros();
        }

        $importLogs = $diffFounds = $diffFoundsErrors = [];
        foreach ($synchros as $synchro) {
            $importLog = $this->importLogService->findLastLogForSynchro($synchro);
            $importLogs[$synchro->getName()] = $importLog;
            try {
                $diffFounds[$synchro->getName()] = $this->synchroService->fetchSynchroDiffFound($synchro);
            }
            catch (Exception $e) {
                $error = "Erreur rencontrée lors du calcul des différences pour la syncho $synchro :";
                do {
                    $error .= ' ' . $e->getMessage();
                    $e = $e->getPrevious();
                } while ($e);
                $diffFoundsErrors[$synchro->getName()] = $error;
            }
        }

        return [
            'synchros' => $synchros,
            'importLogs' => $importLogs,
            'diffFounds' => $diffFounds,
            'diffFoundsErrors' => $diffFoundsErrors,
            'text' => $text,
        ];
    }

    /**
     * Détails d'une synchro.
     *
     * @return array
     */
    public function voirAction(): array
    {
        $name = $this->params('name');
        $synchro = $this->synchroService->getSynchroByName($name);
        $diffCount = -1;
        try{
            $diffCount = $this->synchroService->fetchSynchroDiffCount($synchro);
        }
        catch (Exception $e){
            $this->flashMessenger()->addErrorMessage(sprintf(
                "Erreur rencontrée lors du calcul des différences pour la syncho %s : %s",
                $synchro,
                $e->getMessage()
            ));
        }
        $importLog = $this->importLogService->findLastLogForSynchro($synchro);

        return [
            'synchro' => $synchro,
            'diffCount' => $diffCount,
            'limit' => 30,
            'importLog' => $importLog,
        ];
    }

    /**
     * Différentiel d'une synchro.
     *
     * @return array
     */
    public function diffAction(): array
    {
        $name = $this->params('name');
        $synchro = $this->synchroService->getSynchroByName($name);

        $diff = $this->synchroService->fetchSynchroDiff($synchro);
        $count = $this->synchroService->fetchSynchroDiffCount($synchro);
        $sqls = $this->synchroService->fetchSynchroSql($synchro);

        return [
            'synchro' => $synchro,
            'diff' => $diff,
            'limit' => 30,
            'count' => $count,
            'sqls' => $sqls,
        ];
    }

    /**
     * @return \Laminas\View\Model\JsonModel
     */
    public function diffCountAction(): JsonModel
    {
        $name = $this->params('name');
        $synchro = $this->synchroService->getSynchroByName($name);

        return new JsonModel([
            'count' => $this->synchroService->fetchSynchroDiffCount($synchro),
        ]);
    }

    /**
     * @return \Laminas\Http\Response|bool
     * @throws \Exception
     */
    public function lancerAction()
    {
        $name = $this->params('name');
        $synchro = $this->synchroService->getSynchroByName($name);

        $result = $this->synchroService->runSynchro($synchro);

        if ($this->getRequest()->isXmlHttpRequest()) {
            return false;
        } else {
            $this->flashMessages($synchro, $result);

            $redirect = $this->params()->fromQuery('redirect') ?:
                $this->url()->fromRoute('unicaen-db-import/synchro/diff', ['name' => $synchro->getName()]);

            return $this->redirect()->toUrl($redirect);
        }
    }

    /**
     * @return \Laminas\Http\Response|bool
     * @throws \Exception
     */
    public function lancerMultipleAction()
    {
        $names = explode(',', $this->params('names', ''));

        foreach ($names as $name) {
            $synchro = $this->synchroService->getSynchroByName($name);
            $result = $this->synchroService->runSynchro($synchro);

            if (! $this->getRequest()->isXmlHttpRequest()) {
                $this->flashMessages($synchro, $result);
            }
        }

        if ($this->getRequest()->isXmlHttpRequest()) {
            return false;
        } else {
            $redirect = $this->params()->fromQuery('redirect') ?: $this->url()->fromRoute('unicaen-db-import/synchro');

            return $this->redirect()->toUrl($redirect);
        }
    }

    protected function flashMessages(SynchroInterface $import, SynchroResult $result)
    {
        if ($exception = $result->getFailureException()) {
            $message = sprintf("La synchro '%s' a échoué." . PHP_EOL, $import);
            foreach (Result::getFailureExceptionGenerator($exception) as $e) {
                $message .= "Raison : " . $e->getMessage() . PHP_EOL;
            }
            $this->flashMessenger()->addErrorMessage(str_replace(PHP_EOL, '<br>', $message));
        } else {
            $message = sprintf("Synchro '%s' exécutée avec succès en %s." . PHP_EOL . PHP_EOL, $import, $result->getDurationToString());
            if ($result->hasExceptionInResults()) {
                $message .= "Néanmoins, un problème a été rencontré." . PHP_EOL . PHP_EOL;
            }
            $message .= $result->toHtmlString(false) . PHP_EOL;
            $this->flashMessenger()->addSuccessMessage(str_replace(PHP_EOL, '<br>', $message));
        }
    }

    /**
     * @return \Laminas\View\Model\JsonModel
     */
    public function lastLogAction(): JsonModel
    {
        $name = $this->params('name');
        $synchro = $this->synchroService->getSynchroByName($name);
        $importLog = $this->importLogService->findLastLogForSynchro($synchro);

        if ($importLog === null) {
            return new JsonModel(null);
        }

        return new JsonModel([
            'endedOn' => $importLog->getEndedOnToString(),
            'isSuccess' => $importLog->isSuccess(),
        ]);
    }
}