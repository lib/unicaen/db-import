<?php

namespace UnicaenDbImport\Controller;

use Exception;
use Unicaen\Console\ColorInterface;
use Unicaen\Console\Request;
use Unicaen\Console\Controller\AbstractConsoleController;
use UnicaenDbImport\Domain\Exception\NotFoundException;
use UnicaenDbImport\Domain\ImportInterface;
use UnicaenDbImport\Domain\SynchroInterface;
use UnicaenDbImport\Service\ImportService;
use UnicaenDbImport\Service\SynchroService;

class ConsoleController extends AbstractConsoleController
{
    /**
     * @var ImportService
     */
    private $importService;

    /**
     * @param ImportService $importService
     */
    public function setImportService(ImportService $importService)
    {
        $this->importService = $importService;
    }

    /**
     * @var SynchroService
     */
    private $synchroService;

    /**
     * @param SynchroService $synchroService
     */
    public function setSynchroService(SynchroService $synchroService)
    {
        $this->synchroService = $synchroService;
    }

    /**
     * @param ImportInterface[] $imports
     * @throws \UnicaenDbImport\Service\Exception\DatabaseServiceException
     */
    private function runImports(array $imports)
    {
        if (count($imports) === 0) {
            $this->getConsole()->writeLine("Aucun import réalisé.", ColorInterface::NORMAL);

            return;
        }

        foreach ($imports as $import) {

            $this->getConsole()->writeLine("### " . $import->getName() . " ###", ColorInterface::BLACK, ColorInterface::WHITE);
            $this->getConsole()->writeLine(date_format(date_create(), 'd/m/Y H:i:s'), ColorInterface::NORMAL);
            $this->getConsole()->writeLine();

            $result = $this->importService->runImport($import);

            if ($exception = $result->getFailureException()) {
                $this->getConsole()->writeLine("Une erreur a été rencontrée!", ColorInterface::RED);
                $this->getConsole()->writeLine($exception->getMessage(), ColorInterface::RED);
                $this->getConsole()->writeLine($exception->getTraceAsString(), ColorInterface::GRAY);
                while ($exception = $exception->getPrevious()) {
                    $this->getConsole()->writeLine("------", ColorInterface::RED);
                    $this->getConsole()->writeLine("Erreur précédente :", ColorInterface::RED);
                    $this->getConsole()->writeLine($exception->getMessage(), ColorInterface::RED);
                    $this->getConsole()->writeLine($exception->getTraceAsString(), ColorInterface::GRAY);
                }
            }
            else {
                $this->getConsole()->writeLine((string)$result);
                $this->getConsole()->writeLine("Import réalisé avec succès.", ColorInterface::GREEN);
            }

            $startDate = $result->getStartDate();
            $endDate = $result->getEndDate();
            $diffDate = date_diff($startDate, $endDate);

            $this->getConsole()->writeLine($diffDate->format('%i min %s sec.'), ColorInterface::NORMAL);
            $this->getConsole()->writeLine();
        }
    }

    /**
     * @param SynchroInterface[] $synchros
     * @throws \UnicaenDbImport\Service\Exception\DatabaseServiceException
     */
    private function runSynchros(array $synchros)
    {
        if (count($synchros) === 0) {
            $this->getConsole()->writeLine("Aucune synchro réalisée.", ColorInterface::NORMAL);

            return;
        }

        foreach ($synchros as $synchro) {

            $this->getConsole()->writeLine("### " . $synchro->getName() . " ###", ColorInterface::BLACK, ColorInterface::WHITE);
            $this->getConsole()->writeLine(date_format(date_create(), 'd/m/Y H:i:s'), ColorInterface::NORMAL);

            $result = $this->synchroService->runSynchro($synchro);

            if ($exception = $result->getFailureException()) {
                $this->getConsole()->writeLine("Une erreur a été rencontrée!", ColorInterface::RED);
                $this->getConsole()->writeLine($exception->getMessage(), ColorInterface::RED);
                $this->getConsole()->writeLine($exception->getTraceAsString(), ColorInterface::GRAY);
                while ($exception = $exception->getPrevious()) {
                    $this->getConsole()->writeLine("------", ColorInterface::RED);
                    $this->getConsole()->writeLine("Erreur précédente :", ColorInterface::RED);
                    $this->getConsole()->writeLine($exception->getMessage(), ColorInterface::RED);
                    $this->getConsole()->writeLine($exception->getTraceAsString(), ColorInterface::GRAY);
                }
            }
            
            $this->getConsole()->writeLine($result->toString());
            $this->getConsole()->writeLine("Synchro terminée.", ColorInterface::NORMAL);

            $startDate = $result->getStartDate();
            $endDate = $result->getEndDate();
            $diffDate = date_diff($startDate, $endDate);

            $this->getConsole()->writeLine($diffDate->format('%i min %s sec.'), ColorInterface::NORMAL);
            $this->getConsole()->writeLine();
        }
    }

    public function listImportsAction(): void
    {
        $this->getConsole()->writeLine();
        $this->getConsole()->writeLine("######################## LISTE DES IMPORTS ########################", ColorInterface::BLUE);
        $this->getConsole()->writeLine();

        if ($imports = $this->importService->getImports()) {
            $this->getConsole()->writeLine(sprintf('(%s) %s', "Order", "Name"));
            $this->getConsole()->writeLine('-----------------------------------------');
            foreach ($imports as $import) {
                $this->getConsole()->writeLine(sprintf('(%s) %s', $import->getOrder(), $import->getName()));
            }
        } else {
            $this->getConsole()->writeLine("Aucun import trouvé.");
        }
        
        $this->getConsole()->writeLine();
    }

    /**
     * @throws Exception
     */
    public function runImportAction()
    {
        /** @var Request $request */
        $request = $this->getRequest();
        $name = $request->getParam('name');

        $this->getConsole()->writeLine();
        $this->getConsole()->writeLine("######################## IMPORTS ########################", ColorInterface::BLUE);
        $this->getConsole()->writeLine();

        try {
            if ($name === null) {
                $imports = $this->importService->getImports();
            } else {
                try {
                    $import = $this->importService->getImportByName($name);
                    $imports = [$import];
                }
                catch (NotFoundException $exception) {
                    $imports = [];
                    $this->getConsole()->writeLine($exception->getMessage(), ColorInterface::RED);
                }
            }

            $this->runImports($imports);

        } catch (Exception $exception) {
            $this->getConsole()->writeLine("Une erreur imprévue est survenue durant l'import !", ColorInterface::RED);
            throw $exception;
        }

        $this->getConsole()->writeLine();
    }

    public function listSynchrosAction(): void
    {
        $this->getConsole()->writeLine();
        $this->getConsole()->writeLine("######################## LISTE DES SYNCHROS ########################", ColorInterface::BLUE);
        $this->getConsole()->writeLine();

        if ($synchros = $this->synchroService->getSynchros()) {
            $this->getConsole()->writeLine(sprintf('(%s) %s', "Order", "Name"));
            $this->getConsole()->writeLine('-----------------------------------');
            foreach ($synchros as $synchro) {
                $this->getConsole()->writeLine(sprintf('(%s) %s', $synchro->getOrder(), $synchro->getName()));
            }
        } else {
            $this->getConsole()->writeLine("Aucune synchro trouvée.");
        }

        $this->getConsole()->writeLine();
    }

    /**
     * @throws Exception
     */
    public function runSynchroAction()
    {
        /** @var Request $request */
        $request = $this->getRequest();
        $name = $request->getParam('name');

        $this->getConsole()->writeLine();
        $this->getConsole()->writeLine("######################## SYNCHROS ########################", ColorInterface::BLUE);
        $this->getConsole()->writeLine();

        try {
            if ($name === null) {
                $synchros = $this->synchroService->getSynchros();
            } else {
                try {
                    $synchro = $this->synchroService->getSynchroByName($name);
                    $synchros = [$synchro];
                }
                catch (NotFoundException $exception) {
                    $synchros = [];
                    $this->getConsole()->writeLine($exception->getMessage(), ColorInterface::RED);
                }
            }

            $this->runSynchros($synchros);

        } catch (Exception $exception) {
            $this->getConsole()->writeLine("Une erreur imprévue est survenue durant la synchro !", ColorInterface::RED);
            throw $exception;
        }

        $this->getConsole()->writeLine();
    }

}