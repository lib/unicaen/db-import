<?php

namespace UnicaenDbImport\Privilege;

use UnicaenPrivilege\Provider\Privilege\Privileges;

class ObservationPrivilege extends Privileges
{
    const LISTER = 'unicaen-db-import-observation-lister';
    const CONSULTER_RESULTAT = 'unicaen-db-import-observation-consulter-resultat';
}