<?php

namespace UnicaenDbImport\Privilege;

use UnicaenPrivilege\Provider\Privilege\Privileges;

class SynchroPrivilege extends Privileges
{
    const LISTER = 'unicaen-db-import-synchro-lister';
    const CONSULTER = 'unicaen-db-import-synchro-consulter';
    const LANCER = 'unicaen-db-import-synchro-lancer';
}