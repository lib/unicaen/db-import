<?php

namespace UnicaenDbImport\Privilege;

use UnicaenPrivilege\Provider\Privilege\Privileges;

class ImportPrivilege extends Privileges
{
    const LISTER = 'unicaen-db-import-import-lister';
    const CONSULTER = 'unicaen-db-import-import-consulter';
    const LANCER = 'unicaen-db-import-import-lancer';
}