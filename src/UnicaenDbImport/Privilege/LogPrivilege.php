<?php

namespace UnicaenDbImport\Privilege;

use UnicaenPrivilege\Provider\Privilege\Privileges;

class LogPrivilege extends Privileges
{
    const LISTER = 'unicaen-db-import-log-lister';
    const CONSULTER = 'unicaen-db-import-log-consulter';
}