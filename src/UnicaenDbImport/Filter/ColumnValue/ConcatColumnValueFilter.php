<?php

namespace UnicaenDbImport\Filter\ColumnValue;

use InvalidArgumentException;

class ConcatColumnValueFilter extends AbstractColumnValueFilter
{
    /**
     * Noms ordonnés de colonnes/attributs existantes dont on concaténera les valeurs pour générer la valeur
     * de la colonne/attribut calculée.
     */
    protected array $columns;

    protected string $separator = ' - ';

    public function __toString(): string
    {
        return sprintf("Concaténation des valeurs des colonnes %s (séparées par '%s') dans la colonne %s",
            implode (', ', array_map(fn($c) => "'$c'", $this->columns)),
            $this->separator,
            $this->column
        );
    }

    public function setParams(array $params): void
    {
        if (array_key_exists('columns', $params)) {
            $this->columns = (array) $params['columns'];
        }
        if (array_key_exists('separator', $params)) {
            $this->separator = $params['separator'];
        }

        parent::setParams($params);
    }

    public function filter($value): array
    {
        if (!is_array($value)) {
            throw new InvalidArgumentException("Les données reçues ne sont pas un tableau");
        }
        if (!array_key_exists($this->column, $value)) {
            throw new InvalidArgumentException("La colonne '$this->column' est introuvable dans les données reçues");
        }

        $value[$this->column] = implode(
            $this->separator,
            array_map(
                function($column) use ($value) {
                    if (!array_key_exists($column, $value)) {
                        // si la colonne est introuvable (ex: '['), on la considère comme une chaîne à concaténer
                        return $column;
                    }
                    return $value[$column];
                },
                $this->columns
            )
        );

        return $value;
    }
}