<?php

namespace UnicaenDbImport\Filter\ColumnValue;

abstract class AbstractColumnValueFilter implements ColumnValueFilterInterface
{
    protected string $column;
    protected array $params = [];

    public function __construct(array $params = [])
    {
        $this->setParams($params);
    }

    public function setColumn(string $column): static
    {
        $this->column = $column;
        $this->columnName = $column;
        return $this;
    }

    public function setParams(array $params): void
    {
        $this->params = $params;

        if (array_key_exists('column', $params) || array_key_exists('column_name', $params)) {
            $this->setColumn($params['column'] ?: $params['column_name']);
        }
    }


    /////////////////////////////////////////// @deprecated /////////////////////////////////////////////

    /**
     * @deprecated Remplacée par {@see static::$column}
     */
    protected string $columnName;

    /**
     * @deprecated Remplacée par {@see setColumn()}
     */
    public function setColumnName(string $column): static
    {
        return $this->setColumn($column);
    }
}