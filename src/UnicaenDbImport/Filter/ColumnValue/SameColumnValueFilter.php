<?php

namespace UnicaenDbImport\Filter\ColumnValue;

use InvalidArgumentException;

class SameColumnValueFilter extends AbstractColumnValueFilter
{
    /**
     * Colonne/attribut existante dont on écrasera la valeur avec celle de la colonne/attribut source.
     */
    protected string $column;

    /**
     * Colonne/attribut source existante dont on prendra la valeur.
     */
    private string $sourceColumn;

    public function __toString(): string
    {
        return sprintf(
            "Colonne '%s' = colonne '%s'",
            $this->column,
            $this->sourceColumn,
        );
    }

    public function setParams(array $params): void
    {
        if (array_key_exists('source_column', $params)) {
            $this->setSourceColumn($params['source_column']);
        }

        parent::setParams($params);
    }

    /**
     * Spécifie la colonne/attribut source dont on prendra la valeur.
     */
    public function setSourceColumn(string $sourceColumn): void
    {
        $this->sourceColumn = $sourceColumn;
    }

    public function filter($value): array
    {
        if (!is_array($value)) {
            throw new InvalidArgumentException("Les données reçues ne sont pas un tableau");
        }
        if (!array_key_exists($this->column, $value)) {
            throw new InvalidArgumentException("La colonne suivante est introuvable dans les données reçues : " . $this->column);
        }
        if (!array_key_exists($this->sourceColumn, $value)) {
            throw new InvalidArgumentException("La colonne source suivante est introuvable dans les données reçues : " . $this->column);
        }

        $value[$this->column] = $value[$this->sourceColumn];

        return $value;
    }
}