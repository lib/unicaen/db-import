<?php

namespace UnicaenDbImport\Filter\ColumnValue;

use Laminas\Filter\FilterInterface;

/**
 * Interface de transformation d'une valeur de colonne/attribut
 * en une autre valeur.
 *
 * @author Unicaen
 */
interface ColumnValueFilterInterface extends FilterInterface
{
    /**
     * Retourne un texte expliquant ce que fait ce filtre.
     *
     * @return string
     */
    public function __toString(): string;

    /**
     * Spécifie le nom de la colonne/attribut dont on veut calculer/transformer la valeur.
     *
     * @param string $column Nom de colonne/attribut
     */
    public function setColumn(string $column): static;

    /**
     * Permet de fournir d'éventuels attributs libres complémentaires (contexte, etc.)
     *
     * @param array $params
     */
    public function setParams(array $params): void;

    /**
     * Transforme la valeur de la colonne/attribut dans les données spécifiées.
     *
     * @param array $value Ex: ['code' => 'UMR6211', ...] avec {@see $column} = 'code'
     * @return array Ex: ['code' => 'UCN::UMR6211', ...]
     */
    /**
     * Transforme la valeur de la colonne/attribut dans les données spécifiées.
     *
     * @param array $value Ligne possédant la colonne/attribut dont la valeur doit être transformée
     * @return mixed Ligne après transformation
     */
    public function filter($value): array;
}