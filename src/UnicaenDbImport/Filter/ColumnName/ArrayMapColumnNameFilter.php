<?php

namespace UnicaenDbImport\Filter\ColumnName;

use UnicaenDbImport\Utils;

class ArrayMapColumnNameFilter implements ColumnNameFilterInterface
{
    /**
     * Tableau de correspondance ['colonne_ou_attribut_source' => 'colonne_destination'].
     *
     * @var array Ex: ['nomDeFamille' => 'NOM_USUEL', 'prenom' => 'PRENOM']
     */
    protected $mapping = [];

    /**
     * @var array
     */
    protected $normalizedMapping = [];

    /**
     * @param array $mapping Tableau de correspondance ['colonne_ou_attribut_source' => 'colonne_destination'].
     */
    public function __construct(array $mapping = [])
    {
        $this->setMapping($mapping);
    }

    /**
     * @inheritDoc
     */
    public function __toString(): string
    {
        return Utils::associativeArrayToString($this->mapping);
    }

    /**
     * @param array $mapping
     * @return \UnicaenDbImport\Filter\ColumnName\ArrayMapColumnNameFilter
     */
    public function setMapping(array $mapping): self
    {
        $this->mapping = $mapping;

        $keysToLower = array_map('strtolower', $keys = array_keys($mapping));
        $keysToUpper = array_map('strtoupper', $keys);

        $this->normalizedMapping = array_merge(
            array_combine($keysToLower, $mapping),
            array_combine($keysToUpper, $mapping),
        );

        return $this;
    }

    /**
     * Retourne le nom de colonne dans la destination à partir du nom de colonne/attribut dans la source.
     *
     * @param string $name Nom de colonne ou attribut dans la source
     * @return string Nom de colonne dans la destination
     */
    public function filter(string $name): string
    {
        return $this->normalizedMapping[strtolower($name)] ?? $this->normalizedMapping[strtoupper($name)] ?? $name;
    }
}