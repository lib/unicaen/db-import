<?php

namespace UnicaenDbImport\Filter\ColumnName;

/**
 * Interface de traduction/transformation d'un nom de colonne/attribut issu de la source
 * en le nom de colonne destination.
 *
 * @author Unicaen
 */
interface ColumnNameFilterInterface
{
    /**
     * Retourne une représentation littérale de ce filtre.
     *
     * @return string
     */
    public function __toString(): string;

    /**
     * Fournit le nom de colonne dans la destination à partir du nom de colonne/attribut dans la source.
     *
     * @param string $name Nom de colonne/attribut dans la source
     * @return string Nom de colonne dans la destination
     */
    public function filter(string $name): string;
}