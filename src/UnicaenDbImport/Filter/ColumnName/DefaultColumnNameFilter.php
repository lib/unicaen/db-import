<?php

namespace UnicaenDbImport\Filter\ColumnName;

use Laminas\Filter\FilterChain;
use Laminas\Filter\StringToUpper;
use Laminas\Filter\Word\CamelCaseToUnderscore;

/**
 * Filtre de transformation par défaut d'un nom de colonne/attribut issu de la source
 * en le nom de colonne destination.
 *
 * Exemples :
 * - 'dateDebValidite'     => 'DATE_DEB_VALIDITE'
 * - 'date_debut_validite' => 'DATE_DEB_VALIDITE'
 * - 'DATE_DEB_VALIDITE'   => 'DATE_DEB_VALIDITE'
 *
 * @author Unicaen
 */
class DefaultColumnNameFilter implements ColumnNameFilterInterface
{
    /**
     * @inheritDoc
     */
    public function __toString(): string
    {
        return "attributExemple => ATTRIBUT_EXEMPLE";
    }

    /**
     * @inheritDoc
     */
    public function filter(string $name): string
    {
        $chain = new FilterChain();

        // dateDebValidite => date_Deb_Validite
        $chain->attach(new CamelCaseToUnderscore());

        // date_Deb_Validite => DATE_DEB_VALIDITE
        $chain->attach(new StringToUpper());

        return $chain->filter($name);
    }
}