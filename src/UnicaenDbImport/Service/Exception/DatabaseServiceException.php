<?php

namespace UnicaenDbImport\Service\Exception;

use Exception;

class DatabaseServiceException extends Exception
{
    static public function error(string $message = null, Exception $previous = null): DatabaseServiceException
    {
        return new static($message, null, $previous);
    }
}