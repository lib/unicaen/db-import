<?php

namespace UnicaenDbImport\Service\Exception;

use Exception;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\ServerException;
use GuzzleHttp\Psr7\Message;
use Psr\Http\Message\ResponseInterface;
use RuntimeException;

class ApiServiceException extends Exception
{
    static public function unexpectedError(Exception $exception, string $uri): ApiServiceException
    {
        return new self("Erreur inattendue rencontrée lors de l'envoi de la requête $uri au WS", null, $exception);
    }

    static public function badConfig(Exception $previous = null): ApiServiceException
    {
        return new self("La config fournie est incorrecte.", null, $previous);
    }

    static public function emptyResponse(string $uri): ApiServiceException
    {
        return new self("Réponse du WS vide (URI '$uri') !");
    }

    static public function unexpectedResponse(ResponseInterface $response, string $uri): ApiServiceException
    {
        return new self("Réponse inattendue du WS à la requête $uri ! Status : " . $response->getStatusCode());
    }

    static public function invalidJSONResponse(string $responseBody, string $uri): ApiServiceException
    {
        return new self(sprintf(
            "Erreur rencontrée lors du décodage de la réponse JSON du WS (URI '%s') : %s. Début de la réponse reçue : %s",
            $uri, json_last_error_msg(), static::truncatedString($responseBody, 100)
        ));
    }

    static public function clientError(ClientException $error, string $uri): ApiServiceException
    {
        return new self("Erreur ClientException rencontrée lors de l'envoi de la requête $uri au WS", $error->getCode(), $error);
    }

    static public function serverError(ServerException $error, string $uri): ApiServiceException
    {
        $message = "Erreur distante rencontrée par le serveur du WS lors de la requête $uri";
        $previous = $error;
        if ($error->hasResponse() && $error->getResponse()->getBody()->getSize() > 0) {
            $previous = new RuntimeException($error->getResponse()->getBody(), null, $error);
        }

        return new self($message, null, $previous);
    }

    static public function networkError(RequestException $error, string $uri): ApiServiceException
    {
        $message = "Erreur réseau rencontrée lors de l'envoi de la requête $uri au WS";
        if ($error->hasResponse()) {
            $message .= " : " . Message::toString($error->getResponse());
        }

        return new self($message, null, $error);
    }


    /**
     * Tronque une chaîne de caractères au dernier espace trouvé dans les N premiers caractères de celle-ci.
     *
     * @param string $string Chaîne de caractères à tronquer
     * @param int $length N
     * @param string $appended Ajouté à la fin de la chaîne tronquée
     */
    static protected function truncatedString(string $string, int $length = 60, string $appended = '...'): string
    {
        if (strlen($string) <= $length) {
            return $string;
        }

        $trunc = substr($string, 0, $length);
        if ($string[$length] === ' ') {
            $kept = $trunc;
        } else {
            $found = strrpos($trunc, ' '); // position du dernier espace
            if ($found === false) {
                $kept = $trunc;
            } else {
                $kept = substr($string, 0, $found); // on tronque à l'espace trouvé
            }
        }

        return $kept . $appended;
    }
}