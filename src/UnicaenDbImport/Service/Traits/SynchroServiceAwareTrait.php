<?php

namespace UnicaenDbImport\Service\Traits;

use UnicaenDbImport\Service\SynchroService;

trait SynchroServiceAwareTrait
{
    /**
     * @var SynchroService
     */
    protected $synchroService;

    /**
     * @param SynchroService $synchroService
     * @return self
     */
    public function setSynchroService(SynchroService $synchroService): self
    {
        $this->synchroService = $synchroService;
        return $this;
    }
}