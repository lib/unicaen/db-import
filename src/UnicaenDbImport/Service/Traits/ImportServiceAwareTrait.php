<?php

namespace UnicaenDbImport\Service\Traits;

use UnicaenDbImport\Service\ImportService;

trait ImportServiceAwareTrait
{
    /**
     * @var ImportService
     */
    protected $importService;

    /**
     * @param ImportService $importService
     * @return self
     */
    public function setImportService(ImportService $importService): self
    {
        $this->importService = $importService;
        return $this;
    }
}