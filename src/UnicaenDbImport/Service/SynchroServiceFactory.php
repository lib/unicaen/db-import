<?php

namespace UnicaenDbImport\Service;

use Psr\Container\ContainerInterface;
use Psr\Log\NullLogger;
use UnicaenDbImport\Config\Config;
use UnicaenDbImport\Service\Facade\LogFacadeService;
use UnicaenDbImport\Service\Facade\SynchroFacadeService;

class SynchroServiceFactory
{
    /**
     * @param \Interop\Container\ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return \UnicaenDbImport\Service\SynchroService
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container, string $requestedName, ?array $options = null): SynchroService
    {
        /** @var \Psr\Log\LoggerInterface $logger */
        $logger = $options['logger'] ?? new NullLogger();

        /** @var Config $config */
        $config = $container->get(Config::class);

        /** @var SynchroFacadeService $synchroFacadeService */
        $synchroFacadeService = $container->get(SynchroFacadeService::class);
        $synchroFacadeService->setLogger($logger);

        /** @var LogFacadeService $logFacadeService */
        $logFacadeService = $container->get(LogFacadeService::class);
        $logFacadeService->setLogger($logger);

        $service = new SynchroService($synchroFacadeService, $logFacadeService, $config);
        $service->setLogger($logger);

        return $service;
    }
}