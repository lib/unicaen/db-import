<?php

namespace UnicaenDbImport\Service\Api;

use Assert\Assertion;
use Assert\AssertionFailedException;
use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\ServerException;
use Laminas\Http\Response;
use Psr\Log\LoggerAwareTrait;
use stdClass;
use RuntimeException;
use UnicaenDbImport\Connection\ApiConnection;
use UnicaenDbImport\Domain\DestinationInterface;
use UnicaenDbImport\Domain\SourceInterface;
use UnicaenDbImport\Service\Api\Response\DataExtractor\ResponseDataExtractorsAwareTrait;
use UnicaenDbImport\Service\Exception\ApiServiceException;

/**
 * Service dédié à l'envoi de requêtes au Web Service.
 */
class ApiService
{
    use LoggerAwareTrait;
//    use DataNormalizersAwareTrait;
    use ResponseDataExtractorsAwareTrait;

    /**
     * @var array
     */
    protected $config;

    /**
     * @var Client
     */
    protected $client;

    /**
     * @var SourceInterface $source
     */
    protected $source;

    /**
     * @var DestinationInterface $destination
     */
    protected $destination;

    /**
     * @param SourceInterface $source
     */
    public function setSource(SourceInterface $source): void
    {
        $this->source = $source;
    }

    /**
     * @param DestinationInterface $destination
     */
    public function setDestination(DestinationInterface $destination): void
    {
        $this->destination = $destination;
    }

    /**
     * @param array $config
     * @return self
     * @throws \UnicaenDbImport\Service\Exception\ApiServiceException
     */
    protected function setConfig(array $config): self
    {
        try {
            $this->loadConfig($config);
        } catch (AssertionFailedException $e) {
            throw ApiServiceException::badConfig($e);
        }

        $this->client = null; // nécessaire puisque la config change

        return $this;
    }

    /**
     * Interroge l'API pour obtenir tous les enregistrements.
     *
     * @param ApiConnection $connection
     * @param SourceInterface $source
     * @return stdClass[]
     * @throws \UnicaenDbImport\Service\Exception\ApiServiceException
     */
    public function fetchAll(ApiConnection $connection, SourceInterface $source): array
    {
        $this->setConfig($connection->toArray());

        $url = $this->buildUrl($connection, $source);

        // pagination éventuelle
        $pageSize = $source->getPageSize();
        $pageParam = $connection->getPageParam();
        $pageSizeParam = $connection->getPageSizeParam();

        return $this->callApi($url, $pageSize, 0, $pageSizeParam, $pageParam);
    }

    /**
     * Interroge l'API pour obtenir seulement le 1er enregistrement ($pageSize = $page = 1).
     *
     * @param ApiConnection $connection
     * @param SourceInterface $source
     * @return stdClass[]
     * @throws \UnicaenDbImport\Service\Exception\ApiServiceException
     */
    public function fetchFirstRow(ApiConnection $connection, SourceInterface $source): array
    {
        $this->setConfig($connection->toArray());

        $url = $this->buildUrl($connection, $source);

        $pageSize = $page = 1; // un seul enregistrement
        $pageParam = $connection->getPageParam();
        $pageSizeParam = $connection->getPageSizeParam();

        return $this->callApi($url, $pageSize, $page, $pageSizeParam, $pageParam);
    }

    /**
     * Interrogation de l'API.
     *
     * NB : Dès lors que $pageSize > 0, l'interrogation se fait page par page.
     *
     * @param string $url
     * @param integer $pageSize
     * @param int $page
     * @param string $pageSizeParam
     * @param string $pageParam
     * @return stdClass[]
     * @throws \UnicaenDbImport\Service\Exception\ApiServiceException
     */
    protected function callApi(
        string $url,
        int $pageSize,
        int $page = 0,
        string $pageSizeParam = 'page_size',
        string $pageParam = 'page'): array
    {
        $paginated = $pageSize > 0;
        $onePageOnly = $page <> 0;

        $rows = [];
        $p = 1;
        do {
            $finalUrl = $url;

            $params = [];
            if ($paginated) {
                $params[$pageParam] = $p;
                $params[$pageSizeParam] = $pageSize;
            }
            if (!empty($params)) {
                $sep = !str_contains($finalUrl, '?') ? '?' : '&';
                $finalUrl .= $sep . http_build_query($params);
            }

            $responseData = $this->get($finalUrl);

            $knownPageCount = $this->extractDataPageCountFromResponse($responseData);
            $data = $this->extractDataFromResponse($responseData);

            $rows = array_merge($rows, $data);
            $p++;

            if (! $paginated) {
                break;
            }
            if ($onePageOnly) {
                break;
            }
            if (count($data) === 0) {
                break;
            }
            if ($knownPageCount > 0 && $p > $knownPageCount) {
                break;
            }
        }
        while (true);

        return $rows;
    }

    /**
     * @param ApiConnection   $connection
     * @param SourceInterface $source
     * @return string
     */
    private function buildUrl(ApiConnection $connection, SourceInterface $source): string
    {
        $url = $connection->getUrl();

        $select = $source->getSelect();
        if ($select) {
            $url = rtrim($url, '/');
            $select = ltrim($select, '/');
            $url = $url . '/' . $select;
        }

        return $url;
    }

    /**
     * Envoie une requête quelconque au web service, ex: 'version/current'.
     *
     * @param string $uri
     * @return stdClass|stdClass[]
     * @throws \UnicaenDbImport\Service\Exception\ApiServiceException
     */
    private function get(string $uri)
    {
        return $this->sendRequest($uri);
    }

    /**
     * @param array $config
     * @throws AssertionFailedException
     */
    private function loadConfig(array $config)
    {
        Assertion::keyIsset($config, 'url');

        $this->config = [
            'base_uri' => $config['url'],
            'headers' => [
                'Accept' => 'application/json',
            ],
        ];

        if (array_key_exists('user', $config)) {
            $this->config['auth'] = [
                $config['user'],
                $config['password']
            ];
        }

        if (array_key_exists('verify', $config)) {
            $this->config['verify'] = $config['verify'];
        }
        if (array_key_exists('timeout', $config)) {
            $this->config['timeout'] = $config['timeout'];
        }
        if (array_key_exists('connect_timeout', $config)) {
            $this->config['connect_timeout'] = $config['connect_timeout'];
        }
        if (array_key_exists('proxy', $config)) {
            $this->config['proxy'] = $config['proxy'];
        } else {
            $this->config['proxy'] = ['no' => 'localhost'];
        }
    }

    /**
     * @param Client $client
     * @return self
     */
    public function setClient(Client $client): self
    {
        $this->client = $client;

        return $this;
    }

    /**
     * @return Client
     */
    public function getClient(): Client
    {
        if ($this->client === null) {
            $this->client = $this->createClient();
        }

        return $this->client;
    }

    /**
     * @return Client
     */
    private function createClient(): Client
    {
        if ($this->config === null) {
            throw new RuntimeException("Une config doit être fournie au préalable.");
        }

        return new Client($this->config);
    }

    /**
     * Appel du Web Service d'import de données.
     *
     * @param string $uri : la "page" du Web Service à interroger
     * @return stdClass|stdClass[] Réponse du Web Service au format JSON
     *
     * @throws \UnicaenDbImport\Service\Exception\ApiServiceException
     */
    private function sendRequest(string $uri)
    {
        $client = $this->getClient();

        $response = null;
        try {
            $_debut = microtime(true);
            $response = $client->request('GET', $uri);
            $_fin = microtime(true);
            $this->logger->debug(sprintf("Interrogation du WS %s en %.2f s.", $uri, $_fin - $_debut));
        } catch (ClientException $e) {
            throw ApiServiceException::clientError($e, $uri);
        } catch (ServerException $e) {
            throw ApiServiceException::serverError($e, $uri);
        } catch (RequestException $e) {
            throw ApiServiceException::networkError($e, $uri);
        } catch (GuzzleException $e) {
        } catch (Exception $e) {
            throw ApiServiceException::unexpectedError($e, $uri);
        }
        if (!$response) {
            throw ApiServiceException::emptyResponse($uri);
        }

        if ($response->getStatusCode() !== Response::STATUS_CODE_200) {
            throw ApiServiceException::unexpectedResponse($response, $uri);
        }

        $body = $response->getBody();

        /** @var stdClass|stdClass[] $json */
        $json = json_decode($body);

        if ($json === null) {
            // NULL is returned if the json cannot be decoded or if the encoded data is deeper than the recursion limit.
            throw ApiServiceException::invalidJSONResponse((string)$body, $uri);
        }

        return $json;
    }
}