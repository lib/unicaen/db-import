<?php

namespace UnicaenDbImport\Service\Api\Response\DataExtractor\Hal;

use stdClass;
use UnicaenDbImport\Service\Api\Response\DataExtractor\ResponseDataExtractorInterface;
use Webmozart\Assert\Assert;

/**
 * Extraction des données dans une réponse d'API au format "Hypertext Application Language" (HAL).
 *
 * @see https://api-tools.getlaminas.org/documentation/api-primer/halprimer
 */
class HalResponseDataExtractor implements ResponseDataExtractorInterface
{
    public function canExtractFromResponse($response): bool
    {
        return $response instanceof stdClass && isset($response->page_count) && isset($response->_embedded);
    }

    /**
     * @param stdclass $response Réponse de la forme `{ "page_count": 5, "_embedded": { "financements": [{}, {}, ...] }, ...}`
     */
    public function extractDataPageCountFromResponse($response): int
    {
        return (int) $response->page_count;
    }

    /**
     * @param stdclass $response Réponse de la forme `{ "page_count": 5, "_embedded": { "financements": [{}, {}, ...] }, ...}`
     */
    public function extractDataFromResponse($response): array
    {
        Assert::true($this->canExtractFromResponse($response), "Cet extractor ne sait pas traiter les données spécifiées !");

        $embedded = get_object_vars($response->_embedded);
        $data = (array) reset($embedded);

        // suppression de la clé '_links' de chaque ligne de données
        array_walk($data, function(stdClass $obj) {
            unset($obj->_links);
        });

        return $data;
    }
}