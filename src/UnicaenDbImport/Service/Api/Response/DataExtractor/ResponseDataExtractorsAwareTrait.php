<?php

namespace UnicaenDbImport\Service\Api\Response\DataExtractor;

use LogicException;
use SplPriorityQueue;

trait ResponseDataExtractorsAwareTrait
{
    protected ?SplPriorityQueue $responseDataExtractors = null;

    /**
     * Insert dans la queue un extractor avec la priorité spécifiée.
     */
    public function addResponseDataExtractor(ResponseDataExtractorInterface $extractor, int $priority): void
    {
        if ($this->responseDataExtractors === null) {
            $this->responseDataExtractors = new SplPriorityQueue();
        }

        $this->responseDataExtractors->insert($extractor, $priority);
    }

    /**
     * Extrait le nombre de pages de données éventuellement indiquée dans la réponse spécifiée, à l'aide du 1er extractor compétent trouvé.
     *
     * @see ResponseDataExtractorInterface::extractDataPageCountFromResponse()
     */
    protected function extractDataPageCountFromResponse($response): int
    {
        $dataExtractor = $this->retrieveDataExtractorForResponse($response);

        return $dataExtractor->extractDataPageCountFromResponse($response);
    }

    /**
     * Extrait les données de la réponse spécifiée, à l'aide du 1er extractor compétent trouvé.
     *
     * @see ResponseDataExtractorInterface::extractDataFromResponse()
     */
    protected function extractDataFromResponse($response): array
    {
        $dataExtractor = $this->retrieveDataExtractorForResponse($response);

        return $dataExtractor->extractDataFromResponse($response);
    }

    /**
     * Retourne le 1er extractor compétent pour la réponse spécifiée.
     */
    protected function retrieveDataExtractorForResponse($response): ResponseDataExtractorInterface
    {
        /** @var ResponseDataExtractorInterface $dataExtractor */
        foreach ($this->responseDataExtractors as $dataExtractor) {
            if ($dataExtractor->canExtractFromResponse($response)) {
                return $dataExtractor;
            }
        }

        throw new LogicException("Aucun extractor compétent n'a été trouvé");
    }
}