<?php

namespace UnicaenDbImport\Service\Api\Response\DataExtractor\Array;

use UnicaenDbImport\Service\Api\Response\DataExtractor\ResponseDataExtractorInterface;
use Webmozart\Assert\Assert;

/**
 * Extractor pour le cas où les données reçues d'une API sont un simple tableau.
 */
class ArrayResponseDataExtractor implements ResponseDataExtractorInterface
{
    public function canExtractFromResponse($response): bool
    {
        return is_array($response);
    }

    public function extractDataPageCountFromResponse($response): int
    {
        return 0; // non applicable
    }

    public function extractDataFromResponse($response): array
    {
        Assert::true($this->canExtractFromResponse($response), "Cet extractor ne sait pas traiter les données spécifiées !");

        return $response;
    }
}