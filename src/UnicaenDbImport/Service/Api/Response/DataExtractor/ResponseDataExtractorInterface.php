<?php

namespace UnicaenDbImport\Service\Api\Response\DataExtractor;

interface ResponseDataExtractorInterface
{
    /**
     * Détermine si cet extractor est de mesure de traiter la réponse spécifiée.
     */
    public function canExtractFromResponse($response): bool;

    /**
     * Extrait le nombre de pages de données éventuellement indiqué dans la réponse spécifiée,
     * à l'aide du 1er extractor compétent trouvé.
     *
     * @return int Nombre de pages de données, ou 0 s'il n'a pu être déterminé.
     */
    public function extractDataPageCountFromResponse($response): int;

    /**
     * Extrait les données de la réponse spécifiée.
     */
    public function extractDataFromResponse($response): array;
}