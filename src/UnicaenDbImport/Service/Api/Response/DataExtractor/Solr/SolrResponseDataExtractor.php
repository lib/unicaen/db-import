<?php

namespace UnicaenDbImport\Service\Api\Response\DataExtractor\Solr;

use stdClass;
use UnicaenDbImport\Service\Api\Response\DataExtractor\ResponseDataExtractorInterface;
use Webmozart\Assert\Assert;

/**
 * Extraction des données dans une réponse d'API au format "Solr JSON".
 *
 * @see https://solr.apache.org/guide/solr/latest/query-guide/response-writers.html#json-response-writer
 */
class SolrResponseDataExtractor implements ResponseDataExtractorInterface
{
    public function canExtractFromResponse($response): bool
    {
        return $response instanceof stdClass && isset($response->response->numFound) && isset($response->response->docs);
    }

    public function extractDataPageCountFromResponse($response): int
    {
        return 0; // non connu
    }

    public function extractDataFromResponse($response): array
    {
        Assert::true($this->canExtractFromResponse($response), "Cet extractor ne sait pas traiter les données spécifiées !");

        return (array) $response->response->docs;
    }
}