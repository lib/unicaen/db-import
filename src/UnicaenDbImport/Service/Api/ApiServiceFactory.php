<?php

namespace UnicaenDbImport\Service\Api;

use Psr\Container\ContainerInterface;
use Psr\Log\NullLogger;
use UnicaenDbImport\Service\Api\Response\DataExtractor\Array\ArrayResponseDataExtractor;
use UnicaenDbImport\Service\Api\Response\DataExtractor\Hal\HalResponseDataExtractor;
use UnicaenDbImport\Service\Api\Response\DataExtractor\Solr\SolrResponseDataExtractor;

class ApiServiceFactory
{
    /**
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container): ApiService
    {
        $service = new ApiService();
        $service->setLogger(new NullLogger()); // logger par défaut

        // response data extractors (priorisés)
        $halDataExtractor = $container->get(HalResponseDataExtractor::class); /** @var HalResponseDataExtractor $halDataExtractor */
        $solrDataExtractor = $container->get(SolrResponseDataExtractor::class); /** @var SolrResponseDataExtractor $solrDataExtractor */
        $arrayDataExtractor = $container->get(ArrayResponseDataExtractor::class); /** @var ArrayResponseDataExtractor $arrayDataExtractor */
        $service->addResponseDataExtractor($halDataExtractor, 30); // le + prioritaire
        $service->addResponseDataExtractor($solrDataExtractor, 20);
        $service->addResponseDataExtractor($arrayDataExtractor, 10);

        return $service;
    }
}