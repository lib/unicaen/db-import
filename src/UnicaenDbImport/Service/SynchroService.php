<?php

namespace UnicaenDbImport\Service;

use Exception;
use Psr\Log\LoggerAwareTrait;
use RuntimeException;
use UnicaenDbImport\Config\Config;
use UnicaenDbImport\Domain\Exception\NotFoundException;
use UnicaenDbImport\Domain\NameAwareInterface;
use UnicaenDbImport\Domain\SynchroInterface;
use UnicaenDbImport\Domain\SynchroResult;
use UnicaenDbImport\Service\Facade\LogFacadeService;
use UnicaenDbImport\Service\Facade\SynchroFacadeService;

class SynchroService
{
    use LoggerAwareTrait;

    /**
     * @var Config
     */
    protected $config;

    /**
     * @var \UnicaenDbImport\Service\Facade\LogFacadeService
     */
    protected $logFacadeService;

    /**
     * @var \UnicaenDbImport\Service\Facade\SynchroFacadeService
     */
    private $synchroFacadeService;

    /**
     * SynchroService constructor.
     *
     * @param \UnicaenDbImport\Service\Facade\SynchroFacadeService $synchroFacadeService
     * @param \UnicaenDbImport\Service\Facade\LogFacadeService $logFacadeService
     * @param Config $config
     */
    public function __construct(
        SynchroFacadeService $synchroFacadeService,
        LogFacadeService     $logFacadeService,
        Config               $config)
    {
        $this->synchroFacadeService = $synchroFacadeService;
        $this->logFacadeService = $logFacadeService;
        $this->config = $config;
    }

    /**
     * Retourne toutes les synchros issues de la config.
     *
     * @return SynchroInterface[]
     */
    public function getSynchros(): array
    {
        return $this->config->getSynchros();
    }

    /**
     * Retourne les synchros (bruts) issus de la config, filtrées selon les critères spécifiés.
     *
     * @param array $filters
     * @return SynchroInterface[]
     */
    public function getSynchrosFilteredBy(array $filters): array
    {
        $objects = $this->config->getSynchros();

        if ($text = $filters['text'] ?? null) {
            $objects = array_filter($objects, function (NameAwareInterface $o) use ($text) {
                return preg_match('#' . $text . '#i', $o->getName());
            });
        }

        return $objects;
    }

    /**
     * Lance tous les synchros.
     *
     * @return SynchroResult[] Agrégat des résultats des exécutions des synchros
     *
     * @throws \UnicaenDbImport\Service\Exception\DatabaseServiceException
     */
    public function runAllSynchros(): array
    {
        $synchros = $this->getSynchros();
        $results = [];

        foreach ($synchros as $synchro) {
            $results[$synchro->getName()] = $this->runSynchro($synchro);
        }

        return $results;
    }

    /**
     * Retourne la synchro dont le nom est spécifié.
     *
     * @param string $name Nom de la synchro (name)
     * @return SynchroInterface Synchro trouvée
     *
     * @throws NotFoundException Synchro introuvable
     */
    public function getSynchroByName(string $name): SynchroInterface
    {
        $synchro = $this->config->getSynchro($name);
        if ($synchro === null) {
            throw NotFoundException::synchroByName($name);
        }

        return $synchro;
    }

    /**
     * Lance la synchro spécifiée.
     *
     * @param SynchroInterface $synchro
     * @return SynchroResult Résultat de l'exécution de la synchro
     *
     * @throws \UnicaenDbImport\Service\Exception\DatabaseServiceException
     */
    public function runSynchro(SynchroInterface $synchro): SynchroResult
    {
        $this->logger->info("Lancement de la synchro " . $synchro);

        $this->synchroFacadeService->setSynchro($synchro);
        $this->logFacadeService->setDestination($synchro->getDestination());

        $startDate = date_create();

        try {
            $this->logFacadeService->createImportLog();
            $this->synchroFacadeService->validateDestination();
            $this->synchroFacadeService->validateSource();
            $this->synchroFacadeService->discoverSourceColumns();

            $result = $this->synchroFacadeService->synchronizeDestination();
        } catch (Exception $e) {
            $result = new SynchroResult();
            $result->setFailureException($e);
        }

        $result->setSynchro($synchro);
        $result->setStartDate($startDate);
        $result->setEndDate();

        $this->logFacadeService->saveResultToImportLog($result);

        if ($this->logger) {
            $this->logger->info($result);
        }

        return $result;
    }

    /**
     * @param \UnicaenDbImport\Domain\SynchroInterface $synchro
     * @return array
     */
    public function fetchSynchroDiff(SynchroInterface $synchro): array
    {
        $this->synchroFacadeService->setSynchro($synchro);

        try {
            $this->synchroFacadeService->validateDestination();
            $this->synchroFacadeService->validateSource();
            $this->synchroFacadeService->discoverSourceColumns();

            return $this->synchroFacadeService->fetchSynchroDiff();

        } catch (Exception $e) {
            throw new RuntimeException("Erreur rencontrée.", null, $e);
        }
    }

    /**
     * @param \UnicaenDbImport\Domain\SynchroInterface $synchro
     * @return bool
     */
    public function fetchSynchroDiffFound(SynchroInterface $synchro): bool
    {
        $this->synchroFacadeService->setSynchro($synchro);

        try {
            $this->synchroFacadeService->validateDestination();
            $this->synchroFacadeService->validateSource();
            $this->synchroFacadeService->discoverSourceColumns();

            return $this->synchroFacadeService->fetchSynchroDiffFound();

        } catch (Exception $e) {
            throw new RuntimeException("Erreur rencontrée : " . $e->getMessage(), null, $e);
        }
    }

    /**
     * @param \UnicaenDbImport\Domain\SynchroInterface $synchro
     * @return int
     */
    public function fetchSynchroDiffCount(SynchroInterface $synchro): int
    {
        $this->synchroFacadeService->setSynchro($synchro);

        try {
            $this->synchroFacadeService->validateDestination();
            $this->synchroFacadeService->validateSource();
            $this->synchroFacadeService->discoverSourceColumns();

            return $this->synchroFacadeService->fetchSynchroDiffCount();

        } catch (Exception $e) {
            throw new RuntimeException("Erreur rencontrée : " . $e->getMessage(), null, $e);
        }
    }

    /**
     * @param \UnicaenDbImport\Domain\SynchroInterface $synchro
     * @return string[] ['operation' => 'sql']
     */
    public function fetchSynchroSql(SynchroInterface $synchro): array
    {
        $this->synchroFacadeService->setSynchro($synchro);

        try {
            return $this->synchroFacadeService->generateSynchroSql();
        } catch (Exception $e) {
            throw new RuntimeException("Erreur rencontrée : " . $e->getMessage(), null, $e);
        }
    }
}