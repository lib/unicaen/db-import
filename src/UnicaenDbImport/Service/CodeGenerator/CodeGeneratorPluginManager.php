<?php

namespace UnicaenDbImport\Service\CodeGenerator;

use UnicaenDbImport\CodeGenerator\CodeGenerator;
use Laminas\ServiceManager\AbstractPluginManager;

class CodeGeneratorPluginManager extends AbstractPluginManager
{
    protected $instanceOf = CodeGenerator::class;
}