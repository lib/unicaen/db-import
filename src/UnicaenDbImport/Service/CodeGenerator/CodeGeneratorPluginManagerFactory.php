<?php

namespace UnicaenDbImport\Service\CodeGenerator;

use Psr\Container\ContainerInterface;
use Laminas\ServiceManager\Factory\FactoryInterface;
use UnicaenDbImport\Config\Config;

class CodeGeneratorPluginManagerFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return CodeGeneratorPluginManager
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null): CodeGeneratorPluginManager
    {
        $factoriesConfig = $this->getCodeGeneratorPluginFactories($container);

        return new CodeGeneratorPluginManager($container, [
            'factories' => $factoriesConfig,
        ]);
    }

    /**
     * @param ContainerInterface $container
     * @return array
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    protected function getCodeGeneratorPluginFactories(ContainerInterface $container): array
    {
        /** @var Config $config */
        $config = $container->get(Config::class);

        return $config->getCodeGeneratorsFactoriesConfig();
    }
}