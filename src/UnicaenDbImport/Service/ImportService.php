<?php

namespace UnicaenDbImport\Service;

use Exception;
use Psr\Log\LoggerAwareTrait;
use UnicaenDbImport\Config\Config;
use UnicaenDbImport\Domain\Exception\NotFoundException;
use UnicaenDbImport\Domain\ImportInterface;
use UnicaenDbImport\Domain\ImportResult;
use UnicaenDbImport\Domain\NameAwareInterface;
use UnicaenDbImport\Service\Facade\ImportFacadeService;
use UnicaenDbImport\Service\Facade\LogFacadeService;

class ImportService
{
    use LoggerAwareTrait;

    /**
     * @var Config
     */
    protected $config;

    /**
     * @var \UnicaenDbImport\Service\Facade\LogFacadeService
     */
    protected $logFacadeService;

    /**
     * @var \UnicaenDbImport\Service\Facade\ImportFacadeService
     */
    private $importFacadeService;

    /**
     * ImportService constructor.
     *
     * @param \UnicaenDbImport\Service\Facade\ImportFacadeService $importFacadeService
     * @param \UnicaenDbImport\Service\Facade\LogFacadeService $logFacadeService
     * @param Config $config
     */
    public function __construct(
        ImportFacadeService $importFacadeService,
        LogFacadeService    $logFacadeService,
        Config              $config)
    {
        $this->importFacadeService = $importFacadeService;
        $this->logFacadeService = $logFacadeService;
        $this->config = $config;
    }

    /**
     * Retourne tous les imports (bruts) issus de la config.
     *
     * @return ImportInterface[]
     */
    public function getImports(): array
    {
        return $this->config->getImports();
    }

    /**
     * Retourne les imports (bruts) issus de la config, filtrés selon les critères spécifiés.
     *
     * @param array $filters
     * @return ImportInterface[]
     */
    public function getImportsFilteredBy(array $filters): array
    {
        $objects = $this->config->getImports();

        if ($text = $filters['text'] ?? null) {
            $objects = array_filter($objects, function (NameAwareInterface $o) use ($text) {
                return preg_match('#' . $text . '#i', $o->getName());
            });
        }

        return $objects;
    }

    /**
     * Lance tous les imports.
     *
     * @return ImportResult[] Agrégat des résultats des exécutions des imports
     *
     * @throws \UnicaenDbImport\Service\Exception\DatabaseServiceException
     */
    public function runAllImports(): array
    {
        $imports = $this->getImports();
        $results = [];

        foreach ($imports as $import) {
            $results[$import->getName()] = $this->runImport($import);
        }

        return $results;
    }

    /**
     * Retourne l'import dont le nom est spécifié.
     *
     * @param string $name Nom de l'import (name)
     * @return ImportInterface Import trouvé
     *
     * @throws NotFoundException Import introuvable
     */
    public function getImportByName(string $name): ImportInterface
    {
        $import = $this->config->getImport($name);
        if ($import === null) {
            throw NotFoundException::importByName($name);
        }

        return $import;
    }

    /**
     * Lance l'import spécifié (import brut).
     *
     * @param ImportInterface $import
     * @return ImportResult Résultat de l'exécution de l'import
     *
     * @throws \UnicaenDbImport\Service\Exception\DatabaseServiceException
     */
    public function runImport(ImportInterface $import): ImportResult
    {
        $this->logger->info("Lancement de l'import " . $import);

        $this->importFacadeService->setImport($import);
        $this->logFacadeService->setDestination($import->getDestination());

        $startDate = date_create();

        // But de ce try-catch : si une erreur est rencontrée, elle est interceptée pour ne rien interrompre,
        // et mise dans le résultat de l'import.
        try {
            $this->logFacadeService->createImportLog();
            $this->importFacadeService->validateDestination();
            $this->importFacadeService->validateSource();
            $this->importFacadeService->fetchSource();
            $this->importFacadeService->discoverSourceColumns();

            // NB : si une erreur d'écriture en bdd est rencontrée, un rollback sera fait.
            $result = $this->importFacadeService->importToDestination();
        } catch (Exception $e) {
            $result = new ImportResult();
            $result->setFailureException($e);
        }

        $result->setImport($import);
        $result->setStartDate($startDate);
        $result->setEndDate();

        $this->logFacadeService->saveResultToImportLog($result);

        return $result;
    }
}