<?php

namespace UnicaenDbImport\Service\Database;

use Doctrine\DBAL\Exception;
use UnicaenDbImport\Domain\ResultInterface;
use UnicaenDbImport\Service\Exception\DatabaseServiceException;

class ImportLogDatabaseService extends AbstractDatabaseService
{
    /**
     * @throws \UnicaenDbImport\Service\Exception\DatabaseServiceException
     */
    public function createImportLogTableIfNotExists()
    {
        $connection = $this->destination->getConnection();
        $importLogTable = $this->destination->getLogTable();

        try {
            $this->assertTableExistsInDestination($importLogTable);
            return;
        } catch (DatabaseServiceException $e) {
            // la table n'existe pas
        }

        // create table
        $sql = $this->codeGeneratorForDestination->generateSQLForImportLogTableCreation($this->destination->getLogTable());
        try {
            $this->queryExecutor->exec($sql, $connection);
        } catch (Exception $e) {
            throw DatabaseServiceException::error("Erreur lors de la création de la table de log '$importLogTable'", $e);
        }
    }

    /**
     * @param ResultInterface $result
     * @throws \UnicaenDbImport\Service\Exception\DatabaseServiceException
     */
    public function saveResultToLogTable(ResultInterface $result)
    {
        $importLogTable = $this->destination->getLogTable();

        $sql = $this->codeGeneratorForDestination->generateSQLForInsertResultIntoLogTable($result, $importLogTable);
        try {
            $this->queryExecutor->exec($sql, $this->destination->getConnection());
        } catch (Exception $e) {
            throw DatabaseServiceException::error("Insert impossible dans la table de log '$importLogTable'", $e);
        }
    }
}