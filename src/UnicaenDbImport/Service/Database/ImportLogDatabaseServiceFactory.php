<?php

namespace UnicaenDbImport\Service\Database;

use Psr\Container\ContainerInterface;
use UnicaenDbImport\Config\Config;
use UnicaenDbImport\QueryExecutor;
use UnicaenDbImport\Service\CodeGenerator\CodeGeneratorPluginManager;

class ImportLogDatabaseServiceFactory
{
    /**
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container): ImportLogDatabaseService
    {
        /** @var Config $config */
        $config = $container->get(Config::class);

        /** @var \UnicaenDbImport\Service\CodeGenerator\CodeGeneratorPluginManager $codeGeneratorPluginManager */
        $codeGeneratorPluginManager = $container->get(CodeGeneratorPluginManager::class);

        $codeGeneratorsConfig = $config->getCodeGeneratorsMappingConfig();

        return new ImportLogDatabaseService(
            $codeGeneratorPluginManager,
            $codeGeneratorsConfig,
            new QueryExecutor()
        );
    }
}