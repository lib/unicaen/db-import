<?php

namespace UnicaenDbImport\Service\Database;

use Assert\Assertion;
use Assert\AssertionFailedException;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Exception;
use UnicaenDbImport\CodeGenerator\CodeGenerator;
use UnicaenDbImport\Domain\DestinationInterface;
use UnicaenDbImport\Domain\SourceInterface;
use UnicaenDbImport\QueryExecutor;
use UnicaenDbImport\Service\CodeGenerator\CodeGeneratorPluginManager;
use UnicaenDbImport\Service\Exception\DatabaseServiceException;

abstract class AbstractDatabaseService
{
    protected CodeGeneratorPluginManager $codeGeneratorPluginManager;
    protected array $codeGeneratorsConfig;
    protected CodeGenerator $codeGeneratorForSource;
    protected CodeGenerator $codeGeneratorForDestination;
    /** @var CodeGenerator[] */
    protected array $cachedCodeGenerators = [];

    protected QueryExecutor $queryExecutor;

    protected SourceInterface $source;
    protected DestinationInterface $destination;

    /**
     * DatabaseHelper constructor.
     *
     * @param CodeGeneratorPluginManager $codeGeneratorPluginManager
     * @param array                      $codeGeneratorsConfig
     * @param QueryExecutor              $queryExecutor
     */
    public function __construct(
        CodeGeneratorPluginManager $codeGeneratorPluginManager,
        array $codeGeneratorsConfig,
        QueryExecutor $queryExecutor)
    {
        $this->codeGeneratorPluginManager = $codeGeneratorPluginManager;
        $this->codeGeneratorsConfig = $codeGeneratorsConfig;
        $this->queryExecutor = $queryExecutor;
    }

    public function setSource(SourceInterface $source): void
    {
        $this->source = $source;

        if ($this->source->getConnection() instanceof Connection) {
            $this->initCodeGeneratorFromSource();
        }
    }

    public function setDestination(DestinationInterface $destination): void
    {
        $this->destination = $destination;

        $this->initCodeGeneratorFromDestination();
    }

    /**
     * Détermine, selon la plateforme de bdd source, le générateur de code SQL à utiliser.
     *
     * @throws \UnicaenDbImport\Service\Exception\DatabaseServiceException
     */
    public function initCodeGeneratorFromSource(): CodeGenerator
    {
        try {
            $platformClass = get_class($this->source->getConnection()->getDatabasePlatform());
        } catch (Exception $e) {
            throw DatabaseServiceException::error(
                "Impossible de déterminer la plateforme de la base de données source", $e);
        }

        $this->codeGeneratorForSource = $this->initCodeGeneratorForPlatform($platformClass);

        return $this->codeGeneratorForSource;
    }

    /**
     * Détermine, selon la plateforme de bdd destination, le générateur de code SQL à utiliser.
     *
     * @throws \UnicaenDbImport\Service\Exception\DatabaseServiceException
     */
    public function initCodeGeneratorFromDestination(): CodeGenerator
    {
        try {
            $platformClass = get_class($this->destination->getConnection()->getDatabasePlatform());
        } catch (Exception $e) {
            throw DatabaseServiceException::error(
                "Impossible de déterminer la plateforme de la base de données destination", $e);
        }

        $this->codeGeneratorForDestination = $this->initCodeGeneratorForPlatform($platformClass);

        return $this->codeGeneratorForDestination;
    }

    protected function initCodeGeneratorForPlatform(string $platformClass): CodeGenerator
    {
        if (! isset($this->cachedCodeGenerators[$platformClass])) {
            try {
                Assertion::keyExists($this->codeGeneratorsConfig, $platformClass);
            } catch (AssertionFailedException $e) {
                throw DatabaseServiceException::error(
                    "Aucun CodeGenerator configuré pour la plateforme de base de données suivante : " . $platformClass, $e);
            }

            $codeGeneratorClass = $this->codeGeneratorsConfig[$platformClass];
            $codeGenerator = $this->codeGeneratorPluginManager->get($codeGeneratorClass);

            $this->cachedCodeGenerators[$platformClass] = $codeGenerator;
        }

        return $this->cachedCodeGenerators[$platformClass];
    }

    /**
     * @throws \UnicaenDbImport\Service\Exception\DatabaseServiceException
     */
    protected function assertTableExistsInDestination(string $tableName, ?string $message = null): void
    {
        $sql = $this->codeGeneratorForDestination->generateSQLForTableExistenceCheck($tableName);
        try {
            $result = $this->queryExecutor->fetchAll($sql, $this->destination->getConnection());
        } catch (Exception $e) {
            throw DatabaseServiceException::error("Erreur lors du test d'existence de la table '$tableName'", $e);
        }
        $exists = $this->codeGeneratorForDestination->convertTableExistenceCheckResultToBoolean($result);
        if (!$exists) {
            throw DatabaseServiceException::error(sprintf(
                $message ?: "La table '%s' nécessaire au fonctionnement est introuvable dans la base destination. ",
                $tableName
            ));
        }
    }

    /**
     * @throws \UnicaenDbImport\Service\Exception\DatabaseServiceException
     */
    protected function rollBack(Connection $connection)
    {
        try {
            $connection->rollBack();
        } catch (Exception $e) {
            throw DatabaseServiceException::error("Erreur rencontrée lors du rollback !", $e);
        }
    }
}