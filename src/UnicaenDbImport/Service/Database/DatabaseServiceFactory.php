<?php

namespace UnicaenDbImport\Service\Database;

use Psr\Container\ContainerInterface;
use UnicaenDbImport\Config\Config;
use UnicaenDbImport\Entity\Db\Service\ImportObserv\ImportObservService;
use UnicaenDbImport\QueryExecutor;
use UnicaenDbImport\Service\CodeGenerator\CodeGeneratorPluginManager;

class DatabaseServiceFactory
{
    /**
     * @param ContainerInterface $container
     * @return DatabaseService
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container): DatabaseService
    {
        /** @var ImportObservService $importObservService */
        $importObservService = $container->get(ImportObservService::class);

        /** @var Config $config */
        $config = $container->get(Config::class);

        /** @var \UnicaenDbImport\Service\CodeGenerator\CodeGeneratorPluginManager $codeGeneratorPluginManager */
        $codeGeneratorPluginManager = $container->get(CodeGeneratorPluginManager::class);

        $codeGeneratorsConfig = $config->getCodeGeneratorsMappingConfig();

        $service = new DatabaseService(
            $codeGeneratorPluginManager,
            $codeGeneratorsConfig,
            new QueryExecutor()
        );
        $service->setImportObservService($importObservService);

        return $service;
    }
}