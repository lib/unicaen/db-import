<?php

namespace UnicaenDbImport\Service\Database;

use Doctrine\DBAL\Exception;
use UnicaenDbImport\Domain\DestinationInterface;
use UnicaenDbImport\Domain\Operation;
use UnicaenDbImport\Entity\Db\Service\ImportObserv\ImportObservServiceAwareTrait;
use UnicaenDbImport\Service\Exception\DatabaseServiceException;

class DatabaseService extends AbstractDatabaseService
{
    use ImportObservServiceAwareTrait;

    const SOURCE_TABLE_NAME = 'SOURCE';
    const SOURCE_TABLE_CODE_COLUMN = 'CODE';
    const DATA_SLICE_SIZE_FOR_INSERT = 1000;

    /**
     * Validation de la table destination.
     *
     * @throws \UnicaenDbImport\Service\Exception\DatabaseServiceException
     */
    public function validateDestinationTable(): void
    {
        $connection = $this->destination->getConnection();
        $table = $this->destination->getTable();
        $sourceCodeColumn = $this->destination->getSourceCodeColumn();

        // test if table exists
        $this->assertTableExistsInDestination($table, "La table destination '%s' est introuvable. ");

        // id column validation
        $columnCount = 0;
        $sql = $this->codeGeneratorForDestination->generateSQLForIdColumnValidationInTable($table, $columnCount);
        try {
            $result = $this->queryExecutor->fetchAll($sql, $connection);
        } catch (Exception $e) {
            throw DatabaseServiceException::error("Erreur lors de la validation de la colonne id dans la table destination '$table'", $e);
        }
        $exception = $this->codeGeneratorForDestination->convertIdColumnValidationBadResultToException($table, $result, $columnCount);
        if ($exception !== null) {
            throw DatabaseServiceException::error("La table '$table' n'est pas une table de destination valide. ", $exception);
        }

        if ($this->destination->getIdColumnStrategy() === DestinationInterface::ID_STRATEGY_SEQUENCE) {
            // existence de la sequence pour la colonne id
            $idColumnSequence = $this->destination->getIdColumnSequence();
            $sql = $this->codeGeneratorForDestination->generateSQLForSequenceExistenceCheck($table, $idColumnSequence);
            try {
                $result = $this->queryExecutor->fetchAll($sql, $connection);
            } catch (Exception $e) {
                throw DatabaseServiceException::error("Erreur lors du test d'existence de la séquence dans la table destination '$table'", $e);
            }
            $exception = $this->codeGeneratorForDestination->convertSequenceExistenceCheckResultToException($idColumnSequence, $result);
            if ($exception !== null) {
                throw DatabaseServiceException::error("La sequence '$idColumnSequence' est introuvable. ", $exception);
            }
        }

        // histo columns validation
        $columnCount = 0;
        $sql = $this->codeGeneratorForDestination->generateSQLForHistoColumnsValidationInTable($table, $columnCount);
        try {
            $result = $this->queryExecutor->fetchAll($sql, $connection);
        } catch (Exception $e) {
            throw DatabaseServiceException::error("Erreur lors de la validation des colonne d'historique dans la table destination '$table'", $e);
        }
        $exception = $this->codeGeneratorForDestination->convertHistoColumnsValidationBadResultToException($table, $result, $columnCount);
        if ($exception !== null) {
            throw DatabaseServiceException::error("La table '$table' n'est pas une table de destination valide. ", $exception);
        }

        // source columns validation
        $columnCount = 0;
        $sql = $this->codeGeneratorForDestination->generateSQLForSourceColumnsValidationInTable($table, $sourceCodeColumn, $columnCount);
        try {
            $result = $this->queryExecutor->fetchAll($sql, $connection);
        } catch (Exception $e) {
            throw DatabaseServiceException::error("Erreur lors de la validation des colonne de source dans la table destination '$table'", $e);
        }
        $exception = $this->codeGeneratorForDestination->convertSourceColumnsValidationBadResultToException($table, $sourceCodeColumn, $result, $columnCount);
        if ($exception !== null) {
            throw DatabaseServiceException::error("La table '$table' n'est pas une table de destination valide. ", $exception);
        }
    }

    /**
     * Validation de la table 'SOURCE' dans la destination.
     *
     * ATTENTION : on parle bien ici de la table nommée `SOURCE` qui doit exister dans la base de données destination,
     * et non pas de la table contenant les données sources de l'import/synchro.
     *
     * @throws \UnicaenDbImport\Service\Exception\DatabaseServiceException
     */
    public function validateSourceTable()
    {
        $tableName = self::SOURCE_TABLE_NAME;

        // test if table exists
        $this->assertTableExistsInDestination($tableName);
    }

    /**
     * Vérifie dans la table SOURCE qu'il existe bien une source ayant le 'code' spécifié.
     *
     * ATTENTION : on parle bien ici de la table nommée `SOURCE` qui doit exister dans la base de données destination,
     * et non pas de la table contenant les données sources de l'import/synchro.
     *
     * @throws \UnicaenDbImport\Service\Exception\DatabaseServiceException
     */
    public function validateCodeExistsInSourceTable(string $code)
    {
        $connection = $this->destination->getConnection();
        $tableName = self::SOURCE_TABLE_NAME;
        $column = self::SOURCE_TABLE_CODE_COLUMN;

        // test if value exists in table
        $sql = $this->codeGeneratorForDestination->generateSQLForValueExistenceCheckInTable($column, $code, $tableName);
        try {
            $result = $this->queryExecutor->fetchAll($sql, $connection);
        } catch (Exception $e) {
            throw DatabaseServiceException::error("Erreur lors du test d'existence de la valeur '$code' dans la table '$tableName'", $e);
        }
        $exists = $this->codeGeneratorForDestination->convertValueExistenceCheckInTableResultToBoolean($result);
        if (!$exists) {
            throw DatabaseServiceException::error("Vous devez déclarer une source dans la table '$tableName' dont le $column est '$code'.");
        }
    }

    /**
     * @throws \UnicaenDbImport\Service\Exception\DatabaseServiceException
     */
    public function checkIntermediateTableNotExists(string $intermediateTable)
    {
        try {
            $this->assertTableExistsInDestination($intermediateTable);
        } catch (DatabaseServiceException $e) {
            // :-) la table n'existe pas
            return;
        }

        throw DatabaseServiceException::error(
            "Une table $intermediateTable existe déjà. Veuillez la supprimer ou alors spécifier dans la config " .
            "le nom à utiliser pour générer la table intermédiaire nécessaire à l'import dans la table " .
            $this->destination->getTable());
    }

    /**
     * @throws \UnicaenDbImport\Service\Exception\DatabaseServiceException
     */
    public function createIntermediateTable($intermediateTable)
    {
        $sql = $this->codeGeneratorForDestination->generateSQLForIntermediateTableCreation($this->source, $this->destination, $intermediateTable);
        try {
            $this->queryExecutor->exec($sql, $this->destination->getConnection());
        } catch (Exception $e) {
            throw DatabaseServiceException::error("Erreur lors de la création de la table intermédiaire '$intermediateTable'", $e);
        }
    }

    /**
     * @throws \UnicaenDbImport\Service\Exception\DatabaseServiceException
     */
    public function dropIntermediateTable($intermediateTable)
    {
        $sql = $this->codeGeneratorForDestination->generateSQLForIntermmediateTableDrop($intermediateTable);
        try {
            $this->queryExecutor->exec($sql, $this->destination->getConnection());
        } catch (Exception $e) {
            throw DatabaseServiceException::error("Erreur lors de la suppression de la table intermédiaire '$intermediateTable'", $e);
        }
    }

    /**
     * @throws \UnicaenDbImport\Service\Exception\DatabaseServiceException
     */
    public function truncateDestinationTable()
    {
        $data = $this->source->getData();
        if (empty($data)) {
            return;
        }

        $table = $this->destination->getTable();

        // détermination de l'id de la source des données à insérer, pour n'effacer que les données de cette source
        $row1 = reset($data);
        $preparedRow1 = $this->prepareSourceDataRow($row1);
        $sourceId = $preparedRow1['source_id'] ?? $preparedRow1['SOURCE_ID'];
        if (!$sourceId) {
            throw DatabaseServiceException::error(
                "Les données sources ont un 'source_id' absent ou vide, cela empêche le vidage partiel de la table destination '$table'"
            );
        }

        $sql = $this->codeGeneratorForDestination->generateSQLForClearTable($table);
        $sql .= " WHERE source_id = (SELECT id FROM source WHERE code = '$sourceId')";
        try {
            $this->queryExecutor->exec($sql, $this->destination->getConnection());
        } catch (Exception $e) {
            throw DatabaseServiceException::error("Erreur lors du vidage de la table destination '$table'", $e);
        }
    }

    /**
     * @return int
     * @throws \UnicaenDbImport\Service\Exception\DatabaseServiceException
     */
    public function populateDestinationTableFromSource(): int
    {
        $destinationTable = $this->destination->getTable();
        $idColumnStrategy = $this->destination->getIdColumnStrategy();
        $idColumnSequence = $this->destination->getIdColumnSequence();

        $connection = $this->destination->getConnection();
        try {
            $connection->beginTransaction();
            $this->truncateDestinationTable();
            $count = $this->populateTableFromSource($destinationTable, $idColumnStrategy, $idColumnSequence);
            $connection->commit();
        } catch (DatabaseServiceException $e) {
            $this->rollBack($connection);
            throw $e;
        } catch (Exception $e) {
            $this->rollBack($connection);
            throw DatabaseServiceException::error("Erreur rencontrée en bdd : " . $e->getMessage(), $e);
        }

        return $count;
    }

    /**
     * @param string $tableName
     * @param string|null $idColumnStrategy
     * @param string|null $idColumnSequence
     * @return int
     * @throws \UnicaenDbImport\Service\Exception\DatabaseServiceException Aucune données dans la source ; ou anomalie ; ou erreur en bdd
     */
    public function populateTableFromSource(string $tableName, ?string $idColumnStrategy = null, ?string $idColumnSequence = null): int
    {
        $sourceCode = $this->source->getCode();
        $data = $this->source->getData();

        if (empty($data)) {
            return 0;
        }

        $count = 0;

        foreach ($data as $row) {
            $preparedRow = $this->prepareSourceDataRow($row);
            $columnsValues = $this->prepareDestinationData($preparedRow);

            $insertsSQL = $this->codeGeneratorForDestination->generateSQLForInsertOneRowIntoTable($tableName, $columnsValues, $sourceCode, $idColumnStrategy, $idColumnSequence);
            try {
                $n = $this->queryExecutor->exec($insertsSQL, $this->destination->getConnection());
            } catch (Exception $e) {
                throw DatabaseServiceException::error("Erreur rencontrée lors du remplissage de la table destination '$tableName'", $e);
            }
            if ($n === 0) {
                throw DatabaseServiceException::error(sprintf(
                    "Lors du remplissage de la table destination '$tableName', un insert au moins n'a eu aucun effet, ".
                    "sans doute parce que la source dont le '%s' est '%s' n'est pas déclarée dans la table %s",
                    self::SOURCE_TABLE_CODE_COLUMN, $preparedRow['SOURCE_ID'], self::SOURCE_TABLE_NAME
                ));
            }
            $count += $n;
        }

        return $count;
    }

    /**
     * @throws \UnicaenDbImport\Service\Exception\DatabaseServiceException
     */
    private function prepareSourceDataRow(array $row): array
    {
        $this->appendSourceComputedColumns($row);
        $this->applySourceColumnValueFilters($row);

        $row = $this->applySourceColumnNameFilters($row);

        // si la Source n'est pas fournie (par son 'code'), il faut que les données l'inclue (colonne 'SOURCE_ID')
        $sourceCode = $this->source->getCode();
        if ($sourceCode === null) {
            if (array_key_exists('source_id', $row)) {
                $row['SOURCE_ID'] = $row['source_id'];
            }
            elseif (!array_key_exists('SOURCE_ID', $row)) {
                throw DatabaseServiceException::error(
                    "Lorsque la config ne fournie pas le 'code' de la source ($this->source), " .
                    "les données doivent contenir une colonne SOURCE_ID ou source_id");
            }
        }
        else{
            $row['SOURCE_ID'] = $sourceCode;
        }

        return $row;
    }

    private function appendSourceComputedColumns(array &$row): void
    {
        foreach ($this->source->getComputedColumns() as $computedColumnName => $filter) {
            $row = $filter->filter($row);
        }
    }

    /**
     * Transformations éventuelles de valeurs de colonnes/attributs source
     * (application des filtres l'un après l'autre).
     */
    private function applySourceColumnValueFilters(array &$columnsValues): void
    {
        $columnValueFilters = $this->source->getColumnValueFilter();
        foreach ($columnValueFilters as $filter) {
            $filter->setParams($this->source->getExtra());
            $columnsValues = $filter->filter($columnsValues);
        }
    }

    /**
     * Transformations éventuelles de noms de colonnes/attributs.
     */
    private function applySourceColumnNameFilters(array $row): array
    {
        $columnNameFilter = $this->source->getColumnNameFilter();
        if ($columnNameFilter !== null) {
            $filteredKeys = array_map([$columnNameFilter, 'filter'], array_keys($row));
            $row = array_combine($filteredKeys, $row);
        }

        return $row;
    }

    private function prepareDestinationData(array $row): array
    {
        $columns = $this->source->getColumns();
        $sourceCodeColumn = $this->source->getSourceCodeColumn();
        $columns = array_merge([$sourceCodeColumn], $columns);

        // préparation des données :
        // - mise à NULL des colonnes pour lesquelles on n'a pas de valeur.
        $columnsValues = [];
        foreach ($columns as $column) {
            $columnsValues[$column] = $row[$column] ?? $row[strtolower($column)] ?? null;
        }

        // transformations éventuelles de valeurs de colonnes/attributs
        $this->applyDestinationColumnValueFilters($columnsValues);

        return $columnsValues;
    }

    /**
     * Transformations éventuelles de valeurs de colonnes/attributs destination
     * (application des filtres l'un après l'autre).
     */
    private function applyDestinationColumnValueFilters(array &$columnsValues): void
    {
        $columnValueFilters = $this->destination->getColumnValueFilter();
        foreach ($columnValueFilters as $filter) {
            $filter->setParams($this->destination->getExtra());
            foreach ($columnsValues as $columnName => $columnValue) {
                $columnsValues[$columnName] = $filter->setColumn($columnName)->filter($columnValue);
            }
        }
    }

    /**
     * @throws \UnicaenDbImport\Service\Exception\DatabaseServiceException
     */
    public function recreateDiffView(?string $intermediateTable = null): void
    {
        $destinationTable = $this->destination->getTable();
        $diffViewName = $this->codeGeneratorForDestination->generateDiffViewName($destinationTable);

        try {
            $sql = $this->codeGeneratorForDestination->generateSQLForDiffViewCreation($this->source, $this->destination, $diffViewName, $intermediateTable);
        } catch (Exception $e) {
            throw DatabaseServiceException::error("Erreur lors de la génération du SQL de création de la vue différentielle '$diffViewName'", $e);
        }
        try {
            $this->queryExecutor->exec($sql, $this->destination->getConnection());
        } catch (Exception $e) {
            throw DatabaseServiceException::error("Erreur lors de la création de la vue différentielle '$diffViewName'", $e);
        }
    }

    /**
     * @return array
     * @throws \UnicaenDbImport\Service\Exception\DatabaseServiceException
     */
    public function fetchDiffView(): array
    {
        $sql = $this->codeGeneratorForDestination->generateSQLForDiffViewSelect($this->source, $this->destination);

        try {
            return $this->queryExecutor->fetchAll($sql, $this->destination->getConnection());
        } catch (Exception $e) {
            throw DatabaseServiceException::error("Erreur lors de l'interrogation de la vue différentielle ", $e);
        }
    }

    /**
     * @throws \UnicaenDbImport\Service\Exception\DatabaseServiceException
     */
    public function fetchDiffViewFound(): bool
    {
        $sql = $this->codeGeneratorForDestination->generateSQLForDiffViewSelect($this->source, $this->destination);
        $sql = 'select count(*) as found from (' . $sql . ' limit 1) tmp';

        try {
            $result = $this->queryExecutor->fetch($sql, $this->destination->getConnection());
        } catch (Exception $e) {
            throw DatabaseServiceException::error("Erreur lors de l'interrogation de la vue différentielle ", $e);
        }

        return (bool) (int) $result['found'];
    }

    /**
     * @return int
     * @throws \UnicaenDbImport\Service\Exception\DatabaseServiceException
     */
    public function fetchDiffViewCount(): int
    {
        $sql = $this->codeGeneratorForDestination->generateSQLForDiffViewSelect($this->source, $this->destination);
        $sql = 'select count(*) as nb from (' . $sql . ') tmp';

        try {
            $result = $this->queryExecutor->fetch($sql, $this->destination->getConnection());
        } catch (Exception $e) {
            throw DatabaseServiceException::error("Erreur lors de l'interrogation de la vue différentielle ", $e);
        }

        return (int) $result['nb'];
    }

    /**
     * La table IMPORT_OBSERV permet de demander la "détection" de certains changements de valeurs
     * lors de la synchro. Les changements détectés sont inscrits dans la table IMPORT_OBSERV_RESULT.
     *
     * Cette méthode interroge la table IMPORT_OBSERV pour savoir quels changements de valeurs doivent être détectés
     * et peuple la table IMPORT_OBSERV_RESULT le cas échéant.
     *
     * @throws \UnicaenDbImport\Service\Exception\DatabaseServiceException
     */
    public function populateImportObservResultTable()
    {
//        $qb = $this->importObservService->getRepository()->createQueryBuilder('io')
//            ->andWhere("upper(io.tableName) = upper(:table)")->setParameter('table', $this->destination->getTable())
//            ->andWhere("io.operation = 'UPDATE'")
//            ->andWhere("io.enabled = 1")
//            ->addOrderBy('io.operation, io.tableName, io.columnName');
//        $importObservRows = $qb->getQuery()->getResult(Query::HYDRATE_ARRAY);
        $sql = $this->codeGeneratorForDestination->generateSQLForSelectingInImportObservTable($this->destination->getTable());
        try {
            $importObservRows = $this->queryExecutor->fetchAll($sql, $this->destination->getConnection());
        } catch (Exception $e) {
            throw DatabaseServiceException::error("Erreur lors du fetch des ImportObserv", $e);
        }

        if (!empty($importObservRows)) {
            $sql = $this->codeGeneratorForDestination->generateSQLForInsertionIntoImportObservResult($this->destination, $importObservRows);
            try {
                $this->queryExecutor->exec($sql, $this->destination->getConnection());
            } catch (Exception $e) {
                throw DatabaseServiceException::error("Erreur lors de la création des ImportObservResult", $e);
            }
        }
    }

    /**
     * @return array [operation => int|Exception]
     *
     * @throws \UnicaenDbImport\Service\Exception\DatabaseServiceException
     */
    public function updateDestination(): array
    {
        $connection = $this->destination->getConnection();
        $results = [];

        $sqls = $this->generateUpdateDestinationSql();
        foreach ($sqls as $operation => $sql) {
            try {
                $connection->beginTransaction();
                $result = $this->queryExecutor->exec($sql, $connection);
                $connection->commit();
            } catch (Exception $e) {
                $this->rollBack($connection);
                $result = DatabaseServiceException::error("Erreur lors de l'opération '$operation'", $e);
            }

            $results[$operation] = $result;
        }

        return $results;
    }

    /**
     * @return string[] ['operation' => 'sql']
     */
    public function generateUpdateDestinationSql(): array
    {
        $sqls = [];
        foreach (Operation::OPERATIONS as $operation) {
            $sqls[$operation] =
                $this->codeGeneratorForDestination->generateSQLForDestinationUpdate($operation, $this->source, $this->destination);
        }

        return $sqls;
    }

    /**
     * Requête la Source pour obtenir les données sources.
     *
     * @return array
     * @throws \UnicaenDbImport\Service\Exception\DatabaseServiceException
     */
    public function fetchSource(): array
    {
        $selectSql = $this->codeGeneratorForSource->generateSQLForSelectFromSource($this->source);

        try {
            return $this->queryExecutor->fetchAll($selectSql, $this->source->getConnection());
        } catch (Exception $e) {
            throw DatabaseServiceException::error("Erreur lors de l'interrogation de la table/select", $e);
        }
    }

    /**
     * Requête la Source pour obtenir le 1er enregistrement des données sources.
     *
     * @return array|null
     * @throws \UnicaenDbImport\Service\Exception\DatabaseServiceException
     */
    public function fetchSourceFirstRow(): ?array
    {
        $selectSql = $this->codeGeneratorForSource->generateSQLForSelectFromSource($this->source);

        try {
            return $this->queryExecutor->fetch($selectSql, $this->source->getConnection());
        } catch (Exception $e) {
            throw DatabaseServiceException::error("Erreur lors du fetch", $e);
        }
    }

    /**
     * @throws \UnicaenDbImport\Service\Exception\DatabaseServiceException
     */
    public function fetchSourceTableColumnNames(): array
    {
        $sourceTableName = $this->source->getTable();
        $selectSql = $this->codeGeneratorForSource->generateSQLForSelectSourceTableColumnsInformation($sourceTableName);

        try {
            $result = $this->queryExecutor->fetchAll($selectSql, $this->source->getConnection());
            if (!$result) {
                throw DatabaseServiceException::error("Impossible de déterminer les colonnes de la table/vue source '$sourceTableName'");
            }
            return array_map(fn ($row) => $row['column_name'], $result);
        } catch (Exception $e) {
            throw DatabaseServiceException::error("Erreur lors du fetch", $e);
        }
    }
}