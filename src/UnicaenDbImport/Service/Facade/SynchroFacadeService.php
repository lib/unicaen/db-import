<?php

namespace UnicaenDbImport\Service\Facade;

use Doctrine\DBAL\Connection as DbConnection;
use UnicaenDbImport\Domain\SynchroInterface;
use UnicaenDbImport\Domain\SynchroResult;

class SynchroFacadeService extends AbstractFacadeService
{
    /**
     * @var SynchroInterface
     */
    protected $synchro;

    /**
     * @var bool
     */
    protected $requiresIntermediateTable = false;

    /**
     * @param SynchroInterface $synchro
     */
    public function setSynchro(SynchroInterface $synchro)
    {
        $this->synchro = $synchro;
        $this->requiresIntermediateTable = $synchro->requiresIntermediateTable();
        $this->setSource($synchro->getSource());
        $this->setDestination($synchro->getDestination());
    }

    /**
     * @var bool
     */
    protected $useImportObserv = false;

    /**
     * @param bool $useImportObserv
     */
    public function setUseImportObserv(bool $useImportObserv)
    {
        $this->useImportObserv = $useImportObserv;
    }

    /**
     * @throws \UnicaenDbImport\Service\Exception\DatabaseServiceException
     */
    public function validateSource()
    {
        parent::validateSource();

        $connection = $this->source->getConnection();

        switch (true) {
            case $connection instanceof DbConnection:
                $this->databaseService->validateSourceTable();
                break;
        }
    }

    /**
     * @return array
     * @throws \UnicaenDbImport\Service\Exception\DatabaseServiceException
     * @throws \Exception
     */
    public function fetchSynchroDiff(): array
    {
        $intermediateTable = null;

        if ($this->requiresIntermediateTable) {
            $this->fetchSource();
            $intermediateTable = $this->destination->getIntermediateTable();
            if ($this->destination->getIntermediateTableAutoDrop()) {
                $this->logger->info("Suppression de la table intermédiaire $intermediateTable");
                $this->databaseService->dropIntermediateTable($intermediateTable);
            } else {
                $this->databaseService->checkIntermediateTableNotExists($intermediateTable);
            }

            $this->logger->info("Création de la table intermédiaire $intermediateTable");
            $this->databaseService->createIntermediateTable($intermediateTable);
            $this->logger->info("Peuplement de la table intermédiaire $intermediateTable");
            $this->databaseService->populateTableFromSource($intermediateTable);
        }

        $this->recreateDiffView();
        $result = $this->databaseService->fetchDiffView();

        if ($this->requiresIntermediateTable) {
            $this->logger->info("Suppression de la table intermédiaire");
            $this->databaseService->dropIntermediateTable($intermediateTable);
        }

        return $result;
    }

    /**
     * @throws \UnicaenDbImport\Service\Exception\DatabaseServiceException
     * @throws \UnicaenDbImport\Service\Exception\ApiServiceException
     */
    public function fetchSynchroDiffFound(): bool
    {
        $intermediateTable = null;

        if ($this->requiresIntermediateTable) {
            $this->fetchSource();
            $intermediateTable = $this->destination->getIntermediateTable();
            if ($this->destination->getIntermediateTableAutoDrop()) {
                $this->logger->info("Suppression de la table intermédiaire $intermediateTable");
                $this->databaseService->dropIntermediateTable($intermediateTable);
            } else {
                $this->databaseService->checkIntermediateTableNotExists($intermediateTable);
            }

            $this->logger->info("Création de la table intermédiaire $intermediateTable");
            $this->databaseService->createIntermediateTable($intermediateTable);
            $this->logger->info("Peuplement de la table intermédiaire $intermediateTable");
            $this->databaseService->populateTableFromSource($intermediateTable);
        }

        $this->recreateDiffView();
        $result = $this->databaseService->fetchDiffViewFound();

        if ($this->requiresIntermediateTable) {
            $this->logger->info("Suppression de la table intermédiaire");
            $this->databaseService->dropIntermediateTable($intermediateTable);
        }
        return $result;
    }

    /**
     * @return int
     * @throws \UnicaenDbImport\Service\Exception\DatabaseServiceException
     * @throws \UnicaenDbImport\Service\Exception\ApiServiceException
     */
    public function fetchSynchroDiffCount(): int
    {
        $intermediateTable = null;

        if ($this->requiresIntermediateTable) {
            $this->fetchSource();
            $intermediateTable = $this->destination->getIntermediateTable();
            if ($this->destination->getIntermediateTableAutoDrop()) {
                $this->logger->info("Suppression de la table intermédiaire $intermediateTable");
                $this->databaseService->dropIntermediateTable($intermediateTable);
            } else {
                $this->databaseService->checkIntermediateTableNotExists($intermediateTable);
            }

            $this->logger->info("Création de la table intermédiaire $intermediateTable");
            $this->databaseService->createIntermediateTable($intermediateTable);
            $this->logger->info("Peuplement de la table intermédiaire $intermediateTable");
            $this->databaseService->populateTableFromSource($intermediateTable);
        }

        $this->recreateDiffView();
        $result = $this->databaseService->fetchDiffViewCount();

        if ($this->requiresIntermediateTable) {
            $this->logger->info("Suppression de la table intermédiaire");
            $this->databaseService->dropIntermediateTable($intermediateTable);
        }
        return $result;
    }

    /**
     * @throws \UnicaenDbImport\Service\Exception\DatabaseServiceException
     */
    public function generateSynchroSql(): array
    {
        $this->recreateDiffView();

        return $this->databaseService->generateUpdateDestinationSql();
    }

    /**
     * @throws \UnicaenDbImport\Service\Exception\DatabaseServiceException
     * @throws \UnicaenDbImport\Service\Exception\ApiServiceException
     */
    public function synchronizeDestination(): SynchroResult
    {
        $intermediateTable = null;

        if ($this->requiresIntermediateTable) {
            $this->fetchSource();

            $intermediateTable = $this->destination->getIntermediateTable();
            if ($this->destination->getIntermediateTableAutoDrop()) {
                $this->logger->info("Suppression de la table intermédiaire $intermediateTable");
                $this->databaseService->dropIntermediateTable($intermediateTable);
            } else {
                $this->databaseService->checkIntermediateTableNotExists($intermediateTable);
            }

            $this->logger->info("Création de la table intermédiaire $intermediateTable");
            $this->databaseService->createIntermediateTable($intermediateTable);
            $this->logger->info("Peuplement de la table intermédiaire $intermediateTable");
            $this->databaseService->populateTableFromSource($intermediateTable);
        }

        $this->recreateDiffView();

        if ($this->useImportObserv) {
            $this->logger->info("Peuplement des ImportObservResult éventuels");
            $this->databaseService->populateImportObservResultTable();
        }

        $this->logger->info("Mise à jour de la destination " . $this->destination);
        $resultsByOperation = $this->databaseService->updateDestination();

        $result = new SynchroResult();
        $result->setResultsByOperation($resultsByOperation);
        if ($result->hasExceptionInResults()) {
            return $result;
        }

        if ($this->requiresIntermediateTable) {
            $this->logger->info("Suppression de la table intermédiaire");
            $this->databaseService->dropIntermediateTable($intermediateTable);
        }

        return $result;
    }

    /**
     * @throws \UnicaenDbImport\Service\Exception\DatabaseServiceException
     */
    protected function recreateDiffView()
    {
        $this->logger->info("Re/Création de la vue différentielle");

        $intermediateTable = null;
        if ($this->requiresIntermediateTable) {
            $intermediateTable = $this->destination->getIntermediateTable();
        }

        $this->databaseService->recreateDiffView($intermediateTable);
    }
}