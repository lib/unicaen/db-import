<?php

namespace UnicaenDbImport\Service\Facade;

use Psr\Container\ContainerInterface;
use Psr\Log\NullLogger;
use UnicaenDbImport\Service\Api\ApiService;
use UnicaenDbImport\Service\Database\DatabaseService;

class ImportFacadeServiceFactory
{
    /**
     * @param ContainerInterface $container
     * @return \UnicaenDbImport\Service\Facade\ImportFacadeService
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container): ImportFacadeService
    {
        /** @var DatabaseService $databaseService */
        $databaseService = $container->get(DatabaseService::class);

        /** @var \UnicaenDbImport\Service\Api\ApiService $apiService */
        $apiService = $container->get(ApiService::class);

        $facade = new ImportFacadeService($databaseService, $apiService);
        $facade->setLogger(new NullLogger());

        return $facade;
    }
}