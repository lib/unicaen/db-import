<?php

namespace UnicaenDbImport\Service\Facade;

use Psr\Container\ContainerInterface;
use Psr\Log\NullLogger;
use UnicaenDbImport\Service\Database\ImportLogDatabaseService;

class LogFacadeServiceFactory
{
    /**
     * @param ContainerInterface $container
     * @return LogFacadeService
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container): LogFacadeService
    {
        /** @var \UnicaenDbImport\Service\Database\ImportLogDatabaseService $importLogService */
        $importLogService = $container->get(ImportLogDatabaseService::class);

        $facade = new LogFacadeService($importLogService);
        $facade->setLogger(new NullLogger());

        return $facade;
    }
}