<?php

namespace UnicaenDbImport\Service\Facade;

use Psr\Container\ContainerInterface;
use Psr\Log\NullLogger;
use UnicaenDbImport\Config\Config;
use UnicaenDbImport\Service\Api\ApiService;
use UnicaenDbImport\Service\Database\DatabaseService;

class SynchroFacadeServiceFactory
{
    /**
     * @param ContainerInterface $container
     * @return SynchroFacadeService
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container): SynchroFacadeService
    {
        /** @var Config $config */
        $config = $container->get(Config::class);

        /** @var \UnicaenDbImport\Service\Database\DatabaseService $databaseService */
        $databaseService = $container->get(DatabaseService::class);

        /** @var ApiService $apiService */
        $apiService = $container->get(ApiService::class);

        $facade = new SynchroFacadeService($databaseService, $apiService);
        $facade->setUseImportObserv($config->getUseImportObserv());
        $facade->setLogger(new NullLogger());

        return $facade;
    }
}