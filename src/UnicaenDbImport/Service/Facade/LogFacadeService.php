<?php

namespace UnicaenDbImport\Service\Facade;

use Doctrine\DBAL\Connection as DbConnection;
use Psr\Log\LoggerAwareTrait;
use UnicaenDbImport\Domain\DestinationInterface;
use UnicaenDbImport\Domain\Exception\ConnectionException;
use UnicaenDbImport\Domain\ResultInterface;
use UnicaenDbImport\Service\Database\ImportLogDatabaseService;

class LogFacadeService
{
    use LoggerAwareTrait;
    
    protected ImportLogDatabaseService $importLogService;
    protected DestinationInterface $destination;
    protected bool $importLogTableCreated = false;

    /**
     * @param \UnicaenDbImport\Service\Database\ImportLogDatabaseService $importLogService
     */
    public function __construct(ImportLogDatabaseService $importLogService)
    {
        $this->importLogService = $importLogService;
    }

    /**
     * @param DestinationInterface $destination
     */
    public function setDestination(DestinationInterface $destination): void
    {
        $this->destination = $destination;
        $this->importLogService->setDestination($this->destination);
    }

    /**
     * @throws \UnicaenDbImport\Service\Exception\DatabaseServiceException
     */
    public function createImportLog()
    {
        if ($this->importLogTableCreated) {
            return;
        }

        $connection = $this->destination->getConnection();

        switch (true) {
            case $connection instanceof DbConnection:
                $this->importLogService->createImportLogTableIfNotExists();
                $this->importLogTableCreated = true;
                break;
            default:
                throw ConnectionException::unexpected($connection);
        }
    }

    /**
     * @param ResultInterface $result
     * @throws \UnicaenDbImport\Service\Exception\DatabaseServiceException
     */
    public function saveResultToImportLog(ResultInterface $result)
    {
        $this->logger->info("Enregistrement des résultats d'import/synchro");

        $connection = $this->destination->getConnection();

        switch (true) {
            case $connection instanceof DbConnection:
                $this->importLogService->saveResultToLogTable($result);
                break;
            default:
                throw ConnectionException::unexpected($connection);
        }
    }
}