<?php

namespace UnicaenDbImport\Service\Facade;

use Doctrine\DBAL\Connection as DbConnection;
use Exception;
use InvalidArgumentException;
use Psr\Log\LoggerAwareTrait;
use stdClass;
use RuntimeException;
use UnicaenDbImport\Connection\ApiConnection;
use UnicaenDbImport\Connection\NoConnection;
use UnicaenDbImport\Domain\DestinationInterface;
use UnicaenDbImport\Domain\Exception\ConnectionException;
use UnicaenDbImport\Domain\SourceInterface;
use UnicaenDbImport\Service\Api\ApiService;
use UnicaenDbImport\Service\Database\DatabaseService;
use UnicaenDbImport\Service\Exception\ApiServiceException;
use UnicaenDbImport\Service\Exception\DatabaseServiceException;

abstract class AbstractFacadeService
{
    use LoggerAwareTrait;

    /**
     * @var \UnicaenDbImport\Service\Database\DatabaseService
     */
    protected $databaseService;

    /**
     * @var ApiService
     */
    protected $apiService;

    /**
     * @var SourceInterface $source
     */
    protected $source;

    /**
     * @var DestinationInterface $destination
     */
    protected $destination;

    /**
     * FacadeService constructor.
     *
     * @param \UnicaenDbImport\Service\Database\DatabaseService $databaseService
     * @param ApiService $apiService
     */
    public function __construct(DatabaseService $databaseService, ApiService $apiService)
    {
        $this->databaseService = $databaseService;
        $this->apiService = $apiService;
    }

    /**
     * @param SourceInterface $source
     */
    protected function setSource(SourceInterface $source): void
    {
        $this->source = $source;
        $this->databaseService->setSource($this->source);
        $this->apiService->setSource($this->source);
    }

    /**
     * @param DestinationInterface $destination
     */
    protected function setDestination(DestinationInterface $destination): void
    {
        $this->destination = $destination;
        $this->databaseService->setDestination($this->destination);
        $this->apiService->setDestination($this->destination);
    }

    /**
     *
     * @throws \UnicaenDbImport\Service\Exception\DatabaseServiceException
     */
    public function validateSource()
    {
        $connection = $this->source->getConnection();

        switch (true) {
            case $connection instanceof NoConnection:
                // RAS
                break;
            case $connection instanceof DbConnection:
                $this->databaseService->validateSourceTable();
                break;
            case $connection instanceof ApiConnection:
                if ($code = $this->source->getCode()) {
                    $this->databaseService->validateCodeExistsInSourceTable($code);
                }
                break;
            default:
                throw ConnectionException::unexpected($connection);
        }
    }

    /**
     * @throws \UnicaenDbImport\Service\Exception\DatabaseServiceException
     */
    public function validateDestination()
    {
        $connection = $this->destination->getConnection();

        switch (true) {
            case $connection instanceof DbConnection:
                $this->databaseService->validateDestinationTable();
                $this->databaseService->validateSourceTable();
                break;
            default:
                throw ConnectionException::unexpected($connection);
        }
    }

    /**
     * Requête la Source pour obtenir les données sources.
     * Ces données sont ensuite disponibles dans la Source en question.
     *
     * @throws \UnicaenDbImport\Service\Exception\DatabaseServiceException
     * @throws \UnicaenDbImport\Service\Exception\ApiServiceException
     *
     * @see SourceInterface::getData()
     */
    public function fetchSource(): void
    {
        if (count($this->source->getData()) === 0) {

            $connection = $this->source->getConnection();

            $this->logger->info("Interrogation de la source " . $this->source);

            switch (true) {
                case $connection instanceof NoConnection:
                    // les données sont spécifiées manuellement pour ce type de connexion
                    $data = $connection->getData();
                    break;

                case $connection instanceof ApiConnection:
                    if ($this->logger) {
                        $this->apiService->setLogger($this->logger);
                    }
                    try {
                        $data = $this->apiService->fetchAll($connection, $this->source);
                    } catch (ApiServiceException $e) {
                        throw new ApiServiceException("Erreur rencontrée lors de l'obtention des données sources (API)", null, $e);
                    }
                    break;

                case $connection instanceof DbConnection:
                    try {
                        $data = $this->databaseService->fetchSource();
                    } catch (DatabaseServiceException $e) {
                        throw new DatabaseServiceException("Erreur rencontrée lors de l'obtention des données sources (DB)", null, $e);
                    }
                    break;

                default:
                    throw new InvalidArgumentException("Type de connexion source inattendue");
            }

            $this->source->setData($data);
        }
    }

    /**
     * Requête la Source pour déterminer la liste des colonnes.
     *
     * @throws \Exception
     * @see SourceInterface::getColumns()
     * @see SourceInterface::getData()
     */
    public function discoverSourceColumns(): void
    {
        // la liste des attributs/colonnes peut avoir été fournie dès la config
        if ($this->source->getColumns()) {
            return;
        }

        $connection = $this->source->getConnection();

        switch (true) {
            case $connection instanceof NoConnection:
                $data = $connection->getData();
                if (count($data) === 0) {
                    throw new InvalidArgumentException(sprintf(
                        "Le type de connexion %s suppose de fournir des données manuellement",
                        NoConnection::class
                    ));
                }
                $columns = $this->extractColumnsFromData($data);
                break;
            case $connection instanceof ApiConnection:
                $columns = $this->discoverSourceColumnsFromApi($connection);
                break;
            case $connection instanceof DbConnection:
                $columns = $this->discoverSourceColumnsFromDb($connection);
                break;
            default:
                throw new RuntimeException("Type de connexion source inattendue");
        }

        $this->source->setColumns($columns);
    }

    /**
     * @throws \Exception
     */
    protected function discoverSourceColumnsFromApi(ApiConnection $connection): array
    {
        if ($this->logger) {
            $this->apiService->setLogger($this->logger);
        }
        try {
            $data = $this->apiService->fetchFirstRow($connection, $this->source);
        } catch (ApiServiceException $e) {
            throw new Exception("Erreur rencontrée lors de l'obtention de la 1ere ligne des données sources (API)", null, $e);
        }

        return $this->extractColumnsFromData($data);
    }

    /**
     * @throws \Exception
     */
    protected function discoverSourceColumnsFromDb(DbConnection $connection): array
    {
        if ($this->source->getSelect()) {
            try {
                $firstRow = $this->databaseService->fetchSourceFirstRow();
            } catch (DatabaseServiceException $e) {
                throw new Exception("Erreur rencontrée lors de l'obtention de la 1ere ligne des données sources (DB)", null, $e);
            }
            if ($firstRow === null) {
                throw new RuntimeException(sprintf(
                    "Impossible de découvrir la liste des colonnes à partir des données sources suivantes " .
                    "car celles-ci sont vides : %s",
                    $this->source->getTable() ?: $this->source->getSelect()
                ));
            }
            $data = [$firstRow];
            $columns = $this->extractColumnsFromData($data);
        }
        else {
            try {
                $columns = $this->databaseService->fetchSourceTableColumnNames();
            } catch (DatabaseServiceException $e) {
                throw new Exception(
                    "Erreur rencontrée lors de l'obtention des colonnes de la table source : " . $e->getMessage(), null, $e);
            }
        }

        return $columns;
    }

    /**
     * @param array $rows
     * @return string[]
     */
    private function extractColumnsFromData(array $rows): array
    {
        $first = current($rows);

        if ($first instanceof stdClass) {
            $columns = array_keys(get_object_vars($first));
        } elseif (is_array($first)) {
            $columns = array_keys($first);
        } else {
            throw new RuntimeException(
                "Impossible de déterminer la liste des colonnes à partir des données car leur type est inconnu (ni array ni stdClass)");
        }

        return $columns;
    }
}