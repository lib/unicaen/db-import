<?php

namespace UnicaenDbImport\Service\Facade;

use Doctrine\DBAL\Connection as DbConnection;
use UnicaenDbImport\Domain\Exception\ConnectionException;
use UnicaenDbImport\Domain\ImportInterface;
use UnicaenDbImport\Domain\ImportResult;

class ImportFacadeService extends AbstractFacadeService
{
    /**
     * @var ImportInterface
     */
    protected $import;

    /**
     * @param ImportInterface $import
     */
    public function setImport(ImportInterface $import)
    {
        $this->import = $import;
        $this->setSource($import->getSource());
        $this->setDestination($import->getDestination());
    }

    /**
     * @return ImportResult
     * @throws \UnicaenDbImport\Service\Exception\DatabaseServiceException
     */
    public function importToDestination(): ImportResult
    {
        $connection = $this->destination->getConnection();

        $this->logger->info("Peuplement de la destination " . $this->destination);

        switch (true) {
            case $connection instanceof DbConnection:
                $count = $this->databaseService->populateDestinationTableFromSource();

                $result = new ImportResult();
                $result->setDestinationTablePopulateResult($count);

                return $result;
            default:
                throw ConnectionException::unexpected($connection);
        }
    }
}