<?php

namespace UnicaenDbImport\Service;

use Psr\Container\ContainerInterface;
use Psr\Log\NullLogger;
use UnicaenDbImport\Config\Config;
use UnicaenDbImport\Service\Facade\ImportFacadeService;
use UnicaenDbImport\Service\Facade\LogFacadeService;

class ImportServiceFactory
{
    /**
     * @param \Interop\Container\ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return \UnicaenDbImport\Service\ImportService
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container, string $requestedName, ?array $options = null): ImportService
    {
        /** @var \Psr\Log\LoggerInterface $logger */
        $logger = $options['logger'] ?? new NullLogger();

        /** @var Config $config */
        $config = $container->get(Config::class);

        /** @var ImportFacadeService $importFacadeService */
        $importFacadeService = $container->get(ImportFacadeService::class);
        $importFacadeService->setLogger($logger);

        /** @var LogFacadeService $logFacadeService */
        $logFacadeService = $container->get(LogFacadeService::class);
        $logFacadeService->setLogger($logger);

        $service = new ImportService($importFacadeService, $logFacadeService, $config);
        $service->setLogger($logger);

        return $service;
    }
}