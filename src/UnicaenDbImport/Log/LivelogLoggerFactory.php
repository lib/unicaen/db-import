<?php

namespace UnicaenDbImport\Log;

use Monolog\Formatter\LineFormatter;
use Monolog\Handler\NoopHandler;
use Monolog\Logger;
use Psr\Container\ContainerInterface;
use UnicaenLivelog\Log\LivelogSocketLogHandler;

class LivelogLoggerFactory
{
    /**
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container): Logger
    {
        // le module unicaen/livelog peut ne pas être activé, on retourne alors un Logger Noop
        if (!$container->has(LivelogSocketLogHandler::class)) {
            return new Logger('LivelogFakeLogger', [new NoopHandler()]);
        }

        /** @var LivelogSocketLogHandler $livelogSocketHandler */
        $livelogSocketHandler = $container->get(LivelogSocketLogHandler::class);
        $livelogSocketHandler->setFormatter(new LineFormatter(null, 'd/m/Y H:i:s'));

        $logger = new Logger('livelog');
        $logger->pushHandler($livelogSocketHandler);

        return $logger;
    }
}