<?php

namespace UnicaenDbImport\ORM\Event\Listeners;

use Psr\Container\ContainerInterface;
use UnicaenDbImport\Config\Config;
use UnicaenDbImport\Config\ConfigException;

class SourceListenerFactory
{
    /**
     * @param ContainerInterface $container
     * @return SourceListener
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     * @throws \UnicaenDbImport\Config\ConfigException
     */
    public function __invoke(ContainerInterface $container): SourceListener
    {
        //
        // ATTENTION aux boucles de dépendance (ex: SourceListener => SourceService => EntityManager => SourceListener),
        // donc maheureusement mieux vaut n'injecter aucun service dans un listener Doctrine.
        //

        /** @var array $config */
        $config = $container->get('Config')['import'];
        if (!isset($config[$key = Config::KEY_source_entity_class])) {
            throw ConfigException::missingKey($key);
        }
        if (!isset($config[$key = Config::KEY_default_source_code])) {
            throw ConfigException::missingKey($key);
        }

        $listener = new SourceListener();
        $listener->setSourceEntityClass($config[Config::KEY_source_entity_class]);
        $listener->setDefaultSourceCode($config[Config::KEY_default_source_code]);

        return $listener;
    }
}
