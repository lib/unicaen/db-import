<?php

namespace UnicaenDbImport\ORM\Event\Listeners;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Event\PrePersistEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Doctrine\ORM\Events;
use Doctrine\Persistence\Event\LifecycleEventArgs;
use RuntimeException;
use UnicaenDbImport\Entity\Db\Interfaces\SourceAwareInterface;
use UnicaenDbImport\Entity\Db\Interfaces\SourceInterface;
use UnicaenDbImport\Entity\Db\Source;

/**
 * Listener Doctrine chargé d'injecter si besoin une Source par défaut dans les entités
 * implémentant {@see SourceAwareInterface}.
 *
 * Déclenchement : avant que l'entité ne soit persistée (création) ou mis à jour (update).
 *
 * @author Bertrand GAUTHIER <bertrand.gauthier at unicaen.fr>
 * @see SourceAwareInterface
 */
class SourceListener implements EventSubscriber
{
    protected ?string $sourceEntityClass = null;

    public function setSourceEntityClass(string $sourceEntityClass): self
    {
        $this->sourceEntityClass = $sourceEntityClass;
        return $this;
    }

    protected ?string $defaultSourceCode = null;

    public function setDefaultSourceCode(string $defaultSourceCode): self
    {
        $this->defaultSourceCode = $defaultSourceCode;
        return $this;
    }

    /**
     * @throws \Doctrine\ORM\Exception\NotSupported
     */
    protected function updateSource(LifecycleEventArgs $args)
    {
        $entity = $args->getObject();

        if (! $entity instanceof SourceAwareInterface) {
            return;
        }
        if ($entity->getSource() !== null) {
            return;
        }

        $defaultSource = $this->fetchDefaultSource($args->getObjectManager());

        $entity->setSource($defaultSource);
    }

    /**
     * @throws \Doctrine\ORM\Exception\NotSupported
     */
    private function fetchDefaultSource(EntityManager $entityManager): SourceInterface
    {
        if ($this->sourceEntityClass === null) {
            throw new RuntimeException("La classe d'entité Source n'a pas été spécifiée");
        }
        if ($this->defaultSourceCode === null) {
            throw new RuntimeException("Aucun code n'a été spécifié pour la Source par défaut");
        }

        /** @var Source $defaultSource */
        $defaultSource = $entityManager->getRepository($this->sourceEntityClass)->findOneBy(['code' => $this->defaultSourceCode]);
        if ($defaultSource === null) {
            throw new RuntimeException("Source par défaut introuvable avec ce code : " . $this->defaultSourceCode);
        }

        return $defaultSource;
    }

    /**
     * @throws \Doctrine\ORM\Exception\NotSupported
     */
    public function prePersist(PrePersistEventArgs $args)
    {
        $this->updateSource($args);
    }

    /**
     * @throws \Doctrine\ORM\Exception\NotSupported
     */
    public function preUpdate(PreUpdateEventArgs $args)
    {
        $this->updateSource($args);
    }

    public function getSubscribedEvents(): array
    {
        return [Events::prePersist, Events::preUpdate];
    }
}