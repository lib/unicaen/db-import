<?php

namespace UnicaenDbImport\Platforms;

class OraclePlatform extends \Doctrine\DBAL\Platforms\OraclePlatform
{
    /**
     * @inheritDoc
     */
    public function getNowExpression($type = 'timestamp'): string
    {
        return 'SYSDATE';
    }

    /**
     * @inheritDoc
     */
    public function getDateTimeTypeDeclarationSQL(array $fieldDeclaration): string
    {
        return 'DATE';
    }

    /**
     * @inheritDoc
     */
    public function getBigIntTypeDeclarationSQL(array $field): string
    {
        return 'NUMBER';
    }
}