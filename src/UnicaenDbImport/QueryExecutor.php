<?php

namespace UnicaenDbImport;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Result;

/**
 * @codeCoverageIgnore
 *
 * @author Unicaen
 */
class QueryExecutor
{
    /**
     * @param string $sql
     * @param Connection $connection
     * @return Result
     * @throws \Doctrine\DBAL\Exception
     */
    private function executeQuery(string $sql, Connection $connection): Result
    {
        return $connection->executeQuery($sql);
    }

    /**
     * @param string $sql
     * @param Connection $connection
     * @return array
     * @throws \Doctrine\DBAL\Exception
     */
    public function fetchAllAsGenerator(string $sql, Connection $connection)
    {
        $statement = $this->executeQuery($sql, $connection);

        $rows = [];
        while ($row = $statement->fetchAssociative()) {
            $rows[] = $row;
            yield $row;
        }

        return $rows;
    }

    /**
     * @param string $sql
     * @param Connection $connection
     * @return array
     * @throws \Doctrine\DBAL\Exception
     */
    public function fetchAll(string $sql, Connection $connection): array
    {
        $statement = $this->executeQuery($sql, $connection);

        return $statement->fetchAllAssociative();
    }

    /**
     * @param string $sql
     * @param Connection $connection
     * @return array|null
     * @throws \Doctrine\DBAL\Exception
     */
    public function fetch(string $sql, Connection $connection): ?array
    {
        $statement = $this->executeQuery($sql, $connection);

        return $statement->fetchAssociative() ?: null;
    }

    /**
     * @param string $sql
     * @param Connection $connection
     * @return int
     * @throws \Doctrine\DBAL\Exception
     */
    public function exec(string $sql, Connection $connection): int
    {
        return $connection->executeStatement($sql);
    }
}