<?php

namespace UnicaenDbImport\CodeGenerator;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use RuntimeException;
use UnicaenDbImport\CodeGenerator\Helper\LogTableHelper;
use UnicaenDbImport\CodeGenerator\Helper\TableHelper;
use UnicaenDbImport\CodeGenerator\Helper\TableValidationHelper;
use UnicaenDbImport\Config\ConfigAwareTrait;
use UnicaenDbImport\Domain\DestinationInterface;
use UnicaenDbImport\Domain\Operation;
use UnicaenDbImport\Domain\ResultInterface;
use UnicaenDbImport\Domain\SourceInterface;

/**
 * Classe mère des générateurs de code SQL, quelque soit la plateforme de base de données.
 *
 * @author Unicaen
 */
abstract class CodeGenerator implements CodeGeneratorInterface
{
    use ConfigAwareTrait;

    /**
     * @var AbstractPlatform
     */
    protected $platform;

    /**
     * @var TableHelper
     */
    protected $tableHelper;

    /**
     * @var TableValidationHelper
     */
    protected $tableValidationHelper;

    /**
     * @var LogTableHelper
     */
    protected $logTableHelper;

    /**
     * @param TableHelper $tableHelper
     * @return CodeGenerator
     */
    public function setTableHelper(TableHelper $tableHelper): CodeGenerator
    {
        $this->tableHelper = $tableHelper;
        return $this;
    }

    /**
     * @param TableValidationHelper $tableValidationHelper
     * @return CodeGenerator
     */
    public function setTableValidationHelper(TableValidationHelper $tableValidationHelper): CodeGenerator
    {
        $this->tableValidationHelper = $tableValidationHelper;
        return $this;
    }

    /**
     * @param LogTableHelper $logTableHelper
     * @return CodeGenerator
     */
    public function setLogTableHelper(LogTableHelper $logTableHelper): CodeGenerator
    {
        $this->logTableHelper = $logTableHelper;
        return $this;
    }

    /**
     * @param string $tableName
     * @return string
     * @codeCoverageIgnore Car simple délégation
     */
    public function generateSQLForTableExistenceCheck(string $tableName): string
    {
        return $this->tableValidationHelper->generateSQLForTableExistenceCheck($tableName);
    }

    /**
     * @param array $result
     * @return bool
     * @codeCoverageIgnore Car simple délégation
     */
    public function convertTableExistenceCheckResultToBoolean(array $result): bool
    {
        try {
            return $this->tableValidationHelper->convertTableExistenceCheckResultToBoolean($result);
        } catch (Helper\HelperException $e) {
            throw new RuntimeException("Erreur rencontrée !", null, $e);
        }
    }

    /**
     * @param string $tableName
     * @param string|null $sequenceName
     * @return string
     * @codeCoverageIgnore Car simple délégation
     */
    public function generateSQLForSequenceExistenceCheck(string $tableName, string &$sequenceName = null): string
    {
        if ($sequenceName === null) {
            // si aucun nom de séquence n'a été fourni, on tente celui par défaut.
            $sequenceName = $this->tableHelper->generateSQLForIdSequenceDefaultName($tableName);
        }

        return $this->tableValidationHelper->generateSQLForSequenceExistenceCheck($sequenceName);
    }

    /**
     * @param string $sequenceName
     * @param array $result
     * @return RuntimeException|null
     * @codeCoverageIgnore Car simple délégation
     */
    public function convertSequenceExistenceCheckResultToException(string $sequenceName, array $result): ?RuntimeException
    {
        return $this->tableValidationHelper->convertSequenceExistenceCheckResultToException($sequenceName, $result);
    }

    /**
     * @param string $tableName
     * @param int $columnsCount
     * @return string
     * @codeCoverageIgnore Car simple délégation
     */
    public function generateSQLForIdColumnValidationInTable(string $tableName, int &$columnsCount): string
    {
        return $this->tableValidationHelper->generateSQLForIdColumnValidation($tableName, $columnsCount);
    }

    /**
     * @param string $tableName
     * @param int $columnsCount
     * @return string
     * @codeCoverageIgnore Car simple délégation
     */
    public function generateSQLForHistoColumnsValidationInTable(string $tableName, int &$columnsCount): string
    {
        return $this->tableValidationHelper->generateSQLForHistoColumnsValidation($tableName, $columnsCount);
    }

    /**
     * @param string $tableName
     * @param array $result
     * @param int $columnCount
     * @return null|RuntimeException
     * @codeCoverageIgnore Car simple délégation
     */
    public function convertIdColumnValidationBadResultToException(string $tableName, array $result, int $columnCount): ?RuntimeException
    {
        return $this->tableValidationHelper->convertIdColumnValidationBadResultToException($tableName, $result, $columnCount);
    }

    /**
     * @param string $tableName
     * @param array $result
     * @param int $columnCount
     * @return null|RuntimeException
     * @codeCoverageIgnore Car simple délégation
     */
    public function convertHistoColumnsValidationBadResultToException(string $tableName, array $result, int $columnCount): ?RuntimeException
    {
        try {
            return $this->tableValidationHelper->convertHistoColumnsValidationBadResultToException($tableName, $result, $columnCount);
        } catch (Helper\HelperException $e) {
            throw new RuntimeException("Erreur rencontrée !", null, $e);
        }
    }

    /**
     * @param string $tableName
     * @param string $sourceCodeColumn
     * @param int $columCount
     * @return string
     * @codeCoverageIgnore Car simple délégation
     */
    public function generateSQLForSourceColumnsValidationInTable(string $tableName, string $sourceCodeColumn, int &$columCount): string
    {
        return $this->tableValidationHelper->generateSQLForSourceColumnsValidation($tableName, $sourceCodeColumn, $columCount);
    }

    /**
     * @param string $tableName
     * @param string $sourceCodeColumn
     * @param array $result
     * @param int $columCount
     * @return null|RuntimeException
     * @codeCoverageIgnore Car simple délégation
     */
    public function convertSourceColumnsValidationBadResultToException(string $tableName, string $sourceCodeColumn, array $result, int $columCount): ?RuntimeException
    {
        return $this->tableValidationHelper->convertSourceColumnsValidationBadResultToException($tableName, $sourceCodeColumn, $result, $columCount);
    }

    /**
     * @param string $columnName
     * @param string $columnValue
     * @param string $tableName
     * @return string
     */
    public function generateSQLForValueExistenceCheckInTable(string $columnName, string $columnValue, string $tableName): string
    {
        return $this->tableHelper->generateSQLForSelectFromTable($tableName) .
            " WHERE $columnName = '$columnValue'";
    }

    /**
     * @param array $result
     * @return bool
     * @codeCoverageIgnore Car simple délégation
     */
    public function convertValueExistenceCheckInTableResultToBoolean(array $result): bool
    {
        return $this->tableValidationHelper->convertValueExistenceCheckInTableResultToBoolean($result);
    }

    /**
     * @param SourceInterface $source
     * @return string
     */
    public function generateSQLForSelectFromSource(SourceInterface $source): string
    {
        if ($source->getSelect()) {
            $query = $source->getSelect();
        } else {
            $sourceTable = $source->getTable();
            $query = $this->tableHelper->generateSQLForSelectFromTable($sourceTable);
        }

        if ($where = $source->getWhere()) {
            $query = sprintf('SELECT * FROM (%s) tmp WHERE (%s)', $query, $where);
        }

        return $query;
    }

    /**
     * @param SourceInterface $source
     * @param DestinationInterface $destination
     * @param string $intermediateTable
     * @return string
     */
    public function generateSQLForIntermediateTableCreation(SourceInterface $source, DestinationInterface $destination, string $intermediateTable): string
    {
        $destinationTable = $destination->getTable();
        $sourceCodeColumn = $source->getSourceCodeColumn();
        $columns = $source->getColumns();

        return $this->tableHelper->generateSQLForIntermediateTableCreation($destinationTable, $sourceCodeColumn, $columns, $intermediateTable);
    }

    /**
     * @param string $tableName
     * @param array $row
     * @param string|null $sourceCode
     * @param string|null $idColumnStrategy
     * @param string|null $idColumnSequence
     * @return string
     */
    public function generateSQLForInsertOneRowIntoTable(string $tableName,
                                                  array  $row,
                                                  string $sourceCode = null,
                                                  ?string $idColumnStrategy = null,
                                                  ?string $idColumnSequence = null): string
    {
        try {
            return $this->tableHelper->generateSQLForInsertOneRowIntoTable($tableName, $row, $sourceCode, $idColumnStrategy, $idColumnSequence);
        } catch (Helper\HelperException $e) {
            throw new RuntimeException("Erreur rencontrée lors de la génération SQL", null, $e);
        }
    }

    /**
     * @param string $tableName
     * @return string
     */
    public function generateSQLForClearTable(string $tableName): string
    {
        return 'DELETE FROM ' . $tableName;
    }

    /**
     * @param string $logTableName
     * @return string
     * @codeCoverageIgnore
     */
    public function generateSQLForImportLogTableCreation(string $logTableName): string
    {
        try {
            return $this->logTableHelper->generateSQLForLogTableCreation($logTableName);
        } catch (Helper\HelperException $e) {
            throw new RuntimeException("Erreur rencontrée !", null, $e);
        }
    }

    /**
     * @param string $tableName
     * @return string
     */
    public function generateSQLForIntermmediateTableDrop(string $tableName): string
    {
        throw CodeGeneratorException::methodToImplement(__METHOD__, get_called_class(), $this->platform);
    }

    public function generateDiffViewName(string $destinationTable): string
    {
        return $this->tableHelper->generateDiffViewName($destinationTable);
    }

    /**
     * @param SourceInterface $source
     * @param DestinationInterface $destination
     * @param string $diffViewName
     * @param string|null $intermediateTable
     * @return string
     */
    public function generateSQLForDiffViewCreation(SourceInterface $source,
                                                   DestinationInterface $destination,
                                                   string $diffViewName,
                                                   string $intermediateTable = null): string
    {
        $destinationTable = $destination->getTable();
        $sourceCodeColumn = $source->getSourceCodeColumn();
        $columns = $source->getColumns();
        $includeIdColumn = $destination->getIdColumnStrategy() === DestinationInterface::ID_STRATEGY_IDENTITY;
        $undeleteEnabledColumn = $destination->getUndeleteEnabledColumn();
        $updateOnDeletedEnabledColumn = $destination->getUpdateOnDeletedEnabledColumn();

        // les éventuelles colonnes d'historique dans les données sources seront écrasées :
        $columns = array_diff($columns, [
            $c = $this->config->getHistoColumnAliasForCreatedOn(), strtoupper($c),
            $c = $this->config->getHistoColumnAliasForCreatedBy(), strtoupper($c),
            $c = $this->config->getHistoColumnAliasForUpdatedOn(), strtoupper($c),
            $c = $this->config->getHistoColumnAliasForUpdatedBy(), strtoupper($c),
            $c = $this->config->getHistoColumnAliasForDeletedOn(), strtoupper($c),
            $c = $this->config->getHistoColumnAliasForDeletedBy(), strtoupper($c),
        ]);

        $diffSourceTableName = $intermediateTable ?: $source->getTable();

        return $this->tableHelper->generateSQLForDiffViewCreationOrUpdate(
            $diffViewName,
            $diffSourceTableName,
            $destinationTable,
            $sourceCodeColumn,
            $columns,
            $includeIdColumn,
            $undeleteEnabledColumn,
            $updateOnDeletedEnabledColumn);
    }

    /**
     * @param SourceInterface $source
     * @param DestinationInterface $destination
     * @return string
     */
    public function generateSQLForDiffViewSelect(SourceInterface $source, DestinationInterface $destination): string
    {
        $destinationTable = $destination->getTable();
        $sourceCodeColumn = $source->getSourceCodeColumn();
        $columns = $source->getColumns();
        $where = $destination->getWhere();
        $includeIdColumn = $destination->getIdColumnStrategy() === DestinationInterface::ID_STRATEGY_IDENTITY;

        // les éventuelles colonnes d'historique dans les données sources seront écrasées :
        $columns = array_diff($columns, [
            $c = $this->config->getHistoColumnAliasForCreatedOn(), strtoupper($c),
            $c = $this->config->getHistoColumnAliasForCreatedBy(), strtoupper($c),
            $c = $this->config->getHistoColumnAliasForUpdatedOn(), strtoupper($c),
            $c = $this->config->getHistoColumnAliasForUpdatedBy(), strtoupper($c),
            $c = $this->config->getHistoColumnAliasForDeletedOn(), strtoupper($c),
            $c = $this->config->getHistoColumnAliasForDeletedBy(), strtoupper($c),
        ]);

        return $this->tableHelper->generateSQLForDiffViewSelect($destinationTable, $sourceCodeColumn, $columns, $where, $includeIdColumn);
    }

    /**
     * @inheritDoc
     */
    public function generateSQLForDestinationUpdate(
        string               $operation,
        SourceInterface      $source,
        DestinationInterface $destination): string
    {
        $destinationTable = $destination->getTable();
        $idColumnStrategy = $destination->getIdColumnStrategy();
        $idColumnSequence = $destination->getIdColumnSequence();
        $where = $destination->getWhere();
        $sourceCodeColumn = $source->getSourceCodeColumn();
        $columns = $source->getColumns();

        // les éventuelles colonnes d'historique dans les données sources seront écrasées :
        $columns = array_diff($columns, [
            $c = $this->config->getHistoColumnAliasForCreatedOn(), strtoupper($c),
            $c = $this->config->getHistoColumnAliasForCreatedBy(), strtoupper($c),
            $c = $this->config->getHistoColumnAliasForUpdatedOn(), strtoupper($c),
            $c = $this->config->getHistoColumnAliasForUpdatedBy(), strtoupper($c),
            $c = $this->config->getHistoColumnAliasForDeletedOn(), strtoupper($c),
            $c = $this->config->getHistoColumnAliasForDeletedBy(), strtoupper($c),
        ]);

        switch ($operation) {
            case Operation::OPERATION_INSERT;
                $sql = $this->tableHelper->generateSQLForInsertOperationInDestinationTable(
                        $destinationTable,
                        $sourceCodeColumn,
                        $columns,
                        $where,
                        $idColumnStrategy,
                        $idColumnSequence);
                break;
            case Operation::OPERATION_UPDATE;
                $sql = $this->tableHelper->generateSQLForUpdateOperationInDestinationTable(
                        $destinationTable,
                        $sourceCodeColumn,
                        $columns,
                        $where);
                break;
            case Operation::OPERATION_DELETE;
                $sql = $this->tableHelper->generateSQLForDeleteOperationInDestinationTable(
                        $destinationTable,
                        $sourceCodeColumn,
                        $columns,
                        $where);
                break;
            case Operation::OPERATION_UNDELETE;
                $sql = $this->tableHelper->generateSQLForUndeleteOperationInDestinationTable(
                        $destinationTable,
                        $sourceCodeColumn,
                        $columns,
                        $where);
                break;
            default:
                throw new RuntimeException("Opération inattendue");
        }

        return $sql . PHP_EOL;
    }

    /**
     * @param ResultInterface $result
     * @param string $importLogTable
     * @return string
     */
    public function generateSQLForInsertResultIntoLogTable(ResultInterface $result, string $importLogTable): string
    {
        try {
            return $this->logTableHelper->generateSQLForInsertResultIntoLogTable($result, $importLogTable);
        } catch (Helper\HelperException $e) {
            throw new RuntimeException("Erreur rencontrée !", null, $e);
        }

    }

    /**
     * @param string $table
     * @return string
     */
    public function generateSQLForSelectingInImportObservTable(string $table): string
    {
        $requiredValueForEnabled = $this->tableHelper->platformSupportsBooleanType() ? 'true' : 1;

        return <<<EOS
select
    id,
    code,
    operation,
    description,
    table_name,
    column_name,
    to_value,
    filter
from import_observ io 
where io.operation = 'UPDATE' and io.enabled = $requiredValueForEnabled
and upper(table_name) = upper('$table')
order by operation, table_name, column_name
EOS;
    }

    /**
     * @param array $importObservRow
     * @param string|null $where
     * @return string
     */
    protected function generateSQLForSelectingDiffViewFromImportObservRow(array $importObservRow, string $where = null): string
    {
        $id = $importObservRow['id'] ?? $importObservRow['ID'];
        $tableName = $importObservRow['table_name'] ?? $importObservRow['TABLE_NAME'];
        $columnName = $importObservRow['column_name'] ?? $importObservRow['COLUMN_NAME'];
        $toValue = $importObservRow['to_value'] ?? $importObservRow['TO_VALUE'];
        $filter = $importObservRow['filter'] ?? $importObservRow['FILTER'] ?? null;

        // Construction du nom de la colonne de la vue V_DIFF_X indiquant un changement de valeur dans la table X.
        // Ex: 'U_RESULTAT' (dans la vue V_DIFF_THESE, indiquant que la colonne THESE.RESULTAT a changé).
        $uColName = 'U_' . $columnName;

        // Construction du nom de la colonne de la vue V_DIFF_X contenant la nouvelle valeur de la colonne.
        // Ex: 'S_RESULTAT' (dans la vue V_DIFF_THESE, contenant la nouvelle valeur de la colonne THESE.RESULTAT).
        $sColName = 'S_' . $columnName;

        // Construction de la clause permettant de ne prendre en compte que la prise de valeur qui nous intéresse.
        // Ex: "v.COLONNE = 'VALEUR'" ou "v.COLONNE IS NULL".
        $toValueCond = 'v.' . $sColName . ($toValue === null ? ' is null' : (" = '" . $toValue . "'"));

        // Eventuels filtres : celui de la table IMPORT_OBSERV + celui spécifié dans la config de la destination.
        $wheres = [];
        if ($filter) $wheres[] = sprintf('(%s)', $filter);
        if ($where) $wheres[] = sprintf('(%s)', str_ireplace('d.', 't.', $where));
        $andWhere = $wheres ? 'and ' . implode(' and ', $wheres) : '';

        // Colonne 'detail'. Ex: "coalesce(t.RESULTAT::varchar, '') || '>' || coalesce(v.RESULTAT::varchar, '')"
        $detail =
            'coalesce(' . $this->tableHelper->generateSQLForCastToString("t.$columnName") . ", '')" .
            " || '>' || " .
            'coalesce(' . $this->tableHelper->generateSQLForCastToString("v.$sColName") . ", '')";

        // Construction de la requête recherchant dans la vue V_DIFF_X les lignes correspondant à :
        // une prise de valeur particulière spécifiée par IMPORT_OBSERV.TO_VALUE,
        // de la colonne spécifiée par IMPORT_OBSERV.COLUMN_NAME,
        // dans la table spécifiée par IMPORT_OBSERV.TABLE_NAME.
        return <<<EOS
select $id import_observ_id, v.source_id, v.source_code, 
$detail detail 
from V_DIFF_$tableName v 
join $tableName t on t.source_code = v.source_code 
where $uColName = 1 and $toValueCond
$andWhere
EOS;
    }

    /**
     * @param DestinationInterface $destination
     * @param array $importObservRows
     * @return string
     */
    public function generateSQLForInsertionIntoImportObservResult(DestinationInterface $destination, array $importObservRows): string
    {
        $where = $destination->getWhere();
        $now = $this->platform->getNowExpression();

        $selects = [];
        foreach ($importObservRows as $importObservRow) {
            $selects[] = $this->generateSQLForSelectingDiffViewFromImportObservRow($importObservRow, $where);
        }
        $selects = implode(PHP_EOL . 'UNION ALL' . PHP_EOL, $selects);

        $nextval = $this->tableHelper->generateSQLForSequenceNextVal('import_observ_result_id_seq');

        return <<<EOS
insert into import_observ_result (id, date_creation, import_observ_id, source_id, source_code, resultat) 
with tmp as (
    $selects
)
select $nextval, $now, import_observ_id, source_id, source_code, detail 
from tmp
EOS;
    }
}