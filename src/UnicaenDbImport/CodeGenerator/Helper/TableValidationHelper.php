<?php

namespace UnicaenDbImport\CodeGenerator\Helper;

use Doctrine\DBAL\Exception;
use RuntimeException;
use UnicaenDbImport\CodeGenerator\Helper;

/**
 * Génération de code SQL pour tester l'existence, la conformité de table.
 *
 * Version commune à toutes les plateformes de bases de données.
 *
 * @author Unicaen
 */
abstract class TableValidationHelper extends Helper
{
    /**
     * @param string $tableName
     * @return string
     */
    abstract public function generateSQLForTableExistenceCheck(string $tableName): string;

    /**
     * @param string $sequenceName
     * @return string
     */
    abstract public function generateSQLForSequenceExistenceCheck(string $sequenceName): string;

    /**
     * @param array $result
     * @return bool
     * @throws \UnicaenDbImport\CodeGenerator\Helper\HelperException
     */
    public function convertTableExistenceCheckResultToBoolean(array $result): bool
    {
        if (! isset($result[0]) && !isset($result['table_exists']) && !isset($result['TABLE_EXISTS'])) {
            throw new HelperException("Données fournies non exploitables");
        }

        $value = false;
        if (isset($result[0])) {
            $value = array_pop($result[0]); // NB: on ignore le nom de la colonne car la casse est incertaine
        }
        if (isset($result['table_exists']) || isset($result['TABLE_EXISTS'])) {
            $value = $result['table_exists'] ?? $result['TABLE_EXISTS'];
        }

        return intval($value) === 1;
    }

    /**
     * @param array $result
     * @return bool
     */
    public function convertValueExistenceCheckInTableResultToBoolean(array $result): bool
    {
        return count($result) > 0;
    }

    /**
     * @param string $sequenceName
     * @param array $result
     * @return null|RuntimeException
     */
    public function convertSequenceExistenceCheckResultToException(string $sequenceName, array $result): ?RuntimeException
    {
        $value = array_pop($result[0]); // NB: on ignore le nom de la colonne car la casse est incertaine

        if (intval($value) === 1) {
            return null;
        }

        $message = "Assurez-vous que la sequence '$sequenceName' existe : " . PHP_EOL;
        $message .= <<<EOT
CREATE SEQUENCE $sequenceName ;
EOT;

        return new RuntimeException($message);
    }

    /**
     * @param string $tableName
     * @param array  $columnsAndTypes
     * @return string
     */
    abstract public function generateSQLForColumnsValidation($tableName, array $columnsAndTypes): string;

    /**
     * @param string $tableName
     * @param array  $result
     * @param array  $expectedColumns
     * @return null|RuntimeException
     */
    public function convertColumnsValidationResultToException($tableName, array $result, array $expectedColumns): ?RuntimeException
    {
        if (count($result) === count($expectedColumns)) {
            return null;
        }

        $message = "Assurez-vous que les colonnes suivantes existent bien dans la table '$tableName' : ";
        $message .= implode(', ', $expectedColumns);

        return new RuntimeException($message);
    }

    /**
     * @param string $tableName
     * @param int $columnsCount
     * @return string
     */
    abstract public function generateSQLForIdColumnValidation(string $tableName, int &$columnsCount): string;

    /**
     * @param string $tableName
     * @param int $columnsCount
     * @return string
     */
    abstract public function generateSQLForHistoColumnsValidation(string $tableName, int &$columnsCount): string;

    /**
     * @param string $tableName
     * @param string $sourceCodeColumn
     * @param int $columnCount
     * @return string
     */
    abstract public function generateSQLForSourceColumnsValidation(string $tableName, string $sourceCodeColumn, int &$columnCount): string;

    /**
     * @param string $tableName
     * @param array $result
     * @param int $columnCount
     * @return null|RuntimeException
     */
    public function convertIdColumnValidationBadResultToException(string $tableName, array $result, int $columnCount): ?RuntimeException
    {
        if (count($result) === $columnCount) {
            return null;
        }

        $integer = $this->platform->getBigIntTypeDeclarationSQL([]);

        $message = "Assurez-vous que la colonne id existe dans la table '$tableName' : " . PHP_EOL;
        $message .= <<<EOT
ALTER TABLE $tableName ADD id $integer NOT NULL ;
EOT;

        return new RuntimeException($message);
    }

    /**
     * @param string $tableName
     * @param array $result
     * @param int $columnCount
     * @return null|RuntimeException
     * @throws \UnicaenDbImport\CodeGenerator\Helper\HelperException
     */
    public function convertHistoColumnsValidationBadResultToException(string $tableName, array $result, int $columnCount): ?RuntimeException
    {
        if (count($result) === $columnCount) {
            return null;
        }

        try {
            $timestamp = $this->platform->getDateTimeTypeDeclarationSQL([]);
        } catch (Exception $e) {
            throw new HelperException("Type non supporté par la plateforme de base de données !", null, $e);
        }
        $integer = $this->platform->getBigIntTypeDeclarationSQL([]);
        $now = $this->platform->getNowExpression();

        $createdOnCol = $this->config->getHistoColumnAliasForCreatedOn();
        $updatedOnCol = $this->config->getHistoColumnAliasForUpdatedOn();
        $deletedOnCol = $this->config->getHistoColumnAliasForDeletedOn();
        $createdByCol = $this->config->getHistoColumnAliasForCreatedBy();
        $updatedByCol = $this->config->getHistoColumnAliasForUpdatedBy();
        $deletedByCol = $this->config->getHistoColumnAliasForDeletedBy();

        $message = "Assurez-vous que les colonnes d'historique suivantes sont présentes dans la table '$tableName' : " . PHP_EOL;
        $message .= <<<EOT
ALTER TABLE $tableName ADD $createdOnCol $timestamp DEFAULT $now NOT NULL ;
ALTER TABLE $tableName ADD $updatedOnCol $timestamp ;
ALTER TABLE $tableName ADD $deletedOnCol $timestamp ;
ALTER TABLE $tableName ADD $createdByCol $integer NOT NULL ;
ALTER TABLE $tableName ADD $updatedByCol $integer ;
ALTER TABLE $tableName ADD $deletedByCol $integer ;
EOT;

        return new RuntimeException($message);
    }

    /**
     * @param string $tableName
     * @param string $sourceCodeColumn
     * @param array $result
     * @param int $columCount
     * @return null|RuntimeException
     */
    public function convertSourceColumnsValidationBadResultToException(string $tableName, string $sourceCodeColumn, array $result, int $columCount): ?RuntimeException
    {
        if (count($result) === $columCount) {
            return null;
        }

        $integer = $this->platform->getBigIntTypeDeclarationSQL([]);
        $varchar = $this->platform->getStringTypeDeclarationSQL(['length' => 100]);

        $message = "Assurez-vous que les colonnes suivantes sont présentes dans la table '$tableName' : " . PHP_EOL;
        $message .= <<<EOT
ALTER TABLE $tableName ADD source_id $integer NOT NULL ;
ALTER TABLE $tableName ADD $sourceCodeColumn $varchar ;
EOT;

        return new RuntimeException($message);
    }
}