<?php

namespace UnicaenDbImport\CodeGenerator\Helper;

use DateTime;
use DateTimeZone;
use stdClass;
use RuntimeException;
use UnicaenDbImport\CodeGenerator\Helper;
use UnicaenDbImport\Domain\DestinationInterface;
use UnicaenDbImport\Domain\Operation;
use UnicaenDbImport\Domain\Synchro;

/**
 * Génération de code SQL autour des tables.
 *
 * Version commune à toutes les plateformes de bases de données.
 *
 * @author Unicaen
 */
abstract class TableHelper extends Helper
{
    /**
     * @param string $tableName
     * @return string
     */
    public function generateSQLForSelectFromTable(string $tableName): string
    {
        return "SELECT * FROM " . $tableName;
    }

    /**
     * @param string $destinationTable
     * @param string $sourceCodeColumn
     * @param string[] $columns
     * @param string $intermediateTable
     * @return string
     */
    public function generateSQLForIntermediateTableCreation(string $destinationTable, string $sourceCodeColumn, array $columns, string $intermediateTable): string
    {
        $createdByCol = $this->config->getHistoColumnAliasForCreatedBy();
        $createdOnCol = $this->config->getHistoColumnAliasForCreatedOn();

        $columns = array_merge(
            ['id', 'source_id', $sourceCodeColumn],
            [$createdByCol, $createdOnCol],
            $columns
        );
        $commaSeparatedColumnNames = implode(', ', $columns);

        return <<<EOT
create table $intermediateTable as select $commaSeparatedColumnNames from $destinationTable;
EOT;
    }

    /**
     * @param string $tableName
     * @return string
     */
    public function generateSQLForTruncateTable(string $tableName): string
    {
        return $this->platform->getTruncateTableSQL($tableName);
    }

    /**
     * @param string $diffViewName
     * @param string $diffSourceTableName
     * @param string $destinationTable
     * @param string $sourceCodeColumn
     * @param array $columns
     * @param bool $includeIdColumn
     * @param ?string $undeleteEnabledColumn
     * @param ?string $updateOnDeletedEnabledColumn
     * @return string
     */
    public function generateSQLForDiffViewCreationOrUpdate(string $diffViewName,
                                                           string $diffSourceTableName,
                                                           string $destinationTable,
                                                           string $sourceCodeColumn,
                                                           array  $columns,
                                                           bool   $includeIdColumn,
                                                           ?string $undeleteEnabledColumn,
                                                           ?string $updateOnDeletedEnabledColumn): string
    {
        $res = '';
        if ($sql = $this->generateSQLForViewDropBeforeUpdate($diffViewName)) {
            $res = $sql . ';' . PHP_EOL . PHP_EOL;
        }

        $res .= "CREATE OR REPLACE VIEW $diffViewName AS " . PHP_EOL;
        $res .= $this->indent(4, $this->generateDiffViewBodySelectSQLSnippet(
                $diffSourceTableName,
                $destinationTable,
                $sourceCodeColumn,
                $columns,
                $includeIdColumn,
                $undeleteEnabledColumn,
                $updateOnDeletedEnabledColumn)) . PHP_EOL;

        return $res;
    }

    abstract protected function generateSQLForViewDropBeforeUpdate($name): ?string;

    /**
     * @param string $destinationTable
     * @param string $sourceCodeColumn
     * @param array $columns
     * @param string|null $where
     * @param bool $includeIdColumn
     * @return string
     */
    public function generateSQLForDiffViewSelect(
        string $destinationTable,
        string $sourceCodeColumn,
        array $columns,
        ?string $where,
        bool $includeIdColumn): string
    {
        if (!$includeIdColumn) {
            $columns = array_diff($columns, ['ID', 'id']);
        }

        $columns = array_diff($columns, ['SOURCE_ID', 'source_id']);

        $name = $this->generateDiffViewName($destinationTable);

        $updatedColumsSql = implode(
            ',',
            array_map(function($col) {
                return sprintf('U_%s', $col);
            }, $columns)
        );

        $srcColumsSql = implode(
            ',' . PHP_EOL,
            array_map(function($col) {
                return sprintf('S_%s', $col);
            }, $columns)
        );

        $destColumsSql = implode(
            ',' . PHP_EOL,
            array_map(function($col) {
                return sprintf('D_%s', $col);
            }, $columns)
        );

        $wheres = null;
        if ($where) {
            $wheres = 'WHERE ' . $where;
        }

        return <<<EOT
SELECT 
    $sourceCodeColumn, 
    operation, 
    $updatedColumsSql, 
    $srcColumsSql, 
    $destColumsSql 
FROM $name d
$wheres
EOT;
    }

    /**
     * @param string $destinationTable
     * @return string
     */
    public function generateDiffViewName(string $destinationTable): string
    {
        return 'V_DIFF_' . $destinationTable;
    }

    /**
     * @param string $destinationTable
     * @return string
     * @throws \Doctrine\DBAL\Exception
     */
    protected function generateDiffViewDeletionSQLSnippet(string $destinationTable): string
    {
        $name = $this->generateDiffViewName($destinationTable);

        return $this->platform->getDropViewSQL($name);
    }

    /**
     * @param string $diffSourceTableName
     * @param string $destinationTable
     * @param string $sourceCodeColumn
     * @param array $columns
     * @param bool $includeIdColumn
     * @param ?string $undeleteEnabledColumn
     * @param ?string $updateOnDeletedEnabledColumn
     * @return string
     */
    protected function generateDiffViewBodySelectSQLSnippet(string $diffSourceTableName,
                                                            string $destinationTable,
                                                            string $sourceCodeColumn,
                                                            array $columns,
                                                            bool $includeIdColumn,
                                                            ?string $undeleteEnabledColumn,
                                                            ?string $updateOnDeletedEnabledColumn): string
    {
        if (!$includeIdColumn) {
            $columns = array_diff($columns, ['ID', 'id']);
        }

        $insert = Operation::OPERATION_INSERT;
        $update = Operation::OPERATION_UPDATE;
        $undelete = Operation::OPERATION_UNDELETE;
        $delete = Operation::OPERATION_DELETE;

        $updatedColumsSql = implode(
            ',' . PHP_EOL,
            array_map(function($col) {
                $tpl = 'CASE WHEN d.%s <> s.%1$s OR (d.%1$s IS NULL AND s.%1$s IS NOT NULL) OR (d.%1$s IS NOT NULL AND s.%1$s IS NULL) THEN 1 ELSE 0 END as U_%1$s';
                return sprintf($tpl, $col);
            }, $columns)
        );

        $srcColumsSql = implode(
            ',' . PHP_EOL,
            array_map(function($col) {
                return sprintf('s.%1$s AS S_%1$s', $col);
            }, $columns)
        );

        $destColumsSql = implode(
            ',' . PHP_EOL,
            array_map(function($col) {
                return sprintf('d.%1$s AS D_%1$s', $col);
            }, $columns)
        );

        $udpatedColumnsAddition = implode(
            '+',
            array_map(function($col) {
                return sprintf('U_%s', $col);
            }, $columns)
        );

        $now = $this->platform->getNowExpression();
        $true = $this->platformSupportsBooleanType() ? 'true' : 1;

        $deletedOnColumn = $this->config->getHistoColumnAliasForDeletedOn();

        $requiredValueForImportable = $this->platformSupportsBooleanType() ? 'true' : 1;

        // Dans la table SOURCE, des flags indiquent les opérations de synchro autorisées.
        // Exemple : interdire le 'delete' est utile lorsque les données sources sont obtenues de façon incrémentale au fil du temps
        // (et non pas de façon exhaustive en une seule fois).
        $requiredValueForSynchroOperationEnabled = $this->platformSupportsBooleanType() ? 'true' : 1;

        // Faut-il prendre en compte une colonne autorisant ou non l'opération 'undelete' sur les enregistrements.
        $andUndeleteOperationAllowedCondition = '';
        if ($undeleteEnabledColumn) {
            $andUndeleteOperationAllowedCondition = "and d.$undeleteEnabledColumn = $true";
        }

        // Faut-il prendre en compte une colonne autorisant l'update sur les enregistrements historisés ?
        $andDeletedOnCondition = "and (d.$deletedOnColumn IS NULL or d.$deletedOnColumn > $now) ";
        if ($updateOnDeletedEnabledColumn) {
            // une valeur 'true' (çàd update autorisée même si historisé) dans cette colonne désactive la condition d'historisation
            $andDeletedOnCondition = "and (d.$updateOnDeletedEnabledColumn = $true or (d.$deletedOnColumn IS NULL or d.$deletedOnColumn > $now)) ";
        }

        return <<<EOT
with diff as (
    SELECT
        coalesce(s.$sourceCodeColumn, d.$sourceCodeColumn) $sourceCodeColumn,
        coalesce(s.source_id, d.source_id) source_id,

        case         -- NB : l'ordre des when est important ! --
        when src.synchro_insert_enabled = $requiredValueForSynchroOperationEnabled and 
             s.$sourceCodeColumn IS NOT NULL AND d.$sourceCodeColumn IS NULL
             then '$insert'
        when src.synchro_undelete_enabled = $requiredValueForSynchroOperationEnabled and 
             s.$sourceCodeColumn IS NOT NULL AND d.$sourceCodeColumn IS NOT NULL and 
             d.$deletedOnColumn IS NOT NULL and d.$deletedOnColumn <= $now 
             $andUndeleteOperationAllowedCondition
             then '$undelete'
        when src.synchro_update_enabled = $requiredValueForSynchroOperationEnabled and 
             s.$sourceCodeColumn IS NOT NULL AND d.$sourceCodeColumn IS NOT NULL  
             $andDeletedOnCondition
             then '$update'
        when src.synchro_delete_enabled = $requiredValueForSynchroOperationEnabled and 
             s.$sourceCodeColumn IS NULL AND d.$sourceCodeColumn IS NOT NULL and 
             (d.$deletedOnColumn IS NULL or d.$deletedOnColumn > $now)
             then '$delete' 
        end as operation,

        $updatedColumsSql,

        $srcColumsSql,

        $destColumsSql

    FROM $destinationTable d
    FULL OUTER JOIN $diffSourceTableName s ON s.source_id = d.source_id AND s.$sourceCodeColumn = d.$sourceCodeColumn
    JOIN source src ON src.id = coalesce(s.source_id, d.source_id) AND src.importable = $requiredValueForImportable
)
select * from diff
where operation is not null
and (operation = '$undelete' or 0 < $udpatedColumnsAddition)
EOT;
    }

    /**
     * @param string $destinationTable
     * @param array $row ['COLONNE_EN_MAJUSCULE' => value, ...]
     * @param string|null $sourceCode
     * @param string|null $idColumnStrategy
     * @param string|null $idColumnSequence
     * @return string
     * @throws \UnicaenDbImport\CodeGenerator\Helper\HelperException
     */
    public function generateSQLForInsertOneRowIntoTable(
        string $destinationTable,
        array  $row,
        string $sourceCode = null,
        ?string $idColumnStrategy = null,
        ?string $idColumnSequence = null): string
    {
        $now = $this->platform->getNowExpression();

        $row = array_map([$this, 'formatValueForInsert'], $row);

        if ($idColumnStrategy === DestinationInterface::ID_STRATEGY_SEQUENCE) {
            // si aucun nom de séquence n'a été fourni, on utilise celui par défaut.
            $idColumnSequence = $idColumnSequence ?: $this->generateSQLForIdSequenceDefaultName($destinationTable);
            // les éventuels id d'origine sont écrasés
            unset($row['id']);
            unset($row['ID']);
            $idExpr = $this->generateSQLForSequenceNextVal($idColumnSequence);
            $row['id'] = $idExpr;
        } elseif ($idColumnStrategy === null) {
            // si aucune stratégie n'est spécifiée pour la colonne 'id', le type de cette colonne devra être
            // auto-incrémentable pour que l'INSERT fonctionne.
            // les éventuels ids sources sont écartés
            unset($row['id']);
            unset($row['ID']);
        }

        $createdOnCol = $this->config->getHistoColumnAliasForCreatedOn();
        $createdByCol = $this->config->getHistoColumnAliasForCreatedBy();
        $createdByValue = $this->config->getHistoColumnValueForCreatedBy();

        $destinationColumns = array_keys($row);

        $columnsToAdd = [
            $createdOnCol,
            $createdByCol,
        ];
        $valuesToAdd = [
            $now,
            $createdByValue !== null ? $createdByValue : 'NULL',
        ];
        if ($sourceCode !== null) {
            // si le code de la Source est fourni dans la config, on l'utilise
            $sourceCode = $this->platform->quoteStringLiteral($sourceCode);
            $columnsToAdd[] = 'source_id';
            $valuesToAdd[] = 's.id';
        } else {
            // si la Source n'est pas fournie (par son 'code'), elle est présente dans les données (colonne 'SOURCE_ID')
            if (array_key_exists('SOURCE_ID', $row)) {
                $sourceCode = $row['SOURCE_ID'];
                $row['SOURCE_ID'] = 's.id';
            } elseif (array_key_exists('source_id', $row)) {
                $sourceCode = $row['source_id'];
                $row['source_id'] = 's.id';
            } else {
                throw new HelperException("La colonne 'source_id' (ou 'SOURCE_ID') devrait être présente dans les données");
            }
        }

        // génération des noms de colonnes de la table destination pour l'INSERT.
        array_unshift($destinationColumns, ...$columnsToAdd);
        // génération de la liste des valeurs à INSERTer.
        $selectExpressions = $row;
        array_unshift($selectExpressions, ...$valuesToAdd);

        $commaSeparatedColumnNames = implode(', ', $destinationColumns);
        $commaSeparatedColumnValues = implode(', ', $selectExpressions);

        return <<<EOT
INSERT INTO $destinationTable ($commaSeparatedColumnNames)
SELECT $commaSeparatedColumnValues
FROM source s
WHERE s.code = $sourceCode;
EOT;
    }

    /**
     * @param string $destinationTable
     * @param string $sourceCodeColumn
     * @param array $columns
     * @param string|null $where
     * @param string|null $idColumnStrategy
     * @param string|null $idColumnSequence
     * @return string
     */
    public function generateSQLForInsertOperationInDestinationTable(
        string  $destinationTable,
        string  $sourceCodeColumn,
        array   $columns,
        string  $where = null,
        ?string $idColumnStrategy = null,
        ?string  $idColumnSequence = null): string
    {
        $diffViewName = $this->generateDiffViewName($destinationTable);
        $now = $this->platform->getNowExpression();

        // colonne ID éventuelle provisoirement retirée
        $columns = array_diff($columns, ['ID', 'id']);

        $sourceIdExistsInColumns = in_array('source_id', $columns) || in_array('SOURCE_ID', $columns);

        $createdOnCol = $this->config->getHistoColumnAliasForCreatedOn();
        $createdByCol = $this->config->getHistoColumnAliasForCreatedBy();
        $createdByValue = $this->config->getHistoColumnValueForCreatedBy();

        // génération des noms de colonnes de la table destination pour l'INSERT.
        $destinationColumns = $columns;
        if (!$sourceIdExistsInColumns) {
            array_unshift($destinationColumns,
                'source_id',
            );
        }
        array_unshift($destinationColumns,
            $createdOnCol,
            $createdByCol,
            $sourceCodeColumn,
        );
        if ($idColumnStrategy === DestinationInterface::ID_STRATEGY_SEQUENCE ||
            $idColumnStrategy === DestinationInterface::ID_STRATEGY_IDENTITY) {
            array_unshift($destinationColumns,
                'id',
            );
        }

        // génération de la liste des noms de colonnes provenant de la vue diff pour l'INSERT.
        $columnNameFormatter = fn(string $columnName) => Synchro::V_DIFF_SRC_COLUMN_PREFIX . strtoupper($columnName);
        $selectExpressions = array_map($columnNameFormatter, $columns);
        if (!$sourceIdExistsInColumns) {
            array_unshift($destinationColumns,
                'source_id',
            );
        }
        array_unshift($selectExpressions,
            $now,
            $createdByValue !== null ? $createdByValue : 'NULL',
            $sourceCodeColumn,
        );
        if ($idColumnStrategy === DestinationInterface::ID_STRATEGY_SEQUENCE) {
            // si aucun nom de séquence n'a été fourni, on utilise celui par défaut.
            $idColumnSequence = $idColumnSequence ?: $this->generateSQLForIdSequenceDefaultName($destinationTable);
            $idExpr = $this->generateSQLForSequenceNextVal($idColumnSequence);
            array_unshift($selectExpressions,
                $idExpr,
            );
        } elseif ($idColumnStrategy === DestinationInterface::ID_STRATEGY_IDENTITY) {
            array_unshift($selectExpressions,
                $columnNameFormatter('id'),
            );
        }

        $wheres = null;
        if ($where) {
            $wheres = sprintf(" AND (%s)", $where);
        }

        $operation = Operation::OPERATION_INSERT;

        // instruction de mise à jour de la table destination
        $markup = "INSERT INTO $destinationTable (" . implode(', ', $destinationColumns) . ")" . PHP_EOL;
        $markup .= "SELECT " . implode(', ', $selectExpressions) . PHP_EOL;
        $markup .= "FROM $diffViewName d WHERE operation = '$operation' ";
        $markup .= $wheres;

        return $markup;
    }

    /**
     * @param string $destinationTable
     * @param string $sourceCodeColumn
     * @param array $columns
     * @param string|null $where
     * @return string
     */
    public function generateSQLForUpdateOperationInDestinationTable(string $destinationTable, string $sourceCodeColumn, array $columns, ?string $where): string
    {
        return $this->generateSQLUpdate(Operation::OPERATION_UPDATE, $destinationTable, $sourceCodeColumn, $columns, $where);
    }

    /**
     * @param string $destinationTable
     * @param string $sourceCodeColumn
     * @param array $columns
     * @param string|null $where
     * @return string
     */
    public function generateSQLForUndeleteOperationInDestinationTable(string $destinationTable, string $sourceCodeColumn, array $columns, ?string $where): string
    {
        return $this->generateSQLUpdate(Operation::OPERATION_UNDELETE, $destinationTable, $sourceCodeColumn, $columns, $where);
    }

    /**
     * @param string $destinationTable
     * @param string $sourceCodeColumn
     * @param array $columns
     * @param string|null $where
     * @return string
     */
    public function generateSQLForDeleteOperationInDestinationTable(string $destinationTable, string $sourceCodeColumn, array $columns, ?string $where): string
    {
        return $this->generateSQLUpdate(Operation::OPERATION_DELETE, $destinationTable, $sourceCodeColumn, $columns, $where);
    }

    /**
     * @param string $operation
     * @param string $destinationTable
     * @param string $sourceCodeColumn
     * @param array $columns
     * @param string|null $where
     * @return string
     */
    abstract protected function generateSQLUpdate(string $operation, string $destinationTable, string $sourceCodeColumn, array $columns, ?string $where): string;

    /**
     * @param string $operation
     * @param array $columns
     * @return string
     * @throws \UnicaenDbImport\CodeGenerator\Helper\HelperException
     */
    protected function generateSQLUpdateSetters(string $operation, array $columns): string
    {
        $now = $this->platform->getNowExpression();

        switch ($operation) {

            case Operation::OPERATION_UPDATE:
                $colUpdates = array_map(function ($col) {
                    return "$col = diff.S_$col";
                }, $columns);

                $updatedOnColumn = $this->config->getHistoColumnAliasForUpdatedOn();
                $updatedByColumn = $this->config->getHistoColumnAliasForUpdatedBy();
                $updatedByValue = $this->config->getHistoColumnValueForUpdatedBy();
                $updatedByValue = $updatedByValue !== null ? $updatedByValue : 'NULL';
                array_unshift($colUpdates,
                    "$updatedOnColumn = $now",
                    "$updatedByColumn = $updatedByValue",
                );

                $setters = implode(', ', $colUpdates);
                break;

            case Operation::OPERATION_UNDELETE:
                $colUpdates = array_map(function ($col) {
                    return "$col = diff.S_$col";
                }, $columns);

                $updatedOnColumn = $this->config->getHistoColumnAliasForUpdatedOn();
                $updatedByColumn = $this->config->getHistoColumnAliasForUpdatedBy();
                $updatedByValue = $this->config->getHistoColumnValueForUpdatedBy();
                $deletedOnColumn = $this->config->getHistoColumnAliasForDeletedOn();
                $deletedByColumn = $this->config->getHistoColumnAliasForDeletedBy();
                $updatedByValue = $updatedByValue !== null ? $updatedByValue : 'NULL';
                array_unshift($colUpdates,
                    "$updatedOnColumn = $now",
                    "$updatedByColumn = $updatedByValue",
                    "$deletedOnColumn = null",
                    "$deletedByColumn = null"
                );

                $setters = implode(', ', $colUpdates);
                break;

            case Operation::OPERATION_DELETE:
                $deletedOnColumn = $this->config->getHistoColumnAliasForDeletedOn();
                $deletedByColumn = $this->config->getHistoColumnAliasForDeletedBy();
                $deletedByValue = $this->config->getHistoColumnValueForDeletedBy();
                $deletedByValue = $deletedByValue !== null ? $deletedByValue : 'NULL';
                $colUpdates = [
                    "$deletedOnColumn = $now",
                    "$deletedByColumn = $deletedByValue"
                ];

                $setters =  implode(', ', $colUpdates);
                break;

            default:
                throw new HelperException("Opération spécifiée inattendue: " . $operation);
        }

        return $setters;
    }

    /**
     * @param mixed $value
     * @return string|int
     */
    protected function formatValueForInsert($value)
    {
        if ($value === null) {
            return 'NULL';
        }

        if (is_bool($value)) {
            return $this->platformSupportsBooleanType() ? ($value ? 'true' : 'false') : (int)$value;
        }

        if ($value instanceof stdClass) {
            if (isset($value->date)) {
                $tz = isset($value->timezone) ? new DateTimeZone($value->timezone) : NULL;
                $d = date_create($value->date, $tz);

                return $this->formatDateTimeForInsert($d);
            } else {
                throw new RuntimeException("Type de valeur inattendu!");
            }
        }

        return $this->platform->quoteStringLiteral($value);
    }

    /**
     * Formate un DateTime pour pouvoir l'utiliser dans un INSERT.
     *
     * @param \DateTime $value
     * @return string
     */
    protected function formatDateTimeForInsert(DateTime $value): string
    {
        $value = $value->format('c');

        return $this->platform->quoteStringLiteral($value);
    }

    /**
     * @param string $tableName
     * @return string
     */
    public function generateSQLForIdSequenceDefaultName(string $tableName): string
    {
        return $tableName . '_ID_SEQ';
    }

    /**
     * @param string $columnName
     * @return string
     */
    abstract public function generateSQLForCastToString(string $columnName): string;

    /**
     * @param string $sequenceName
     * @return string
     */
    abstract public function generateSQLForSequenceNextVal(string $sequenceName): string;
}