<?php

namespace UnicaenDbImport\CodeGenerator\Helper;

use DateTime;
use Exception;
use UnicaenDbImport\CodeGenerator\Helper;
use UnicaenDbImport\Domain\ImportResult;
use UnicaenDbImport\Domain\ResultInterface;
use UnicaenDbImport\Domain\SynchroResult;

/**
 * Génération de code SQL concernant la table de log de synchro.
 *
 * Version commune à toutes les plateformes de bases de données.
 *
 * @author Unicaen
 */
abstract class LogTableHelper extends Helper
{
    /**
     * @param string $logTableName
     * @return string
     * @throws \UnicaenDbImport\CodeGenerator\Helper\HelperException
     */
    public function generateSQLForLogTableCreation(string $logTableName): string
    {
        $varchar = $this->platform->getStringTypeDeclarationSQL(['length' => 128]);
        $boolean = $this->platform->getBooleanTypeDeclarationSQL([]);
        $text = $this->platform->getClobTypeDeclarationSQL([]);
        try {
            $datetime = $this->platform->getDateTimeTypeDeclarationSQL([]);
        } catch (\Doctrine\DBAL\Exception $e) {
            throw new HelperException("Non implémentée!");
        }

        return <<<EOT
CREATE TABLE $logTableName (
    type $varchar not null,
    name $varchar not null,
    success $boolean not null,
    has_problems $boolean not null,
    log $text not null,
    started_on $datetime not null,
    ended_on   $datetime not null,
    import_hash $varchar not null
)
EOT;
    }

    /**
     * @param ResultInterface $result
     * @param string $logTableName
     * @return string
     * @throws \UnicaenDbImport\CodeGenerator\Helper\HelperException
     */
    public function generateSQLForInsertResultIntoLogTable(ResultInterface $result, string $logTableName): string
    {
        switch (true) {
            case $result instanceof ImportResult:
                $type = 'import';
                $name = $result->getImport()->getName();
                break;
            case $result instanceof SynchroResult:
                $type = 'synchro';
                $name = $result->getSynchro()->getName();
                break;
            default:
                throw new HelperException("Type de résultat inattendu");
        }
        $success = $result->getFailureException() ? 0 : 1;
        if ($this->platformSupportsBooleanType()) {
            $success = $success ? 'true' : 'false';
        }
        $hasProblems = $result->hasExceptionInResults() ? 1 : 0;
        if ($this->platformSupportsBooleanType()) {
            $hasProblems = $hasProblems ? 'true' : 'false';
        }
        $log = $result->toString(true);
        if ($exception = $result->getFailureException()) {
            $log .= PHP_EOL . $exception->getMessage();
            while ($exception = $exception->getPrevious()) {
                $log .= PHP_EOL . 'Previous : ' . $exception->getMessage();
            }
        }
        $startDate = $this->generateSQLForDateTimeColumnValueInsert($result->getStartDate());
        $endDate = $this->generateSQLForDateTimeColumnValueInsert($result->getEndDate());

        try {
            $hash = $result->getHash();
        } catch (Exception $e) {
            throw new HelperException("Erreur lors de la génération du hash !", null, $e);
        }

        $sql = "INSERT INTO $logTableName (type, name, success, has_problems, log, started_on, ended_on, import_hash) ";
        $sql .= sprintf("VALUES (%s, %s, %s, %s, %s, %s, %s, %s)",
            $this->platform->quoteStringLiteral($type),
            $this->platform->quoteStringLiteral($name),
            $success,
            $hasProblems,
            $this->platform->quoteStringLiteral($log),
            $startDate,
            $endDate,
            $this->platform->quoteStringLiteral($hash)
        );

        return $sql;
    }

    abstract public function generateSQLForDateTimeColumnValueInsert(DateTime $datetime): string;
}