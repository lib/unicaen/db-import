<?php

namespace UnicaenDbImport\CodeGenerator\Oracle\Helper;

use DateTime;

/**
 * Trait destiné à regrouper les syntaxes d'Oracle pour réaliser des opérations **basiques**,
 * réutilisables dans plusieurs helpers.
 */
trait HelperTrait
{
    public function generateSQLForSequenceNextVal(string $sequenceName): string
    {
        return $sequenceName . '.nextval';
    }

    public function generateSQLForCastToString(string $columnName): string
    {
        return $columnName . "||''";
    }

    public function generateSQLForDateTimeColumnValueInsert(DateTime $datetime): string
    {
        return sprintf("to_timestamp('%s', 'YYYY-MM-DD HH24:MI:SS.FF')", $datetime->format("Y-m-d H:i:s.u"));
    }
}