<?php

namespace UnicaenDbImport\CodeGenerator\Oracle\Helper;

use DateTime;
use UnicaenDbImport\Platforms\OraclePlatform;

/**
 * Génération de code SQL concernant la table de log de synchro.
 *
 * Version Oracle.
 *
 * @author Unicaen
 */
class LogTableHelper extends \UnicaenDbImport\CodeGenerator\Helper\LogTableHelper
{
    use HelperTrait;

    /**
     * @var OraclePlatform
     */
    protected $platform;

    /**
     * Helper constructor.
     */
    public function __construct()
    {
        $this->platform = new OraclePlatform();
    }
}