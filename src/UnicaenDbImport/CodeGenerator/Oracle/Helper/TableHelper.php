<?php

namespace UnicaenDbImport\CodeGenerator\Oracle\Helper;

use RuntimeException;
use UnicaenDbImport\CodeGenerator\Helper\HelperException;
use UnicaenDbImport\Platforms\OraclePlatform;

/**
 * Génération de code SQL autour des tables.
 *
 * Version Oracle.
 *
 * @author Unicaen
 */
class TableHelper extends \UnicaenDbImport\CodeGenerator\Helper\TableHelper
{
    use HelperTrait;

    /**
     * Helper constructor.
     */
    public function __construct()
    {
        $this->platform = new OraclePlatform();
    }

    /**
     * @inheritDoc
     */
    public function generateSQLForIntermediateTableCreation(string $destinationTable, string $sourceCodeColumn, array $columns, string $intermediateTable): string
    {
        // les ; perturbent le driver Oracle donc on met ça dans un bloc BEGIN...END
        return
            'BEGIN ' . PHP_EOL .
            parent::generateSQLForIntermediateTableCreation($destinationTable, $sourceCodeColumn, $columns, $intermediateTable) . PHP_EOL .
            'END;';
    }

    /**
     * {@inheritDoc}
     */
    protected function generateDiffViewDeletionSQLSnippet(string $destinationTable): string
    {
        $name = $this->generateDiffViewName($destinationTable);

        return "DROP VIEW $name";
    }

    /**
     * @param string $operation
     * @param string $destinationTable
     * @param string $sourceCodeColumn
     * @param array $columns
     * @param string|null $where
     * @return string
     */
    protected function generateSQLUpdate(string $operation, string $destinationTable, string $sourceCodeColumn, array $columns, string $where = null): string
    {
        // ne surtout pas mettre à jour la colonne ID !
        $columns = array_diff($columns, ['ID', 'id']);

        $diffViewName = $this->generateDiffViewName($destinationTable);
        try {
            $setters = $this->generateSQLUpdateSetters($operation, $columns);
        } catch (HelperException $e) {
            throw new RuntimeException("Erreur rencontrée lors de la génération SQL", null, $e);
        }

        $wheres = null;
        if ($where) {
            $wheres = sprintf(" AND (%s)", $where);
        }

        // instruction de mise à jour de la table destination
        $markup = "MERGE INTO $destinationTable d USING (SELECT * FROM $diffViewName) diff " . PHP_EOL;
        $markup .= "ON (diff.operation = '$operation' AND d.$sourceCodeColumn = diff.$sourceCodeColumn $wheres) " . PHP_EOL;
        $markup .= "WHEN MATCHED THEN UPDATE SET " . PHP_EOL;
        $markup .= $setters;

        return $markup;
    }

    protected function generateSQLForViewDropBeforeUpdate($name): ?string
    {
        return null;
    }
}