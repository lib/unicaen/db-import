<?php

namespace UnicaenDbImport\CodeGenerator\Oracle\Helper;

use UnicaenDbImport\Platforms\OraclePlatform;

/**
 * Version Oracle.
 *
 * @author Unicaen
 */
class TableValidationHelper extends \UnicaenDbImport\CodeGenerator\Helper\TableValidationHelper
{
    use HelperTrait;

    /**
     * @var OraclePlatform
     */
    protected $platform;

    /**
     * Helper constructor.
     */
    public function __construct()
    {
        $this->platform = new OraclePlatform();
    }

    /**
     * {@inheritDoc}
     */
    public function generateSQLForTableExistenceCheck(string $tableName): string
    {
        return <<<EOT
WITH tmp AS (
    SELECT count(*) as table_exists
    FROM ALL_TABLES
    WHERE upper(table_name) = upper('$tableName')
    UNION
    SELECT count(*) as table_exists
    FROM ALL_VIEWS
    WHERE upper(view_name) = upper('$tableName')
)
SELECT sum(table_exists) AS table_exists
FROM tmp
EOT;
    }

    /**
     * {@inheritDoc}
     */
    public function generateSQLForSequenceExistenceCheck(string $sequenceName): string
    {
        return <<<EOT
SELECT count(*) as seq_exists
FROM all_sequences
WHERE upper(sequence_name) = upper('$sequenceName')
EOT;
    }

    /**
     * {@inheritDoc}
     */
    public function generateSQLForColumnsValidation($tableName, array $columnsAndTypes): string
    {
        $selects = [];
        foreach ($columnsAndTypes as $column => $type) {
            if (is_numeric($column)) {
                $column = $type;
                $type = 'NULL';
            } else {
                $type = "'$type'";
            }
            $selects[] = "SELECT '$column', $type FROM DUAL";
        }
        $selects = implode(' UNION' . PHP_EOL, $selects);

        return <<<EOT

WITH required_cols(column_name, column_type) AS (
$selects
)
SELECT rc.column_name, c.data_type
FROM required_cols rc
JOIN all_tab_cols c ON upper(c.column_name) = upper(rc.column_name)
WHERE upper(c.table_name) = upper('$tableName') AND (rc.column_type IS NULL OR rc.column_type = c.data_type)
EOT;
    }

    /**
     * @param string $tableName
     * @param int $columnsCount
     * @return string
     */
    public function generateSQLForIdColumnValidation(string $tableName, int &$columnsCount): string
    {
        $columnsAndTypes = [
            'id' => 'NUMBER',
        ];

        $columnsCount = count($columnsAndTypes);

        return $this->generateSQLForColumnsValidation($tableName, $columnsAndTypes);
    }

    /**
     * {@inheritDoc}
     */
    public function generateSQLForHistoColumnsValidation(string $tableName, int &$columnsCount): string
    {
        $columnsAndTypes = [
            $this->config->getHistoColumnAliasForCreatedOn() => 'DATE',
            $this->config->getHistoColumnAliasForUpdatedOn() => 'DATE',
            $this->config->getHistoColumnAliasForDeletedOn() => 'DATE',
            $this->config->getHistoColumnAliasForCreatedBy() => 'NUMBER',
            $this->config->getHistoColumnAliasForUpdatedBy() => 'NUMBER',
            $this->config->getHistoColumnAliasForDeletedBy() => 'NUMBER',
        ];

        $columnsCount = count($columnsAndTypes);

        return $this->generateSQLForColumnsValidation($tableName, $columnsAndTypes);
    }

    /**
     * {@inheritDoc}
     */
    public function generateSQLForSourceColumnsValidation(string $tableName, string $sourceCodeColumn, int &$columnCount): string
    {
        $columnsAndTypes = [
            $sourceCodeColumn => 'VARCHAR2',
            'source_id' => 'NUMBER',
        ];

        $columnCount = count($columnsAndTypes);

        return $this->generateSQLForColumnsValidation($tableName, $columnsAndTypes);
    }
}