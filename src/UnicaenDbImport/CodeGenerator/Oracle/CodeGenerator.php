<?php

namespace UnicaenDbImport\CodeGenerator\Oracle;

use UnicaenDbImport\CodeGenerator\Oracle;
use UnicaenDbImport\Platforms\OraclePlatform;

/**
 * Version Oracle.
 *
 * @author Unicaen
 *
 * @property Oracle\Helper\TableHelper $tableHelper
 * @property Oracle\Helper\TableValidationHelper $tableValidationHelper
 * @property Oracle\Helper\LogTableHelper $logTableHelper
 */
class CodeGenerator extends \UnicaenDbImport\CodeGenerator\CodeGenerator
{
    /**
     * CodeGenerator constructor.
     */
    public function __construct()
    {
        $this->platform = new OraclePlatform();
    }

    /**
     * @inheritDoc
     */
    public function generateSQLForIntermmediateTableDrop(string $tableName): string
    {
        return "DROP TABLE $tableName CASCADE CONSTRAINTS ;";
    }

    /**
     * @inheritDoc
     */
    public function generateSQLForSelectSourceTableColumnsInformation(string $sourceTableName): string
    {
        return sprintf("select column_name  as \"column_name\" from all_tab_cols where lower(table_name) = lower('%s')",
            $sourceTableName
        );
    }
}