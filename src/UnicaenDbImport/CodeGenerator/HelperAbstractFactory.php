<?php

namespace UnicaenDbImport\CodeGenerator;

use Psr\Container\ContainerInterface;
use UnicaenDbImport\Config\Config;
use Laminas\ServiceManager\Factory\AbstractFactoryInterface;

class HelperAbstractFactory implements AbstractFactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return Helper
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null): Helper
    {
        /** @var Config $config */
        $config = $container->get(Config::class);

        $helper = new $requestedName();
        $helper->setConfig($config);

        return $helper;
    }

    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @return bool
     */
    public function canCreate(ContainerInterface $container, $requestedName): bool
    {
        return $this->startsWithNamespace($requestedName) AND $this->endsWithHelperNamespaceFragment($requestedName);
    }

    /**
     * @param string $requestedName
     * @return bool
     */
    protected function startsWithNamespace(string $requestedName): bool
    {
        // retourne `true` si `$requestedName` commence par le namespace courant
        return substr($requestedName, 0, strlen(__NAMESPACE__)) === __NAMESPACE__;
    }

    /**
     * @param string $requestedName
     * @return bool
     */
    protected function endsWithHelperNamespaceFragment(string $requestedName): bool
    {
        // fragments de namespace des helpers
        $helperNamespaceFragment = [
            '\Helper\LogTableHelper',
            '\Helper\TableHelper',
            '\Helper\TableValidationHelper',
        ];
        foreach ($helperNamespaceFragment as $helperNamespacePart) {
            $endsWithHelperNamespacePart = substr($requestedName, -1 * strlen($helperNamespacePart)) === $helperNamespacePart;
            if ($endsWithHelperNamespacePart) {
                // retourne `true` si `$requestedName` se termine par l'un des fragments de namespace
                return true;
            }
        }

        return false;
    }
}