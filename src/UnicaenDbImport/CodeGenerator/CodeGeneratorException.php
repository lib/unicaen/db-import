<?php

namespace UnicaenDbImport\CodeGenerator;

use BadMethodCallException;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use RuntimeException;

class CodeGeneratorException extends RuntimeException
{
    /**
     * @param string $name
     * @return CodeGeneratorException
     */
    public static function codeGeneratorServiceNotFound(string $name): CodeGeneratorException
    {
        return new static("CodeGenerator introuvable avec l'identifiant de service suivant : " . $name);
    }

    /**
     * @param string $method
     * @param string $calledClass
     * @param \Doctrine\DBAL\Platforms\AbstractPlatform|null $platform
     * @return BadMethodCallException
     */
    public static function methodToImplement(string $method, string $calledClass, AbstractPlatform $platform = null): BadMethodCallException
    {
        return new BadMethodCallException(sprintf(
            "Vous devez implémenter la méthode '%s::%s' sur mesure pour la pateforme '%s'",
            $method, $calledClass, get_class($platform)
        ));

    }
}