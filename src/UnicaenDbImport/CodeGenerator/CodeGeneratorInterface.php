<?php

namespace UnicaenDbImport\CodeGenerator;

use RuntimeException;
use UnicaenDbImport\Domain\DestinationInterface;
use UnicaenDbImport\Domain\ResultInterface;
use UnicaenDbImport\Domain\SourceInterface;

/**
 * Contrat à honorer pour les générateurs de code SQL.
 *
 * @package UnicaenDbImport\CodeGenerator\Common
 */
interface CodeGeneratorInterface
{
    /**
     * @param string $tableName
     * @return string
     */
    public function generateSQLForTableExistenceCheck(string $tableName): string;

    /**
     * @param array $result
     * @return bool
     */
    public function convertTableExistenceCheckResultToBoolean(array $result): bool;

    /**
     * @param string $tableName
     * @param int $columnsCount
     * @return string
     */
    public function generateSQLForHistoColumnsValidationInTable(string $tableName, int &$columnsCount): string;

    /**
     * @param string $tableName
     * @param array $result
     * @param int $columnCount
     * @return null|RuntimeException
     */
    public function convertHistoColumnsValidationBadResultToException(string $tableName, array $result, int $columnCount): ?RuntimeException;

    /**
     * @param string $tableName
     * @param string $sourceCodeColumn
     * @param int $columCount
     * @return string
     * @codeCoverageIgnore Car simple délégation
     */
    public function generateSQLForSourceColumnsValidationInTable(string $tableName, string $sourceCodeColumn, int &$columCount): string;

    /**
     * @param string $tableName
     * @param string $sourceCodeColumn
     * @param array $result
     * @param int $columCount
     * @return null|RuntimeException
     * @codeCoverageIgnore Car simple délégation
     */
    public function convertSourceColumnsValidationBadResultToException(string $tableName, string $sourceCodeColumn, array $result, int $columCount): ?RuntimeException;

    /**
     * @param SourceInterface $source
     * @return string
     */
    public function generateSQLForSelectFromSource(SourceInterface $source): string;

    /**
     * @param string $sourceTableName
     * @return string
     */
    public function generateSQLForSelectSourceTableColumnsInformation(string $sourceTableName): string;

    /**
     * @param SourceInterface $source
     * @param DestinationInterface $destination
     * @param string $intermediateTable
     * @return string
     */
    public function generateSQLForIntermediateTableCreation(SourceInterface      $source,
                                                            DestinationInterface $destination,
                                                            string               $intermediateTable): string;

    /**
     * @param string $tableName
     * @param array $row
     * @param string|null $sourceCode
     * @param string|null $idColumnStrategy
     * @param string|null $idColumnSequence
     * @return string
     */
    public function generateSQLForInsertOneRowIntoTable(string $tableName,
                                                        array  $row,
                                                        string $sourceCode = null,
                                                        ?string $idColumnStrategy = null,
                                                        ?string $idColumnSequence = null): string;

    /**
     * @param string $logTableName
     * @return string
     */
    public function generateSQLForImportLogTableCreation(string $logTableName): string;

    /**
     * @param string $tableName
     * @return string
     */
    public function generateSQLForIntermmediateTableDrop(string $tableName): string;

    /**
     * @param string $tableName
     * @return string
     */
    public function generateSQLForClearTable(string $tableName): string;

    /**
     * @param SourceInterface $source
     * @param DestinationInterface $destination
     * @param string $diffViewName
     * @param string|null $intermediateTable
     * @return string
     * @throws \Doctrine\DBAL\Exception
     */
    public function generateSQLForDiffViewCreation(SourceInterface $source,
                                                   DestinationInterface $destination,
                                                   string $diffViewName,
                                                   string $intermediateTable = null): string;

    /**
     * @param SourceInterface $source
     * @param DestinationInterface $destination
     * @return string
     */
    public function generateSQLForDiffViewSelect(SourceInterface $source, DestinationInterface $destination): string;

    /**
     * @param string $operation
     * @param SourceInterface $source
     * @param DestinationInterface $destination
     * @return string
     */
    public function generateSQLForDestinationUpdate(
        string               $operation,
        SourceInterface      $source,
        DestinationInterface $destination): string;

    /**
     * @param ResultInterface $result
     * @param string $importLogTable
     * @return string
     */
    public function generateSQLForInsertResultIntoLogTable(ResultInterface $result, string $importLogTable): string;
}