<?php

namespace UnicaenDbImport\CodeGenerator;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use UnicaenDbImport\Config\ConfigAwareTrait;

/**
 * Classe mère des helpers de génération de code SQL.
 *
 * @author Unicaen
 */
abstract class Helper
{
    use ConfigAwareTrait;

    /**
     * @var AbstractPlatform
     */
    protected $platform;

    /**
     * @return bool
     */
    public function platformSupportsBooleanType(): bool
    {
        return
            $this->platform->getBooleanTypeDeclarationSQL([]) === 'BOOLEAN' ||
            $this->platform->getBooleanTypeDeclarationSQL([]) === 'boolean';
    }

    /**
     * @param string $argument
     * @return string
     */
    protected function getQuoteLiteralFunctionCallSQLSnippet(string $argument): string
    {
        throw CodeGeneratorException::methodToImplement(__METHOD__, get_called_class(), $this->platform);
    }

    /**
     * @param int $length
     * @param string $string
     * @return string
     */
    protected function indent(int $length, string $string): string
    {
        $prefix = str_repeat(' ', $length);

        $lines = explode(PHP_EOL, $string);
        $indentedLines = array_map(function($line) use ($prefix) {
            return strlen($line) > 0 ? $prefix . $line : '';
        }, $lines);

        return implode(PHP_EOL, $indentedLines);
    }
}