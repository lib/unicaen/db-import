<?php

namespace UnicaenDbImport\CodeGenerator\PostgreSQL;

use Psr\Container\ContainerInterface;
use UnicaenDbImport\Config\Config;

class CodeGeneratorFactory
{
    /**
     * @param ContainerInterface $container
     * @return CodeGenerator
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container): CodeGenerator
    {
        /** @var Config $config */
        $config = $container->get(Config::class);

        $tableHelper = $container->get(Helper\TableHelper::class);
        $tableValidationHelper = $container->get(Helper\TableValidationHelper::class);
        $logTableHelper = $container->get(Helper\LogTableHelper::class);

        $codeGenerator = new CodeGenerator();
        $codeGenerator->setConfig($config);
        $codeGenerator->setTableHelper($tableHelper);
        $codeGenerator->setTableValidationHelper($tableValidationHelper);
        $codeGenerator->setLogTableHelper($logTableHelper);

        return $codeGenerator;
    }
}