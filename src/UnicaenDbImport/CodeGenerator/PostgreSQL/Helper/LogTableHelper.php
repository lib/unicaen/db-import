<?php

namespace UnicaenDbImport\CodeGenerator\PostgreSQL\Helper;

use Doctrine\DBAL\Platforms\PostgreSqlPlatform;

/**
 * Génération de code SQL concernant la table de log de synchro.
 *
 * Version PostgreSQL.
 *
 * @author Unicaen
 */
class LogTableHelper extends \UnicaenDbImport\CodeGenerator\Helper\LogTableHelper
{
    use HelperTrait;

    /**
     * @var PostgreSqlPlatform
     */
    protected $platform;

    /**
     * Helper constructor.
     */
    public function __construct()
    {
        $this->platform = new PostgreSqlPlatform();
    }
}