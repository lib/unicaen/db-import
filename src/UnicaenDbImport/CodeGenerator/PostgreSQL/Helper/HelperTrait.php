<?php

namespace UnicaenDbImport\CodeGenerator\PostgreSQL\Helper;

use DateTime;

/**
 * Trait destiné à regrouper les syntaxes de PostgreSQL pour réaliser des opérations **basiques**,
 * réutilisables dans plusieurs helpers.
 */
trait HelperTrait
{
    public function generateSQLForSequenceNextVal(string $sequenceName): string
    {
        return "nextval('" . $sequenceName . "')";
    }

    public function generateSQLForCastToString(string $columnName): string
    {
        return $columnName . '::varchar';
    }

    public function generateSQLForDateTimeColumnValueInsert(DateTime $datetime): string
    {
        return $this->platform->quoteStringLiteral($datetime->format("Y-m-d H:i:s.u"));
    }
}