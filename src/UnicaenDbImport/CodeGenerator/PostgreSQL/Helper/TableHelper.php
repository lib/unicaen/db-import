<?php

namespace UnicaenDbImport\CodeGenerator\PostgreSQL\Helper;

use Doctrine\DBAL\Platforms\PostgreSQLPlatform;
use RuntimeException;
use UnicaenDbImport\CodeGenerator\Helper\HelperException;

/**
 * Génération de code SQL autour des tables.
 *
 * Version PostgreSQL.
 *
 * @author Unicaen
 */
class TableHelper extends \UnicaenDbImport\CodeGenerator\Helper\TableHelper
{
    use HelperTrait;

    /**
     * Helper constructor.
     */
    public function __construct()
    {
        $this->platform = new PostgreSQLPlatform();
    }

    /**
     * {@inheritDoc}
     */
    protected function generateDiffViewDeletionSQLSnippet(string $destinationTable): string
    {
        $name = $this->generateDiffViewName($destinationTable);

        return "DROP VIEW IF EXISTS $name";
    }

    /**
     * @param string $operation
     * @param string $destinationTable
     * @param string $sourceCodeColumn
     * @param array $columns
     * @param string|null $where
     * @return string
     */
    protected function generateSQLUpdate(string $operation, string $destinationTable, string $sourceCodeColumn, array $columns, string $where = null): string
    {
        // ne surtout pas mettre à jour la colonne ID !
        $columns = array_diff($columns, ['ID', 'id']);

        $diffViewName = $this->generateDiffViewName($destinationTable);
        try {
            $setters = $this->generateSQLUpdateSetters($operation, $columns);
        } catch (HelperException $e) {
            throw new RuntimeException("Erreur rencontrée lors de la génération SQL", null, $e);
        }
        $wheres = "diff.operation = '$operation' AND d.$sourceCodeColumn = diff.$sourceCodeColumn";
        if ($where) {
            $wheres .= sprintf(" AND (%s)", $where);
        }

        // instruction de mise à jour de la table destination
        $markup = "UPDATE $destinationTable d SET " . PHP_EOL;
        $markup .= $setters . PHP_EOL;
        $markup .= "FROM (SELECT * FROM $diffViewName) AS diff " . PHP_EOL;
        $markup .= "WHERE $wheres";

        return $markup;
    }

    protected function generateSQLForViewDropBeforeUpdate($name): ?string
    {
        return $this->platform->getDropViewSQL('IF EXISTS ' . $name);
    }
}