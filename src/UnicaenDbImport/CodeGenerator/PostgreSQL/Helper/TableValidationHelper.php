<?php

namespace UnicaenDbImport\CodeGenerator\PostgreSQL\Helper;

use Doctrine\DBAL\Platforms\PostgreSqlPlatform;

/**
 * Version PostgreSQL.
 *
 * @author Unicaen
 */
class TableValidationHelper extends \UnicaenDbImport\CodeGenerator\Helper\TableValidationHelper
{
    /**
     * @var PostgreSqlPlatform
     */
    protected $platform;

    /**
     * Helper constructor.
     */
    public function __construct()
    {
        $this->platform = new PostgreSqlPlatform();
    }

    /**
     * {@inheritDoc}
     */
    public function generateSQLForTableExistenceCheck(string $tableName): string
    {
        return <<<EOT
SELECT count(*) as table_exists
FROM   information_schema.tables
WHERE  table_schema = 'public' AND upper(table_name) = upper('$tableName')
EOT;
    }

    /**
     * {@inheritDoc}
     */
    public function generateSQLForSequenceExistenceCheck(string $sequenceName): string
    {
        return <<<EOF
SELECT count(*) as seq_exists
FROM   information_schema.sequences
WHERE  sequence_schema = 'public' AND upper(sequence_name) = upper('$sequenceName')
EOF;

    }

    /**
     * {@inheritDoc}
     */
    public function generateSQLForColumnsValidation($tableName, array $columnsAndTypes): string
    {
        $selects = [];
        foreach ($columnsAndTypes as $column => $type) {
            if (is_numeric($column)) {
                $column = $type;
                $type = 'NULL';
            } else {
                $type = "'$type'::varchar";
            }
            $selects[] = "SELECT '$column'::varchar, $type";
        }
        $selects = implode(' UNION' . PHP_EOL, $selects);

        return <<<EOT

WITH required_cols(column_name, column_type) AS (
$selects
)
SELECT rc.column_name, c.data_type
FROM required_cols rc
JOIN information_schema.columns c ON upper(c.column_name) = upper(rc.column_name)
WHERE upper(c.table_name) = upper('$tableName') AND (rc.column_type  IS NULL OR rc.column_type = c.data_type)
;
EOT;
    }

    /**
     * @param string $tableName
     * @param int $columnsCount
     * @return string
     */
    public function generateSQLForIdColumnValidation(string $tableName, int &$columnsCount): string
    {
        $columnsAndTypes = [
            'id' => 'bigint',
        ];

        $columnsCount = count($columnsAndTypes);

        return $this->generateSQLForColumnsValidation($tableName, $columnsAndTypes);
    }

    /**
     * {@inheritDoc}
     */
    public function generateSQLForHistoColumnsValidation(string $tableName, int &$columnsCount): string
    {
        $columnsAndTypes = [
            $this->config->getHistoColumnAliasForCreatedOn() => 'timestamp without time zone',
            $this->config->getHistoColumnAliasForUpdatedOn() => 'timestamp without time zone',
            $this->config->getHistoColumnAliasForDeletedOn() => 'timestamp without time zone',
            $this->config->getHistoColumnAliasForCreatedBy() => 'bigint',
            $this->config->getHistoColumnAliasForUpdatedBy() => 'bigint',
            $this->config->getHistoColumnAliasForDeletedBy() => 'bigint',
        ];

        $columnsCount = count($columnsAndTypes);

        return $this->generateSQLForColumnsValidation($tableName, $columnsAndTypes);
    }

    /**
     * {@inheritDoc}
     */
    public function generateSQLForSourceColumnsValidation(string $tableName, string $sourceCodeColumn, int &$columCount): string
    {
        $columnsAndTypes = [
            $sourceCodeColumn/* => 'character varying'*/,
            'source_id' => 'bigint',
        ];

        $columCount = count($columnsAndTypes);

        return $this->generateSQLForColumnsValidation($tableName, $columnsAndTypes);
    }
}