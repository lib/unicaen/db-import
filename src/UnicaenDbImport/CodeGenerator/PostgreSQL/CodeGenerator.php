<?php

namespace UnicaenDbImport\CodeGenerator\PostgreSQL;

use Doctrine\DBAL\Platforms\PostgreSqlPlatform;
use UnicaenDbImport\CodeGenerator\PostgreSQL;

/**
 * Version PostgreSQL.
 *
 * @author Unicaen
 *
 * @property PostgreSQL\Helper\TableHelper $tableHelper
 * @property PostgreSQL\Helper\TableValidationHelper $tableValidationHelper
 * @property PostgreSQL\Helper\LogTableHelper $logTableHelper
 */
class CodeGenerator extends \UnicaenDbImport\CodeGenerator\CodeGenerator
{
    /**
     * CodeGenerator constructor.
     */
    public function __construct()
    {
        $this->platform = new PostgreSqlPlatform();
    }

    /**
     * @inheritDoc
     */
    public function generateSQLForIntermmediateTableDrop(string $tableName): string
    {
        return "DROP TABLE IF EXISTS $tableName CASCADE ;";
    }

    /**
     * @inheritDoc
     */
    public function generateSQLForSelectSourceTableColumnsInformation(string $sourceTableName): string
    {
        return sprintf("select column_name from information_schema.columns where lower(table_name) = lower('%s')",
            $sourceTableName
        );
    }
}