<?php

namespace UnicaenDbImport\Formatter;

use UnicaenDbImport\Domain\Operation;
use UnicaenDbImport\Domain\Synchro;

class SynchroDiffFormatter
{
    public function format(Synchro $synchro, array $diff): array
    {
        $sourceCodeColumn = $synchro->getDestination()->getSourceCodeColumn();

        $result = [];

        foreach ($diff as $row) {
            $operation = $row['operation'];

            $data = [];
            $data[$sourceCodeColumn] = $row[strtoupper($sourceCodeColumn)] ?? $row[strtolower($sourceCodeColumn)];
            foreach ($row as $key => $val) {
                // si le nom de colonne est de la forme 'U_xxxx', il s'agit d'un témoin de mise à jour de la colonne 'xxxx'
                $keyPrefix = substr($key, 0, $len = strlen(Synchro::V_DIFF_UPDATED_COLUMN_PREFIX));
                $isUpdatedFlag =
                    $keyPrefix === Synchro::V_DIFF_UPDATED_COLUMN_PREFIX ||
                    $keyPrefix === strtolower(Synchro::V_DIFF_UPDATED_COLUMN_PREFIX);
                if ($isUpdatedFlag && $val === 1) {
                    $col = substr($key, $len);
                    $colValueFrom =
                        $row[Synchro::V_DIFF_DEST_COLUMN_PREFIX . $col] ??
                        $row[strtolower(Synchro::V_DIFF_DEST_COLUMN_PREFIX . $col)]; // NULL en cas d'opération d'insertion
                    $colValueTo =
                        $row[Synchro::V_DIFF_SRC_COLUMN_PREFIX . $col] ??
                        $row[strtolower(Synchro::V_DIFF_SRC_COLUMN_PREFIX . $col)];
                    $data[$col] = [
                        'from' => $colValueFrom,
                        'to' => $colValueTo,
                    ];
                }
            }

            $result[$operation][] = $data;
        }

        // comblement en cas de clé/opération absente
        foreach (Operation::OPERATIONS as $operation) {
            if (!array_key_exists($operation, $result)) {
                $result[$operation] = [];
            }
        }

        return $result;
    }
}