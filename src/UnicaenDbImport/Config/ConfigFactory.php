<?php

namespace UnicaenDbImport\Config;

use Assert\Assertion;
use Assert\AssertionFailedException;
use Doctrine\DBAL\Connection as DbConnection;
use Psr\Container\ContainerInterface;
use UnicaenDbImport\Connection\ApiConnection;
use UnicaenDbImport\Domain\Destination;
use UnicaenDbImport\Domain\Import;
use UnicaenDbImport\Domain\Source;
use UnicaenDbImport\Domain\Synchro;
use UnicaenDbImport\Filter\ColumnName\ArrayMapColumnNameFilter;
use UnicaenDbImport\Filter\ColumnName\ColumnNameFilterInterface;
use UnicaenDbImport\Filter\ColumnValue\ColumnValueFilterInterface;
use UnicaenDbImport\Filter\ColumnValue\SameColumnValueFilter;

class ConfigFactory
{
    const CONFIG_ROOT_KEY = 'import';

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @var array
     */
    private $moduleConfig;

    /**
     * @var array
     */
    private $connections;

    /**
     * @param ContainerInterface $container
     * @return Config
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     * @throws \UnicaenDbImport\Config\ConfigException
     */
    public function __invoke(ContainerInterface $container): Config
    {
        $this->container = $container;
        $this->moduleConfig = $this->getModuleConfig();

        $defaultSourceCode = $this->getDefaultSourceCode();
        $importObservEntityClass = $this->getImportObservEntityClass();
        $importObservResultEntityClass = $this->getImportObservResultEntityClass();
        $codeGeneratorsConfig = $this->getCodeGeneratorsConfig();
        $codeGeneratorsFactoriesConfig = $this->getCodeGeneratorsFactoriesConfig();
        $histoColumnsAliases = $this->getHistoColumnsAliases();
        $histoColumnsValues = $this->getHistoColumnsValues();
        $useImportObserv = $this->getUseImportObserv();
        $imports = $this->createImports();
        $synchros = $this->createSynchros();

        $config = new Config();
        $config->setDefaultSourceCode($defaultSourceCode);
        $config->setImportObservEntityClass($importObservEntityClass);
        $config->setImportObservResultEntityClass($importObservResultEntityClass);
        $config->setCodeGeneratorsMappingConfig($codeGeneratorsConfig);
        $config->setCodeGeneratorsFactoriesConfig($codeGeneratorsFactoriesConfig);
        $config->setHistoColumnsAliases($histoColumnsAliases);
        $config->setHistoColumnsValues($histoColumnsValues);
        $config->setUseImportObserv($useImportObserv);
        $config->setImports($imports);
        $config->setSynchros($synchros);

        return $config;
    }

    /**
     * @return mixed
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     * @throws \UnicaenDbImport\Config\ConfigException
     */
    private function getModuleConfig()
    {
        $appConfig = $this->container->get('Config');

        if (!array_key_exists($key = self::CONFIG_ROOT_KEY, $appConfig)) {
            throw ConfigException::missingRootKey($key);
        }

        return $appConfig[$key];
    }

    /**
     * @return string
     * @throws \UnicaenDbImport\Config\ConfigException
     */
    private function getDefaultSourceCode(): string
    {
        try {
            Assertion::keyExists($this->moduleConfig, $key = 'default_source_code');
        } catch (AssertionFailedException $e) {
            throw ConfigException::missingKey($key);
        }

        return $this->moduleConfig[$key];
    }

    /**
     * @return string
     * @throws \UnicaenDbImport\Config\ConfigException
     */
    private function getImportObservEntityClass(): string
    {
        try {
            Assertion::keyExists($this->moduleConfig, $key = 'import_observ_entity_class');
        } catch (AssertionFailedException $e) {
            throw ConfigException::missingKey($key);
        }

        $importObservEntityClass = $this->moduleConfig[$key];
        if (!class_exists($importObservEntityClass)) {
            throw ConfigException::invalidValueForKey($key, 'nom de classe valide');
        }

        return $importObservEntityClass;
    }

    /**
     * @return string
     * @throws \UnicaenDbImport\Config\ConfigException
     */
    private function getImportObservResultEntityClass(): string
    {
        try {
            Assertion::keyExists($this->moduleConfig, $key = 'import_observ_result_entity_class');
        } catch (AssertionFailedException $e) {
            throw ConfigException::missingKey($key);
        }

        $importObservResultEntityClass = $this->moduleConfig[$key];
        if (!class_exists($importObservResultEntityClass)) {
            throw ConfigException::invalidValueForKey($key, 'nom de classe valide');
        }

        return $importObservResultEntityClass;
    }

    /**
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     * @throws \UnicaenDbImport\Config\ConfigException
     */
    private function createConnections()
    {
        try {
            Assertion::keyExists($this->moduleConfig, $key = 'connections');
        } catch (AssertionFailedException $e) {
            throw ConfigException::missingKey($key);
        }
        try {
            Assertion::isArray($this->moduleConfig[$key]);
        } catch (AssertionFailedException $e) {
            throw ConfigException::invalidValueForKey($key, 'array');
        }

        $connections = [];
        foreach ($this->moduleConfig['connections'] as $key => $value) {
            $connections[$key] = $this->createConnection($value);
        }

        $this->connections = $connections;
    }

    /**
     * @param string|array $value
     * @return DbConnection|ApiConnection
     *
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     * @throws \UnicaenDbImport\Config\ConfigException
     */
    private function createConnection($value)
    {
        if (!is_string($value) && !is_array($value)) {
            throw new ConfigException("Une connection ne peut être créée qu'à partir d'une string ou d'un array");
        }

        if (is_string($value)) {
            $connectionName = $value;
            /** @var DbConnection $conn */
            $conn = $this->container->get($connectionName);
        } else {
            $conn = ApiConnection::fromArrayConfig($value);
        }

        return $conn;
    }

    /**
     * @return Import[]
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     * @throws \UnicaenDbImport\Config\ConfigException
     */
    private function createImports(): array
    {
        $imports = [];
        if (array_key_exists($key = 'imports', $this->moduleConfig)) {
            foreach ($this->moduleConfig[$key] as $importArr) {
                $imports[] = $this->createImport($importArr);
            }
        }

        return $imports;
    }

    /**
     * @param array $importConfig
     * @return Import
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     * @throws \UnicaenDbImport\Config\ConfigException
     */
    private function createImport(array $importConfig): Import
    {
        if (!array_key_exists($key = 'name', $importConfig)) {
            throw ConfigException::missingKeyInImport($key);
        }
        $name = $importConfig['name'];

        if (!array_key_exists($key = 'source', $importConfig)) {
            throw ConfigException::missingKeyInImport($key, $name);
        }
        if (!array_key_exists($key = 'destination', $importConfig)) {
            throw ConfigException::missingKeyInImport($key, $name);
        }

        try {
            $source = $this->createSource($importConfig['source']);
        } catch (ConfigException $e) {
            throw new ConfigException("Une erreur est suvenue lors de la création de la source de l'import '$name'.", null, $e);
        }
        try {
            $destination = $this->createDestination($importConfig['destination']);
        } catch (ConfigException $e) {
            throw new ConfigException("Une erreur est suvenue lors de la création de la destination de l'import '$name'.", null, $e);
        }

        $importConfig['source'] = $source;
        $importConfig['destination'] = $destination;

        return Import::fromConfig($importConfig);
    }

    /**
     * @return Synchro[]
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     * @throws \UnicaenDbImport\Config\ConfigException
     */
    private function createSynchros(): array
    {
        $synchros = [];
        if (array_key_exists($key = 'synchros', $this->moduleConfig)) {
            foreach ($this->moduleConfig[$key] as $synchroArr) {
                $synchros[] = $this->createSynchro($synchroArr);
            }
        }

        return $synchros;
    }

    /**
     * @param array $synchroConfig
     * @return Synchro
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     * @throws \UnicaenDbImport\Config\ConfigException
     */
    private function createSynchro(array $synchroConfig): Synchro
    {
        if (!array_key_exists($key = 'name', $synchroConfig)) {
            throw ConfigException::missingKeyInImport($key);
        }
        $name = $synchroConfig['name'];

        if (!array_key_exists($key = 'source', $synchroConfig)) {
            throw ConfigException::missingKeyInSynchro($key, $name);
        }
        if (!array_key_exists($key = 'destination', $synchroConfig)) {
            throw ConfigException::missingKeyInSynchro($key, $name);
        }

        try {
            $source = $this->createSource($synchroConfig['source']);
        } catch (ConfigException $e) {
            throw new ConfigException("Une erreur est suvenue lors de la création de la source de la synchro '$name'.", null, $e);
        }
        try {
            $destination = $this->createDestination($synchroConfig['destination']);
        } catch (ConfigException $e) {
            throw new ConfigException("Une erreur est suvenue lors de la création de la destination de la synchro '$name'.", null, $e);
        }

        $synchroConfig['source'] = $source;
        $synchroConfig['destination'] = $destination;

        return Synchro::fromConfig($synchroConfig);
    }

    /**
     * @param array $sourceConfig
     * @return Source
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     * @throws \UnicaenDbImport\Config\ConfigException
     */
    private function createSource(array $sourceConfig): Source
    {
        $connectionName = $sourceConfig['connection'];
        $sourceConfig['connection'] = $this->getConnectionByName($connectionName);

        $sourceConfig['computed_columns'] = $this->getColumnValueFilterInstances($sourceConfig, 'computed_columns');
        $sourceConfig['column_value_filter'] = $this->getColumnValueFilterInstances($sourceConfig, 'column_value_filter');

        $columnNameFilter = $sourceConfig['column_name_filter'] ?? null;
        if ($columnNameFilter !== null) {
            $sourceConfig['column_name_filter'] = $this->getColumnNameFilterInstance($columnNameFilter);
        }

        return Source::fromConfig($sourceConfig);
    }

    /**
     * @param array $destinationConfig
     * @return Destination
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     * @throws \UnicaenDbImport\Config\ConfigException
     */
    private function createDestination(array $destinationConfig): Destination
    {
        $connectionName = $destinationConfig['connection'];
        $destinationConfig['connection'] = $this->getConnectionByName($connectionName);

        $destinationConfig['column_value_filter'] = $this->getColumnValueFilterInstances($destinationConfig, 'column_value_filter');

        return Destination::fromConfig($destinationConfig);
    }

    /**
     * @param string $connectionName
     * @return DbConnection|ApiConnection
     *
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     * @throws \UnicaenDbImport\Config\ConfigException
     */
    private function getConnectionByName(string $connectionName)
    {
        if ($this->connections === null) {
            $this->createConnections();
        }

        try {
            Assertion::keyExists($this->connections, $connectionName);
        } catch (AssertionFailedException $e) {
            throw ConfigException::missingKeyInConnections($connectionName);
        }
        /** @var DbConnection|ApiConnection $conn */
        $conn = $this->connections[$connectionName];

        return $conn;
    }

    /**
     * @return array
     * @throws \UnicaenDbImport\Config\ConfigException
     */
    private function getCodeGeneratorsConfig(): array
    {
        try {
            Assertion::keyExists($this->moduleConfig, $key = 'code_generators_mapping');
        } catch (AssertionFailedException $e) {
            throw ConfigException::missingKey($key);
        }
        try {
            Assertion::isArray($this->moduleConfig[$key]);
        } catch (AssertionFailedException $e) {
            throw ConfigException::invalidValueForKey($key, 'array');
        }

        return $this->moduleConfig[$key];
    }

    /**
     * @return array
     * @throws \UnicaenDbImport\Config\ConfigException
     */
    private function getCodeGeneratorsFactoriesConfig(): array
    {
        try {
            Assertion::keyExists($this->moduleConfig, $key = 'code_generators_factories');
        } catch (AssertionFailedException $e) {
            throw ConfigException::missingKey($key);
        }
        try {
            Assertion::isArray($this->moduleConfig[$key]);
        } catch (AssertionFailedException $e) {
            throw ConfigException::invalidValueForKey($key, 'array');
        }

        return $this->moduleConfig[$key];
    }

    /**
     * @return array
     * @throws \UnicaenDbImport\Config\ConfigException
     */
    private function getHistoColumnsAliases(): array
    {
        try {
            Assertion::keyExists($this->moduleConfig, $key = 'histo_columns_aliases');
        } catch (AssertionFailedException $e) {
            throw ConfigException::missingKey($key);
        }
        try {
            Assertion::isArray($this->moduleConfig[$key]);
        } catch (AssertionFailedException $e) {
            throw ConfigException::invalidValueForKey($key, 'array');
        }

        return $this->moduleConfig[$key];
    }

    /**
     * @return array
     * @throws \UnicaenDbImport\Config\ConfigException
     */
    private function getHistoColumnsValues(): array
    {
        try {
            Assertion::keyExists($this->moduleConfig, $key = 'histo_columns_values');
        } catch (AssertionFailedException $e) {
            throw ConfigException::missingKey($key);
        }
        try {
            Assertion::isArray($this->moduleConfig[$key]);
        } catch (AssertionFailedException $e) {
            throw ConfigException::invalidValueForKey($key, 'array');
        }

        return $this->moduleConfig[$key];
    }

    /**
     * @return bool
     * @throws \UnicaenDbImport\Config\ConfigException
     */
    private function getUseImportObserv(): bool
    {
        try {
            Assertion::keyExists($this->moduleConfig, $key = 'use_import_observ');
        } catch (AssertionFailedException $e) {
            throw ConfigException::missingKey($key);
        }

        return (bool) $this->moduleConfig[$key];
    }

    /**
     * @param string|ColumnNameFilterInterface $spec
     * @return ColumnNameFilterInterface
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     * @throws \UnicaenDbImport\Config\ConfigException
     */
    private function getColumnNameFilterInstance($spec): ColumnNameFilterInterface
    {
        $filterInstance = NULL;

        if (is_string($spec)) {
            if ($this->container->has($spec)) {
                $filterInstance = $this->container->get($spec);
                try {
                    Assertion::isInstanceOf($filterInstance, ColumnNameFilterInterface::class);
                } catch (AssertionFailedException $e) {
                    throw ConfigException::invalidValueForKey(
                        'column_name_filter',
                        "service implémentant " . ColumnNameFilterInterface::class
                    );
                }
            } elseif (class_exists($spec)) {
                $filterInstance = new $spec;
                try {
                    Assertion::isInstanceOf($filterInstance, ColumnNameFilterInterface::class);
                } catch (AssertionFailedException $e) {
                    throw ConfigException::invalidValueForKey(
                        'column_name_filter',
                        "instance de type " . ColumnNameFilterInterface::class
                    );
                }
            }
        } elseif (is_array($spec)) {
            $filterInstance = new ArrayMapColumnNameFilter($spec);
        } elseif ($spec instanceof ColumnNameFilterInterface) {
            $filterInstance = $spec;
        }

        if ($filterInstance === NULL) {
            throw ConfigException::invalidValueForKey('column_name_filter');
        }

        return $filterInstance;
    }

    /**
     * @return \UnicaenDbImport\Filter\ColumnValue\ColumnValueFilterInterface[]
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     * @throws \UnicaenDbImport\Config\ConfigException
     */
    private function getColumnValueFilterInstances(array $sourceConfig, string $key): array
    {
        $columnValueFilter = $sourceConfig[$key] ?? [];

        $filterInstances = [];
        foreach ((array) $columnValueFilter as $nameOrIndex => $spec) {
            try {
                $filterInstance = $this->getFilterInstance($spec);
            } catch (ConfigException $e) {
                throw ConfigException::invalidValueForKey($key, null, $e);
            }
            if ($filterInstance === NULL) {
                throw ConfigException::invalidValueForKey($key);
            }
            $filterInstances[$nameOrIndex] = $filterInstance;
        }

        return $filterInstances;
    }

    /**
     * @param string|ColumnValueFilterInterface $spec
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     * @throws \UnicaenDbImport\Config\ConfigException
     */
    private function getFilterInstance($spec): ?ColumnValueFilterInterface
    {
        $filterInstance = NULL;

        if (is_array($spec)) {
            try {
                Assertion::keyExists($spec, $key = 'name');
            } catch (AssertionFailedException $e) {
                throw ConfigException::missingKey($key);
            }
            $filterInstance = $this->getFilterInstance($spec[$key]);
            $filterParams = $spec['params'] ?? [];
            $filterInstance->setParams($filterParams);
        } elseif (is_string($spec)) {
            if ($this->container->has($spec)) {
                $filterInstance = $this->container->get($spec);
                if (!$filterInstance instanceof ColumnValueFilterInterface) {
                    throw ConfigException::invalidValue($spec, "service implémentant " . ColumnValueFilterInterface::class);
                }
            } elseif (class_exists($spec)) {
                $filterInstance = new $spec;
                if (!$filterInstance instanceof ColumnValueFilterInterface) {
                    throw ConfigException::invalidValue($spec, "instance de type " . ColumnValueFilterInterface::class);
                }
            } else {
                $filterInstance = new SameColumnValueFilter(['column' => $spec]);
            }
        } elseif ($spec instanceof ColumnValueFilterInterface) {
            $filterInstance = $spec;
        }

        return $filterInstance;
    }
}