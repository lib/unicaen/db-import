<?php

namespace UnicaenDbImport\Config;

use UnicaenDbImport\Domain\ImportInterface;
use UnicaenDbImport\Domain\SynchroInterface;

/**
 * Config du module.
 */
class Config
{
    const KEY_default_source_code = 'default_source_code';
    const KEY_source_entity_class = 'source_entity_class';

    const COLUMN_CREATED_ON = 'created_on';
    const COLUMN_UPDATED_ON = 'updated_on';
    const COLUMN_DELETED_ON = 'deleted_on';
    const COLUMN_CREATED_BY = 'created_by';
    const COLUMN_UPDATED_BY = 'updated_by';
    const COLUMN_DELETED_BY = 'deleted_by';

    /**
     * @var string
     */
    protected $sourceEntityClass;

    /**
     * @var string
     */
    protected $defaultSourceCode;

    /**
     * @var string
     */
    protected $importObservEntityClass;

    /**
     * @var string
     */
    protected $importObservResultEntityClass;

    /**
     * platform's class => code generator's class
     * @var array
     */
    protected $codeGeneratorsMappingConfig = [];

    /**
     * code generator's class => factory's class
     * @var array
     */
    protected $codeGeneratorsFactoriesConfig = [];

    /**
     * column's name => aliases
     * @var array
     */
    protected $histoColumnsAliases = [
        // date/heure des créations, modifications, suppressions d'enregistrements
        'created_on' => null,
        'updated_on' => null,
        'deleted_on' => null,
        // auteur des créations, modifications, suppressions d'enregistrements
        'created_by' => null,
        'updated_by' => null,
        'deleted_by' => null,
    ];

    /**
     * column's name => forced value
     * @var array
     */
    protected $histoColumnsValues = [
        // auteur des créations, modifications, suppressions d'enregistrements
        'created_by' => null,
        'updated_by' => null,
        'deleted_by' => null,
    ];

    /**
     * @var bool
     */
    protected $useImportObserv = false;

    /**
     * @var ImportInterface[]
     */
    protected $imports = [];

    /**
     * @var SynchroInterface[]
     */
    protected $synchros = [];

    /**
     * @param string $sourceEntityClass
     * @return self
     */
    public function setSourceEntityClass(string $sourceEntityClass): self
    {
        $this->sourceEntityClass = $sourceEntityClass;
        return $this;
    }

    /**
     * @return string
     */
    public function getSourceEntityClass(): string
    {
        return $this->sourceEntityClass;
    }

    /**
     * @return string
     */
    public function getDefaultSourceCode(): string
    {
        return $this->defaultSourceCode;
    }

    /**
     * @param string $defaultSourceCode
     * @return Config
     */
    public function setDefaultSourceCode(string $defaultSourceCode): Config
    {
        $this->defaultSourceCode = $defaultSourceCode;
        return $this;
    }

    /**
     * @return string
     */
    public function getImportObservEntityClass(): string
    {
        return $this->importObservEntityClass;
    }

    /**
     * @param string $importObservEntityClass
     * @return Config
     */
    public function setImportObservEntityClass(string $importObservEntityClass): Config
    {
        $this->importObservEntityClass = $importObservEntityClass;
        return $this;
    }

    /**
     * @return string
     */
    public function getImportObservResultEntityClass(): string
    {
        return $this->importObservResultEntityClass;
    }

    /**
     * @param string $importObservResultEntityClass
     * @return Config
     */
    public function setImportObservResultEntityClass(string $importObservResultEntityClass): Config
    {
        $this->importObservResultEntityClass = $importObservResultEntityClass;
        return $this;
    }

    /**
     * @param array $codeGeneratorsMappingConfig
     * @return Config
     */
    public function setCodeGeneratorsMappingConfig(array $codeGeneratorsMappingConfig): Config
    {
        $this->codeGeneratorsMappingConfig = $codeGeneratorsMappingConfig;

        return $this;
    }

    /**
     * @return array
     */
    public function getCodeGeneratorsMappingConfig(): array
    {
        return $this->codeGeneratorsMappingConfig;
    }

    /**
     * @return array
     */
    public function getCodeGeneratorsFactoriesConfig(): array
    {
        return $this->codeGeneratorsFactoriesConfig;
    }

    /**
     * @param array $codeGeneratorsFactoriesConfig
     * @return static
     */
    public function setCodeGeneratorsFactoriesConfig(array $codeGeneratorsFactoriesConfig): Config
    {
        $this->codeGeneratorsFactoriesConfig = $codeGeneratorsFactoriesConfig;

        return $this;
    }

    /**
     * @return array [column => alias]
     */
    public function getHistoColumnsAliases(): array
    {
        return $this->histoColumnsAliases;
    }

    /**
     * @param array $histoColumnsAliases [column => alias]
     * @return Config
     */
    public function setHistoColumnsAliases(array $histoColumnsAliases): Config
    {
        $this->histoColumnsAliases = array_combine(
            array_map([$this, 'normalizeColumnName'], array_keys($histoColumnsAliases)),
            array_map([$this, 'normalizeColumnName'], array_values($histoColumnsAliases))
        );

        $this->histoColumnsAliases = $histoColumnsAliases;
        return $this;
    }

    /**
     * @return string
     */
    public function getHistoColumnAliasForCreatedOn(): string
    {
        return $this->getHistoColumnAliasFor(self::COLUMN_CREATED_ON);
    }

    /**
     * @return string
     */
    public function getHistoColumnAliasForUpdatedOn(): string
    {
        return $this->getHistoColumnAliasFor(self::COLUMN_UPDATED_ON);
    }

    /**
     * @return string
     */
    public function getHistoColumnAliasForDeletedOn(): string
    {
        return $this->getHistoColumnAliasFor(self::COLUMN_DELETED_ON);
    }

    /**
     * @return string
     */
    public function getHistoColumnAliasForCreatedBy(): string
    {
        return $this->getHistoColumnAliasFor(self::COLUMN_CREATED_BY);
    }

    /**
     * @return string
     */
    public function getHistoColumnAliasForUpdatedBy(): string
    {
        return $this->getHistoColumnAliasFor(self::COLUMN_UPDATED_BY);
    }

    /**
     * @return string
     */
    public function getHistoColumnAliasForDeletedBy(): string
    {
        return $this->getHistoColumnAliasFor(self::COLUMN_DELETED_BY);
    }

    /**
     * @param string $columnName
     * @return string
     */
    public function getHistoColumnAliasFor(string $columnName): string
    {
        $name = $this->normalizeColumnName($columnName);
        $aliases = $this->getHistoColumnsAliases();

        if (array_key_exists($name, $aliases)) {
            return $aliases[$name];
        }

        // pas d'alias
        return $columnName;
    }

    /**
     * @return array [column => value]
     */
    public function getHistoColumnsValues(): array
    {
        return $this->histoColumnsValues;
    }

    /**
     * @param array $histoColumnsValues [column => value]
     * @return Config
     */
    public function setHistoColumnsValues(array $histoColumnsValues): Config
    {
        $this->histoColumnsValues = array_combine(
            array_map([$this, 'normalizeColumnName'], array_keys($histoColumnsValues)),
            $histoColumnsValues
        );

        return $this;
    }

    /**
     * @param string $columnName
     * @return bool
     */
    public function hasHistoColumnValueFor(string $columnName): bool
    {
        $name = $this->normalizeColumnName($columnName);
        $histoColumnsValues = $this->getHistoColumnsValues();

        if (array_key_exists($name, $histoColumnsValues)) {
            return true;
        }

        // le forçage de valeur a pu être spécifié avec un alias de colonne
        $columnNameAlias = $this->getHistoColumnAliasFor($columnName);
        if (array_key_exists($columnNameAlias, $histoColumnsValues)) {
            return true;
        }

        return false;
    }

    /**
     * @return int|null
     */
    public function getHistoColumnValueForCreatedBy(): ?int
    {
        return $this->getHistoColumnValueFor(self::COLUMN_CREATED_BY);
    }

    /**
     * @return int|null
     */
    public function getHistoColumnValueForUpdatedBy(): ?int
    {
        return $this->getHistoColumnValueFor(self::COLUMN_UPDATED_BY);
    }

    /**
     * @return int|null
     */
    public function getHistoColumnValueForDeletedBy(): ?int
    {
        return $this->getHistoColumnValueFor(self::COLUMN_DELETED_BY);
    }

    /**
     * @param string $columnName
     * @return mixed|null
     */
    public function getHistoColumnValueFor(string $columnName)
    {
        $name = $this->normalizeColumnName($columnName);
        $values = $this->getHistoColumnsValues();

        if (array_key_exists($name, $values)) {
            return $values[$name];
        }

        return null;
    }

    /**
     * @param string $columnName
     * @return string
     */
    protected function normalizeColumnName(string $columnName): string
    {
        return strtolower($columnName);
    }

    /**
     * @param ImportInterface[] $imports
     * @return static
     */
    public function setImports(array $imports): self
    {
        usort($imports, function(ImportInterface $a, ImportInterface $b) {
            return $a->getOrder() <=> $b->getOrder();
        });

        $this->imports = $imports;

        return $this;
    }

    /**
     * @return ImportInterface[]
     */
    public function getImports(): array
    {
        return $this->imports;
    }

    /**
     * @param string $name
     * @return ImportInterface
     */
    public function getImport(string $name): ?ImportInterface
    {
        foreach ($this->imports as $import) {
            if ($import->getName() === $name) {
                return $import;
            }
        }

        return null;
    }

    /**
     * @param SynchroInterface[] $synchros
     * @return static
     */
    public function setSynchros(array $synchros): self
    {
        usort($synchros, function(SynchroInterface $a, SynchroInterface $b) {
            return $a->getOrder() <=> $b->getOrder();
        });

        $this->synchros = $synchros;

        return $this;
    }

    /**
     * @return SynchroInterface[]
     */
    public function getSynchros(): array
    {
        return $this->synchros;
    }

    /**
     * @param string $name
     * @return SynchroInterface
     */
    public function getSynchro(string $name): ?SynchroInterface
    {
        foreach ($this->synchros as $synchro) {
            if ($synchro->getName() === $name) {
                return $synchro;
            }
        }

        return null;
    }

    /**
     * @param bool $useImportObserv
     * @return self
     */
    public function setUseImportObserv(bool $useImportObserv): self
    {
        $this->useImportObserv = $useImportObserv;

        return $this;
    }

    /**
     * @return bool
     */
    public function getUseImportObserv(): bool
    {
        return $this->useImportObserv;
    }
}