<?php

namespace UnicaenDbImport\Config;

trait ConfigAwareTrait
{
    /**
     * @var Config
     */
    protected $config;

    /**
     * @param Config $config
     */
    public function setConfig(Config $config)
    {
        $this->config = $config;
    }
}