<?php

namespace UnicaenDbImport\Config;

use Exception;
use Symfony\Component\VarDumper\VarDumper;
use Throwable;

class ConfigException extends Exception
{
    public static function missingKeyInImport($key, $name = null): self
    {
        return $name ?
            new static("Le tableau de config de l'import '$name' doit contenir une clé '$key'") :
            new static("Le tableau de config d'un import ne contient pas la clé '$key' attendue");
    }

    public static function missingKeyInSynchro($key, $name = null): self
    {
        return $name ?
            new static("Le tableau de config de la synchro '$name' doit contenir une clé '$key'") :
            new static("Le tableau de config d'une synchro ne contient pas la clé '$key' attendue");
    }

    public static function missingRootKey($key): self
    {
        return new static("La config de l'appli doit contenir la clé '$key' contenant la config du module");
    }

    public static function missingKey($key): self
    {
        return new static("Le tableau de config du module doit contenir la clé '$key'");
    }

    public static function missingKeyInConnections($key): self
    {
        return new static("Clé '$key' introuvable dans les connexions");
    }

    public static function exclusiveKeys(array $keys): self
    {
        return new static("Les clés de config suivantes ne peuvent être présentes toutes les deux: " . implode(', ', $keys));
    }

    public static function atLeastOneKey(array $keys): self
    {
        return new static("L'une des clés de config suivantes doit être présente: " . implode(', ', $keys));
    }

    public static function illegalKey($key): self
    {
        return new static("La clé de config '$key' n'est pas autorisée");
    }

    public static function invalidValueForKey($key, string $expected = null, Throwable $previous = null): self
    {
        return new static(
            "La valeur associée à la clé de config '$key' n'est pas valide." .
            ($expected ? " Attendu : $expected." : ""),
            0,
            $previous
        );
    }

    public static function invalidValue($value, string $expected = null, Throwable $previous = null): self
    {
        return new static(
            "La valeur suivante n'est pas valide: " . print_r($value, true) . '.' .
            ($expected ? " Attendu : $expected." : ""),
            0,
            $previous
        );
    }
}