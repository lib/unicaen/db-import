<?php

namespace UnicaenDbImport;

use Doctrine\DBAL\Platforms\PostgreSQL100Platform;
use Doctrine\DBAL\Platforms\PostgreSQL120Platform;
use Doctrine\DBAL\Platforms\PostgreSQL91Platform;
use Doctrine\DBAL\Platforms\PostgreSQL92Platform;
use Doctrine\DBAL\Platforms\PostgreSQL94Platform;
use Doctrine\DBAL\Platforms\PostgreSqlPlatform;
use UnicaenDbImport\CodeGenerator\PostgreSQL\CodeGenerator;
use UnicaenDbImport\CodeGenerator\PostgreSQL\CodeGeneratorFactory;
use UnicaenDbImport\Config\Config;

return [
    'import' => [
        /**
         * @see Config::getCodeGeneratorsMappingConfig()
         */
        'code_generators_mapping' => [
            PostgreSqlPlatform::class    => CodeGenerator::class,
            PostgreSQL91Platform::class  => CodeGenerator::class,
            PostgreSQL92Platform::class  => CodeGenerator::class,
            PostgreSQL94Platform::class  => CodeGenerator::class,
            PostgreSQL100Platform::class => CodeGenerator::class,
            PostgreSQL120Platform::class => CodeGenerator::class,
        ],
        /**
         * @see Config::getCodeGeneratorsFactoriesConfig()
         */
        'code_generators_factories' => [
            CodeGenerator::class => CodeGeneratorFactory::class,
        ],
    ],
];
