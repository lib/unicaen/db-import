<?php

namespace UnicaenDbImport;

use Doctrine\DBAL\Platforms\OraclePlatform;
use UnicaenDbImport\CodeGenerator\Oracle\CodeGenerator;
use UnicaenDbImport\CodeGenerator\Oracle\CodeGeneratorFactory;

return [
    'import' => [
        /**
         * @see Config::getCodeGeneratorsMappingConfig()
         */
        'code_generators_mapping' => [
            OraclePlatform::class => CodeGenerator::class,
        ],
        /**
         * @see Config::getCodeGeneratorsFactoriesConfig()
         */
        'code_generators_factories' => [
            CodeGenerator::class => CodeGeneratorFactory::class,
        ],
    ],
];
