<?php

namespace UnicaenDbImport;

use Doctrine\ORM\Mapping\Driver\XmlDriver;
use Doctrine\Persistence\Mapping\Driver\MappingDriverChain;
use Laminas\Router\Http\Literal;
use Laminas\Router\Http\Segment;
use Laminas\ServiceManager\Factory\InvokableFactory;
use UnicaenPrivilege\Guard\PrivilegeController;
use UnicaenDbImport\CodeGenerator\HelperAbstractFactory;
use UnicaenDbImport\Config\Config;
use UnicaenDbImport\Config\ConfigFactory;
use UnicaenDbImport\Controller\ConsoleController;
use UnicaenDbImport\Controller\ConsoleControllerFactory;
use UnicaenDbImport\Controller\ImportController;
use UnicaenDbImport\Controller\ImportControllerFactory;
use UnicaenDbImport\Controller\LogController;
use UnicaenDbImport\Controller\LogControllerFactory;
use UnicaenDbImport\Controller\ObservController;
use UnicaenDbImport\Controller\ObservControllerFactory;
use UnicaenDbImport\Controller\SynchroController;
use UnicaenDbImport\Controller\SynchroControllerFactory;
use UnicaenDbImport\Entity\Db\ImportObserv;
use UnicaenDbImport\Entity\Db\ImportObservResult;
use UnicaenDbImport\Entity\Db\Service\ImportLog\ImportLogService;
use UnicaenDbImport\Entity\Db\Service\ImportLog\ImportLogServiceFactory;
use UnicaenDbImport\Entity\Db\Service\ImportObserv\ImportObservService;
use UnicaenDbImport\Entity\Db\Service\ImportObserv\ImportObservServiceFactory;
use UnicaenDbImport\Entity\Db\Service\ImportObservResult\ImportObservResultService;
use UnicaenDbImport\Entity\Db\Service\ImportObservResult\ImportObservResultServiceFactory;
use UnicaenDbImport\Entity\Db\Service\Source\SourceService;
use UnicaenDbImport\Entity\Db\Service\Source\SourceServiceFactory;
use UnicaenDbImport\Entity\Db\Source;
use UnicaenDbImport\Log\LivelogLogger;
use UnicaenDbImport\Log\LivelogLoggerFactory;
use UnicaenDbImport\ORM\Event\Listeners\SourceListener;
use UnicaenDbImport\ORM\Event\Listeners\SourceListenerFactory;
use UnicaenDbImport\Privilege\ImportPrivilege;
use UnicaenDbImport\Privilege\LogPrivilege;
use UnicaenDbImport\Privilege\ObservationPrivilege;
use UnicaenDbImport\Privilege\SynchroPrivilege;
use UnicaenDbImport\Service\Api\ApiService;
use UnicaenDbImport\Service\Api\ApiServiceFactory;
use UnicaenDbImport\Service\Api\Response\DataExtractor\Array\ArrayResponseDataExtractor;
use UnicaenDbImport\Service\Api\Response\DataExtractor\Hal\HalResponseDataExtractor;
use UnicaenDbImport\Service\Api\Response\DataExtractor\Solr\SolrResponseDataExtractor;
use UnicaenDbImport\Service\CodeGenerator\CodeGeneratorPluginManager;
use UnicaenDbImport\Service\CodeGenerator\CodeGeneratorPluginManagerFactory;
use UnicaenDbImport\Service\Database\DatabaseService;
use UnicaenDbImport\Service\Database\DatabaseServiceFactory;
use UnicaenDbImport\Service\Database\ImportLogDatabaseService;
use UnicaenDbImport\Service\Database\ImportLogDatabaseServiceFactory;
use UnicaenDbImport\Service\Facade\ImportFacadeService;
use UnicaenDbImport\Service\Facade\ImportFacadeServiceFactory;
use UnicaenDbImport\Service\Facade\LogFacadeService;
use UnicaenDbImport\Service\Facade\LogFacadeServiceFactory;
use UnicaenDbImport\Service\Facade\SynchroFacadeService;
use UnicaenDbImport\Service\Facade\SynchroFacadeServiceFactory;
use UnicaenDbImport\Service\ImportService;
use UnicaenDbImport\Service\ImportServiceFactory;
use UnicaenDbImport\Service\SynchroService;
use UnicaenDbImport\Service\SynchroServiceFactory;

return [
    'import' => [
        'source_entity_class' => Source::class,
        'default_source_code' => 'app',

        'import_observ_entity_class' => ImportObserv::class,
        'import_observ_result_entity_class' => ImportObservResult::class,

        'connections' => [],
        'code_generators_mapping' => [],
        'code_generators_factories' => [],
        'imports' => [],
        'synchros' => [],
    ],

    'doctrine'     => [
        'driver'     => [
            'orm_default'        => [
                'class'   => MappingDriverChain::class,
                'drivers' => [
                    'UnicaenDbImport\Entity\Db\AbstractImportObserv' => 'db_import_orm_default_xml_driver',
                    'UnicaenDbImport\Entity\Db\AbstractImportObservResult' => 'db_import_orm_default_xml_driver',
                    'UnicaenDbImport\Entity\Db\AbstractImportLog' => 'db_import_orm_default_xml_driver',
                    'UnicaenDbImport\Entity\Db\AbstractSource' => 'db_import_orm_default_xml_driver',

                    'UnicaenDbImport\Entity\Db\ImportObserv' => 'db_import_orm_default_xml_driver',
                    'UnicaenDbImport\Entity\Db\ImportObservResult' => 'db_import_orm_default_xml_driver',
                    'UnicaenDbImport\Entity\Db\ImportLog' => 'db_import_orm_default_xml_driver',
                    'UnicaenDbImport\Entity\Db\Source' => 'db_import_orm_default_xml_driver',
                ],
            ],
            'db_import_orm_default_xml_driver' => [
                'class' => XmlDriver::class,
                'cache' => 'array',
                'paths' => [
                    __DIR__ . '/../src/UnicaenDbImport/Entity/Db/Mapping',
                ],
            ],
        ],
        'eventmanager' => [
            'orm_default' => [
                'subscribers' => [
                    SourceListener::class,
                ],
            ],
        ],
    ],

    'navigation' => [
        'default' => [
            'home' => [
                'pages' => [
                    'unicaen-db-import' => [
                        'label' => "Import/Synchro",
                        'route' => 'unicaen-db-import/import',
                        'resource' => ImportPrivilege::getResourceId(ImportPrivilege::LISTER),
                        'order' => 100,
                        'pages' => [
                            'import' => [
                                'label' => "Imports",
                                'route' => 'unicaen-db-import/import',
                                'resource' => ImportPrivilege::getResourceId(ImportPrivilege::LISTER),
                                'order' => 10,
                            ],
                            'synchro' => [
                                'label' => "Synchros",
                                'route' => 'unicaen-db-import/synchro',
                                'resource' => SynchroPrivilege::getResourceId(SynchroPrivilege::LISTER),
                                'order' => 20,
                            ],
                            'log' => [
                                'label' => "Logs",
                                'route' => 'unicaen-db-import/log',
                                'resource' => LogPrivilege::getResourceId(LogPrivilege::LISTER),
                                'order' => 30,
                            ],
                            'observ' => [
                                'label' => "Observations",
                                'route' => 'unicaen-db-import/observ',
                                'resource' => ObservationPrivilege::getResourceId(ObservationPrivilege::LISTER),
                                'order' => 40,
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],

    'router' => [
        'routes' => [
            'unicaen-db-import' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/unicaen-db-import',
                ],
                'may_terminate' => false,
                'child_routes' => [
                    'module' => [
                        'type' => Segment::class,
                        'options' => [
                            'route' => '/module',
                            'defaults' => [
                                'controller' => ImportController::class,
                                'action' => 'index',
                            ],
                        ],
                        'may_terminate' => true,
                        'child_routes' => [
                            'voir' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/voir/:name',
                                    'constraints' => [
                                        'name' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                    ],
                                    'defaults' => [
                                        'action' => 'voir',
                                    ],
                                ],
                            ],
                        ],
                    ],
                    'import' => [
                        'type' => Segment::class,
                        'options' => [
                            'route' => '/import',
                            'defaults' => [
                                'controller' => ImportController::class,
                                'action' => 'index',
                            ],
                        ],
                        'may_terminate' => true,
                        'child_routes' => [
                            'voir' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/voir/:name',
                                    'constraints' => [
                                        'name' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                    ],
                                    'defaults' => [
                                        'action' => 'voir',
                                    ],
                                ],
                            ],
                            'lancer' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/lancer/:name',
                                    'constraints' => [
                                        'name' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                    ],
                                    'defaults' => [
                                        'action' => 'lancer',
                                    ],
                                ],
                            ],
                            'lancer-multiple' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/lancer-multiple/:names',
                                    'constraints' => [
                                        'names' => '[a-zA-Z][,a-zA-Z0-9_-]*',
                                    ],
                                    'defaults' => [
                                        'action' => 'lancer-multiple',
                                    ],
                                ],
                            ],
                            'lastLog' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/last-log/:name',
                                    'constraints' => [
                                        'name' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                    ],
                                    'defaults' => [
                                        'action' => 'lastLog',
                                    ],
                                ],
                            ],
                        ],
                    ],
                    'synchro' => [
                        'type' => Segment::class,
                        'options' => [
                            'route' => '/synchro',
                            'defaults' => [
                                'controller' => SynchroController::class,
                                'action' => 'index',
                            ],
                        ],
                        'may_terminate' => true,
                        'child_routes' => [
                            'voir' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/voir/:name',
                                    'constraints' => [
                                        'name' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                    ],
                                    'defaults' => [
                                        'action' => 'voir',
                                    ],
                                ],
                            ],
                            'diff' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/diff/:name',
                                    'constraints' => [
                                        'name' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                    ],
                                    'defaults' => [
                                        'action' => 'diff',
                                    ],
                                ],
                            ],
                            'diffCount' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/diffCount/:name',
                                    'constraints' => [
                                        'name' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                    ],
                                    'defaults' => [
                                        'action' => 'diffCount',
                                    ],
                                ],
                            ],
                            'lancer' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/lancer/:name',
                                    'constraints' => [
                                        'name' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                    ],
                                    'defaults' => [
                                        'action' => 'lancer',
                                    ],
                                ],
                            ],
                            'lancer-multiple' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/lancer-multiple/:names',
                                    'constraints' => [
                                        'names' => '[a-zA-Z][,a-zA-Z0-9_-]*',
                                    ],
                                    'defaults' => [
                                        'action' => 'lancer-multiple',
                                    ],
                                ],
                            ],
                            'lastLog' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/last-log/:name',
                                    'constraints' => [
                                        'name' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                    ],
                                    'defaults' => [
                                        'action' => 'lastLog',
                                    ],
                                ],
                            ],
                        ],
                    ],
                    'observ' => [
                        'type' => Segment::class,
                        'options' => [
                            'route' => '/observ',
                            'defaults' => [
                                'controller' => ObservController::class,
                                'action' => 'index',
                            ],
                        ],
                        'may_terminate' => true,
                        'child_routes' => [
                            'result' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/result/:code',
                                    'constraints' => [
                                        'code' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                    ],
                                    'defaults' => [
                                        'action' => 'result',
                                    ],
                                ],
                            ],
                        ],
                    ],
                    'log' => [
                        'type' => Segment::class,
                        'options' => [
                            'route' => '/log',
                            'defaults' => [
                                'controller' => LogController::class,
                                'action' => 'index',
                            ],
                        ],
                        'may_terminate' => true,
                        'child_routes' => [
                            'voir' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/voir/:id',
                                    'constraints' => [
                                        'id' => '\d+',
                                    ],
                                    'defaults' => [
                                        'action' => 'voir',
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],

    'bjyauthorize' => [
        'guards' => [
            PrivilegeController::class => [
                //--------------------------- import -------------------------
                [
                    'controller' => ImportController::class,
                    'action' => [
                        'index',
                    ],
                    'privileges' => ImportPrivilege::LISTER,
                ],
                [
                    'controller' => ImportController::class,
                    'action' => [
                        'voir',
                    ],
                    'privileges' => ImportPrivilege::CONSULTER,
                ],
                [
                    'controller' => ImportController::class,
                    'action' => [
                        'lancer',
                        'lancer-multiple',
                    ],
                    'privileges' => ImportPrivilege::LANCER,
                ],
                [
                    'controller' => ImportController::class,
                    'action' => [
                        'lastLog',
                    ],
                    'privileges' => LogPrivilege::CONSULTER,
                ],
                [
                    'controller' => ConsoleController::class,
                    'action' => [
                        'listImports',
                        'runImport',
                    ],
                    'role' => [],
                ],

                //--------------------------- synchro -------------------------
                [
                    'controller' => SynchroController::class,
                    'action' => [
                        'index',
                    ],
                    'privileges' => SynchroPrivilege::LISTER,
                ],
                [
                    'controller' => SynchroController::class,
                    'action' => [
                        'voir',
                    ],
                    'privileges' => SynchroPrivilege::CONSULTER,
                ],
                [
                    'controller' => SynchroController::class,
                    'action' => [
                        'lancer',
                        'lancer-multiple',
                    ],
                    'privileges' => SynchroPrivilege::LANCER,
                ],
                [
                    'controller' => SynchroController::class,
                    'action' => [
                        'diff',
                    ],
                    'privileges' => SynchroPrivilege::CONSULTER,
                ],
                [
                    'controller' => SynchroController::class,
                    'action' => [
                        'diffCount',
                    ],
                    'privileges' => SynchroPrivilege::CONSULTER,
                ],
                [
                    'controller' => SynchroController::class,
                    'action' => [
                        'lastLog',
                    ],
                    'privileges' => LogPrivilege::CONSULTER,
                ],
                [
                    'controller' => ConsoleController::class,
                    'action' => [
                        'listSynchros',
                        'runSynchro',
                    ],
                    'role' => [],
                ],

                //--------------------------- observ -------------------------
                [
                    'controller' => ObservController::class,
                    'action' => [
                        'index',
                    ],
                    'privileges' => ObservationPrivilege::LISTER,
                ],
                [
                    'controller' => ObservController::class,
                    'action' => [
                        'result',
                    ],
                    'privileges' => ObservationPrivilege::CONSULTER_RESULTAT,
                ],

                //--------------------------- logs -------------------------
                [
                    'controller' => LogController::class,
                    'action' => [
                        'index',
                    ],
                    'privileges' => LogPrivilege::LISTER,
                ],
                [
                    'controller' => LogController::class,
                    'action' => [
                        'voir',
                    ],
                    'privileges' => LogPrivilege::CONSULTER,
                ],
            ],
        ],
    ],

    'console' => [
        'router' => [
            'routes' => [
                'list_imports' => [
                    'type'    => 'simple',
                    'options' => [
                        'route'    => 'list imports',
                        'defaults' => [
                            /** @see ConsoleController::listImportsAction() */
                            'controller' => ConsoleController::class,
                            'action'     => 'listImports',
                        ],
                    ],
                ],
                'execute_imports' => [
                    'type'    => 'simple',
                    'options' => [
                        'route'    => 'run import [--name=]',
                        'defaults' => [
                            /** @see ConsoleController::runImportAction() */
                            'controller' => ConsoleController::class,
                            'action'     => 'runImport',
                        ],
                    ],
                ],
                'list_synchros' => [
                    'type'    => 'simple',
                    'options' => [
                        'route'    => 'list synchros',
                        'defaults' => [
                            /** @see ConsoleController::listSynchrosAction() */
                            'controller' => ConsoleController::class,
                            'action'     => 'listSynchros',
                        ],
                    ],
                ],
                'execute_synchro' => [
                    'type'    => 'simple',
                    'options' => [
                        'route'    => 'run synchro [--name=]',
                        'defaults' => [
                            /** @see ConsoleController::runSynchroAction() */
                            'controller' => ConsoleController::class,
                            'action'     => 'runSynchro',
                        ],
                    ],
                ],
            ]
        ]
    ],

    'controllers' => [
        'factories' => [
            ConsoleController::class => ConsoleControllerFactory::class,
            ImportController::class => ImportControllerFactory::class,
            SynchroController::class => SynchroControllerFactory::class,
            ObservController::class => ObservControllerFactory::class,
            LogController::class => LogControllerFactory::class,
        ]
    ],

    'service_manager' => [
        'factories' => [
            Config::class => ConfigFactory::class,

            ImportService::class => ImportServiceFactory::class,
            SynchroService::class => SynchroServiceFactory::class,

            ImportFacadeService::class => ImportFacadeServiceFactory::class,
            SynchroFacadeService::class => SynchroFacadeServiceFactory::class,
            LogFacadeService::class => LogFacadeServiceFactory::class,

            DatabaseService::class => DatabaseServiceFactory::class,
            ImportLogDatabaseService::class => ImportLogDatabaseServiceFactory::class,
            ApiService::class => ApiServiceFactory::class,

            ArrayResponseDataExtractor::class => InvokableFactory::class,
            HalResponseDataExtractor::class => InvokableFactory::class,
            SolrResponseDataExtractor::class => InvokableFactory::class,

            CodeGeneratorPluginManager::class => CodeGeneratorPluginManagerFactory::class,

            SourceService::class => SourceServiceFactory::class,
            ImportLogService::class => ImportLogServiceFactory::class,
            ImportObservService::class => ImportObservServiceFactory::class,
            ImportObservResultService::class => ImportObservResultServiceFactory::class,

            SourceListener::class => SourceListenerFactory::class,

            LivelogLogger::class => LivelogLoggerFactory::class,
        ],
        'abstract_factories' => [
            HelperAbstractFactory::class,
        ],
    ],

    'view_manager' => [
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
    ],

    /**
     * NB : Lors d'un `composer install` fait par une appli requérant ce module, le répertoire
     * `public/unicaen` de ce module doit être copié dans le répertoire "public/" de l'appli en question grâce
     * à la "post install command" suivante :
     *
     * "scripts": {
     *      "post-install-cmd": [
     *          "mkdir -p public/unicaen ; cp -r vendor/unicaen/db-import/public/unicaen public/"
     *
     * Les chemins ci-dessous sont donc relatifs au dossier racine de l'appli.
     */
    'public_files' => [
        'head_scripts' => [
            '100_db-import' => '/unicaen/db-import/js/unicaen-db-import.js',
        ],
        'stylesheets' => [
            '100_db-import' => '/unicaen/db-import/css/unicaen-db-import.css',
        ],
    ],
];
